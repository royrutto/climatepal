<?php
class Surveys extends DB{
	
	/*SURVEY FUNCTIONS*/
	/**
	 * Gets all Surgey details and summary
	 * @return string|multitype:
	 */
	
	public function get_all_surveys(){
		$surveys = array();
		$survey_details = array();
		
		$sql = 'SELECT *, 
				(SELECT count(*) 
				FROM quest_field_responses 
				WHERE survey_id=s.survey_id and deleted = 0 and user_id NOT IN (1)) as num_responses
				FROM surveys s';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$survey_details['survey_id'] = $result['survey_id'];
				$survey_details['description'] = $result['description'];
				$survey_details['num_responses'] = $result['num_responses'];
				$survey_details['num_users'] = $this->get_num_of_users($result['survey_id']);
				
				array_push($surveys, $survey_details);
			}
			return $surveys;

		} else return $surveys;
	
		
	}//end get_get_all_surveys
	
	/**
	 * Get number os users who filled a single survey
	 * @param unknown $survey_id
	 * @return string|mixed
	 */
	public function get_num_of_users($survey_id){
		$num_users =0;
	
		$sql = "SELECT user_id FROM quest_field_responses WHERE survey_id={$survey_id}  AND user_id NOT IN (1) GROUP BY user_id";
	
		try{
			//run query
			$rs = DB::query($sql);
			$num_users = DB::count();
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		return $num_users;

	}//get number_of_users
	
	/**
	 * Get questios using the survey id
	 * @param unknown $survey_id
	 * @return string|multitype:
	 */
	public function get_questions_by_survey_id($survey_id, $get_all_surveys=false){
		$questions = array();
		$question_details = array();
		
		if($get_all_surveys){
		$sql = 'SELECT s.survey_id, qst.id, qstfg.group_id, qstf.field_id, qstf.field_name, qstf.field_type, qstfg.group_name, qstf.required, qstf.options
				FROM
				surveys s 
				INNER JOIN questionnaires qst ON s.survey_id=qst.survey_id
				INNER JOIN quest_fields qstf ON  qst.id = qstf.questionnaire_id
				LEFT JOIN quest_field_groups qstfg ON qstf.group_id = qstfg.group_id
				ORDER BY qstfg.group_id, qstf.field_id';
		} else {
		$sql = 'SELECT s.survey_id, qst.id, qstfg.group_id, qstf.field_id, qstf.field_name, qstf.field_type, qstfg.group_name, qstf.required, qstf.options
				FROM
				surveys s
				INNER JOIN questionnaires qst ON s.survey_id=qst.survey_id
				INNER JOIN quest_fields qstf ON  qst.id = qstf.questionnaire_id
				LEFT JOIN quest_field_groups qstfg ON qstf.group_id = qstfg.group_id
				WHERE s.survey_id ="'.$survey_id.'"
				ORDER BY qstfg.group_id, qstf.field_id';
		}
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$question_details['survey_id'] = $result['survey_id'];
				$question_details['quest_id'] = $result['id'];
				$question_details['group_id'] = $result['group_id'];
				$question_details['group_name'] = $result['group_name'];
				$question_details['field_id'] = $result['field_id'];
				$question_details['field_name'] = $result['field_name'];
				$question_details['field_type'] = $result['field_type'];
				$question_details['required'] = $result['required'];
				$question_details['options'] = $result['options'];
			
				array_push($questions, $question_details);
			}
			return $questions;
	
		} else return $questions;
	
	}//end get_questions by survey id
	
	/**
	 * Save questionnaire response
	 * @param unknown $user_id
	 * @param unknown $survey_id
	 * @param unknown $date_added
	 * @param unknown $fields
	 * @return string|boolean
	 */
	public function save_response($user_id, $survey_id, $date_added, $fields, $latitude, $longitude){
		$sql ="INSERT INTO 
				`quest_field_responses` (`user_id`, `survey_id`, `date_added`, `date_updated`, `latitude`, `longitude`) 
				VALUES ('{$user_id}', '{$survey_id}', '{$date_added}', '{$date_added}', '{$latitude}', '{$longitude}')";
		
		try{
			//run query
			$rs = DB::query($sql);
			$response_id = DB::insertId();
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if household with same name and id exists
		if(!empty($rs)){
			return false;
		} 
		else{
			//get fields responses from $_POST array
			$values="";
			$count=0;
			$found_results=false;
			
			foreach($fields as $key=>$value)
			{
				if ($key!='form_name' && $key!='survey_id'){
					if ($value!=""){
						if ($count==0){
							$values.="({$response_id}, {$key}, '{$value}', '{$date_added}')";
						}else{
							$values.=", ({$response_id}, {$key}, '{$value}', '{$date_added}')";
						}
						$count++;
						$found_results=true;
						}
				}
				
			}
			if($found_results){
			$sql1 = "INSERT INTO `quest_field_results` (`response_id`, `field_id`, `value`, `date_added`)
			VALUES ".$values;
				
			try{
			//run query
				$rs = DB::query($sql1);
			
			}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
				echo "Error: " . $e->getMessage() . "<br>\n";
							echo "SQL Query: " . $e->getQuery() . "<br>\n";
			}
			
			if(!empty($rs)){
			return false;
			} else return true;
				return true;
			}else return false;
					
		}
	}//end save response
	
	public function get_survey_repsonses($survey_id){
		$responses = array();
		$response_details = array();
	
		$sql = "SELECT s.survey_id, qfr.date_added, e.firstname, e.surname, qfr.id FROM surveys s 
inner join quest_field_responses qfr on s.survey_id=qfr.survey_id
left  join users u on qfr.user_id = u.user_id
left join employees e on u.employee_id = e.employee_id
where s.survey_id={$survey_id} and qfr.deleted = 0 AND qfr.user_id NOT IN (1)";
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$response_details['id'] = $result['id'];
				$response_details['survey_id'] = $result['survey_id'];
				$response_details['date_added'] = $result['date_added'];
				$response_details['employee'] = $result['firstname'].' '.$result['surname'];
				$response_details['response_id'] = $result['id'];
	
				array_push($responses, $response_details);
			}
			return $responses;
	
		} else return $responses;
	
	
	}//end get_get_all_surveys
	
	
	public function get_responses_by_response_id($response_id){
		$questions = array();
		$question_details = array();
	
	
			$sql = 'SELECT  qstfg.group_id, qfr.field_id, qfr.value,  qstfg.group_name, qstf.field_name
				FROM
				quest_field_results qfr
				INNER JOIN quest_fields qstf ON  qfr.field_id = qstf.field_id
				LEFT JOIN quest_field_groups qstfg ON qstf.group_id = qstfg.group_id
				WHERE qfr.response_id ="'.$response_id.'"
				ORDER BY qstfg.group_id, qstf.field_id';
		
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				
				$question_details['group_id'] = $result['group_id'];
				$question_details['group_name'] = $result['group_name'];
				$question_details['field_id'] = $result['field_id'];
				$question_details['field_name'] = $result['field_name'];
				$question_details['value'] = $result['value'];
			
					
				array_push($questions, $question_details);
			}
			return $questions;
	
		} else return $questions;
	
	}//end get_questions by survey id
	
	public function get_survey_repsonses_summary($survey_id){
		$responses = array();
		$response_details = array();
	
		$sql = "SELECT t1.employee_id, t2.role_id,t1.firstname, t1.surname, count(t2.id) AS filled 
				FROM employees AS t1 
				LEFT JOIN 
				(SELECT s.survey_id, e.employee_id, u.role_id, qfr.date_added, e.firstname, e.surname, qfr.id 
					FROM surveys s 
					INNER JOIN quest_field_responses qfr ON s.survey_id=qfr.survey_id
					LEFT JOIN users u ON qfr.user_id = u.user_id
					LEFT JOIN employees e ON u.employee_id = e.employee_id
					WHERE s.survey_id=$survey_id AND qfr.deleted = 0 AND qfr.user_id NOT IN (1) AND u.role_id=3) AS t2 
				ON t1.employee_id = t2.employee_id
				GROUP BY t1.employee_id ORDER BY filled DESC;";
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$response_details['employee_id'] = $result['employee_id'];
				$response_details['role_id'] = $result['role_id'];
				$response_details['employee'] = $result['firstname'].' '.$result['surname'];
				$response_details['filled'] = $result['filled'];
	
				array_push($responses, $response_details);
			}
			return $responses;
	
		} else return $responses;
	
	
	}//end get_get_all_surveys
	
	public function get_responses_summary($survey_id){
		$questions = array();
		$question_details = array();
	
	
		$sql = 'SELECT  qstfg.group_id, qstf.field_id, qstfg.group_name, qstf.field_name, qstf.field_type
				FROM
				quest_fields qstf
				LEFT JOIN quest_field_groups qstfg ON qstf.group_id = qstfg.group_id
				WHERE qstf.questionnaire_id ="'.$survey_id.'"
				ORDER BY qstfg.group_id, qstf.field_id';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
	
				$question_details['group_id'] = $result['group_id'];
				$question_details['group_name'] = $result['group_name'];
				$question_details['field_id'] = $result['field_id'];
				$question_details['field_name'] = $result['field_name'];
				$question_details['field_type'] = $result['field_type'];
				array_push($questions, $question_details);
			}
			return $questions;
	
		} else return $questions;
	
	}//end 
	
	public function get_survey_summary_report($survey_id){
		$questions = array();
		$question_details = array();
	
	
		$sql = "SELECT 
				    qfr.id, u.username, qfr.date_added, qf.field_id, qf.field_name, qfrs.value
				FROM
				    quest_field_responses qfr
				INNER JOIN users u on u.user_id = qfr.user_id
				INNER JOIN surveys s on s.survey_id = qfr.survey_id
				INNER JOIN quest_field_results qfrs ON qfrs.response_id = qfr.id
				INNER JOIN quest_fields qf ON qf.field_id = qfrs.field_id
				
				
				WHERE
				    qfr.survey_id = '$survey_id' AND qfr.deleted = 0 AND qfr.user_id NOT IN ('1');";
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
	
				$question_details['response_id'] = $result['id'];
				$question_details['username'] = $result['username'];
				$question_details['date_added'] = $result['date_added'];
				$question_details['field_id'] = $result['field_id'];
				$question_details['field_name'] = $result['field_name'];
				$question_details['value'] = $result['value'];
				array_push($questions, $question_details);
			}
			return $questions;
	
		} else return $questions;
	
	}//end
	public function get_question_summary($field_id, $survey_id, $choicegroup=false){
		$questions = array();
		$question_details = array();
	
		if($choicegroup){
		$sql = 'SELECT qfr.field_id, qfr.value, COUNT(qfr.value) as number, qfr.response_id
				FROM
				quest_field_results qfr
				INNER JOIN quest_field_responses qfrs ON qfrs.id = qfr.response_id
				WHERE qfr.field_id="'.$field_id.'"
				AND qfrs.survey_id ="'.$survey_id.'"
				AND qfrs.deleted=0 AND qfrs.user_id NOT IN(1)
				GROUP BY value
				ORDER BY number desc';  
		}else{
			$sql = 'SELECT qfr.field_id, qfr.value, qfr.response_id
				FROM
				quest_field_results qfr
				INNER JOIN quest_field_responses qfrs ON qfrs.id = qfr.response_id
				WHERE qfr.field_id="'.$field_id.'"
				AND qfrs.survey_id ="'.$survey_id.'"
				AND qfrs.deleted=0 AND qfrs.user_id NOT IN(1)';
		}
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$question_details['field_id'] = $result['field_id'];
				$question_details['value'] = $result['value'];
				if ($result['number'])$question_details['number'] = $result['number'];
				$question_details['response_id'] = $result['response_id'];
				array_push($questions, $question_details);
			}
			return $questions;
	
		} else return $questions;
	
	}//end
	public function get_resonse_name($response_id){
		$questions = '';
		$question_details = array();
	
	
		$sql = 'SELECT qfr.value
				FROM
				quest_field_results qfr
				WHERE qfr.field_id = 2 
				AND qfr.response_id ="'.$response_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
	
				$questions = $result['value'];
				
			}
			return $questions;
	
		} else return $questions;
	
	}//end
}

/** Default: Initialize class for use */
$Surveys = new Surveys();
?>