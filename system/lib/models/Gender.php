<?php
/* */
class Gender extends DB{
	
	/**
	 * 
	 * 
	 * Gender functions
	 */
	public function save_gender($description, $added_by, $date_added){
		
		
		$sql_checkgenders ="SELECT * FROM gender WHERE description ='{$description}'";
		try{
			//run query
			$rs = DB::query($sql_checkgenders);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if user with same username exists
		if(!empty($rs)){
			return false;
		} else{
		$sql1 = "INSERT INTO `gender`
					(
					`description`,
					`date_added`,
					`date_updated`,
					`added_by`)
					VALUES
					(
					'{$description}',
					'{$date_added}',
					'{$date_added}',
					'{$added_by}'
					)";
	
	
		try{
			//run query
			$rs = DB::query($sql1);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
	
		return true;
		}
	}//End Save Gender
	public function get_all_genders(){
		$genders = array();
		$gender_details = array();
		
		$sql = 'SELECT * 
				FROM
				gender
				ORDER BY description';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$gender_details['id'] = $result['id'];
				$gender_details['description'] = $result['description'];
				
				array_push($genders, $gender_details);
			}
			return $genders;

		} else return $genders;
	
		
	}//end all users
	public function get_gender_by_id($gender_id){
	
		$gender_details = array();
	
		$sql = 'SELECT *
				FROM
				gender WHERE id= "'.$gender_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$gender_details['id'] = $result['id'];
				$gender_details['description'] = $result['description'];
				
			}
			return $gender_details;
	
		} else return $gender_details;
	
	
	}//end GET GDNDER BY ID
	
	//UPDATE GENDER
	public function update_gender($gender_id,$description,$added_by, $date_updated){
	
		
		$sql1 = "UPDATE `gender`
				SET
				`description` = '{$description}',
				`date_updated` = '{$date_updated}',
				`updated_by` = {$added_by} WHERE `id` = {$gender_id};";
		try{
			//run query
			$rs1 = DB::query($sql1);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			return false;
		}
		return true;
	}//end UPDATE GENDER

}


/** Default: Initialize class for use */
$Gender = new Gender();
?>