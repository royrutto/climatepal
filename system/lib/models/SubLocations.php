<?php
/* */
class SubLocation extends DB{
	
	/**
	 * 
	 * 
	 * SubLocation functions
	 */
	public function save_sublocation($description, $location, $added_by, $date_added){
		
		
		$sql_checksublocations ="SELECT * FROM sublocations WHERE sublocation ='{$description}'";
		try{
			//run query
			$rs = DB::query($sql_checksublocations);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if user with same username exists
		if(!empty($rs)){
			return false;
		} else{
		$sql1 = "INSERT INTO `sublocations` (`sublocation` ,`location` ,`date_added` ,`date_updated` ,`added_by` ,`updated_by`) VALUES ('{$description}','{$location}','{$date_added}','{$date_added}','{$added_by}','{$added_by}')";
	
	
		try{
			//run query
			$rs = DB::query($sql1);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
	
		return true;
		}
	}//End Save SubLocation
	public function get_all_sublocations(){
		$sublocations = array();
		$sublocation_details = array();
		
		$sql = 'select sublocations.id as lID, sublocations.sublocation as name, locations.location as loc from sublocations, locations WHERE sublocations.location = locations.id ORDER BY sublocations.id;';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$sublocation_details['id'] = $result['lID'];
				$sublocation_details['name'] = $result['name'];
				$sublocation_details['location'] = $result['loc'];
				
				array_push($sublocations, $sublocation_details);
			}
			return $sublocations;

		} else return $sublocations;
	
		
	}//end all users
	public function get_sublocation_by_id($sublocation_id){
	
		$sublocation_details = array();
	
		$sql = 'select sublocations.id as lID, sublocations.sublocation as name, locations.id as dID, locations.location as loc from sublocations, locations WHERE sublocations.location = locations.id AND sublocations.id= "'.$sublocation_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$sublocation_details['id'] = $result['lID'];
				$sublocation_details['locate'] = $result['dID'];
				$sublocation_details['name'] = $result['name'];
				$sublocation_details['location'] = $result['loc'];
				
			}
			return $sublocation_details;
	
		} else return $sublocation_details;
	
	
	}//end GET LOCATION BY ID
	
	//UPDATE LOCATION
	public function update_sublocation($sublocation_id,$description, $location, $added_by, $date_updated){
	
		
		$sql1 = "UPDATE `sublocations` SET `sublocation` = '{$description}',`location` = '{$location}',`date_updated` = '{$date_updated}',`updated_by` = {$added_by} WHERE `id` = {$sublocation_id};";
		try{
			//run query
			$rs1 = DB::query($sql1);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			return false;
		}
		return true;
	}//end UPDATE LOCATION
	
	public function get_all_locations(){
		$locations = array();
		$location_details = array();
		
		$sql = 'SELECT * 
				FROM
				locations
				ORDER BY id';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$location_details['id'] = $result['id'];
				$location_details['name'] = $result['location'];
				
				array_push($locations, $location_details);
			}
			return $locations;

		} else return $locations;
	
		
	}//end all locations

}


/** Default: Initialize class for use */
$SubLocation = new SubLocation();
?>