<?php
/* */
class Household extends DB{
	
	/**
	 * 
	 * 
	 * Household functions
	 */
	public function save_household($name, $description, $added_by, $date_added){
		
		
		$sql_check_households ="SELECT * FROM households WHERE household_name ='{$name}'";
		try{
			//run query
			$rs = DB::query($sql_check_households);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if user with same username exists
		if(!empty($rs)){
			return false;
		} else{
		$sql1 = "INSERT INTO `households`
					(
					`household_name`,
					`description`,
					`status`,
					`date_added`,
					`date_updated`,
					`added_by`,
					`updated_by`)
					VALUES
					(
					'{$name}',
					'{$description}',
					1,
					'{$date_added}',
					'{$date_added}',
					'{$added_by}',
					'{$added_by}'
					)";
	
	
		try{
			//run query
			$rs = DB::query($sql1);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
	
		return true;
		}
	}//End Save Household
	public function get_all_households(){
		$households = array();
		$household_details = array();
		
		$sql = 'SELECT * 
				FROM
				households
				WHERE deleted=0
				ORDER BY id';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$household_details['id'] = $result['id'];
				$household_details['name'] = $result['name'];
				$household_details['phone'] = $result['phone'];
				$household_details['gender'] = $result['gender'];
				$household_details['no'] = $result['id_number'];
				$household_details['old'] = $result['old_stove'];
				$household_details['household'] = $result['surname'];
				if ($result['latitude']=='' || $result['latitude']==null){
					$household_details['latitude'] = '--';
				}else{
					$household_details['latitude'] = $result['latitude'];
				}
				if ($result['longitude']=='' || $result['longitude']==null){
					$household_details['longitude'] = '--';
				}else{
					$household_details['longitude'] = $result['longitude'];
				}
				
				
			
				if ($result['gender']==1){
					$household_details['sex'] = 'Male';
				}else if ($result['gender']==1){
					$household_details['sex'] = 'Female';
				}else $household_details['sex'] = "--";
				
				array_push($households, $household_details);
			}
			return $households;

		} else return $households;
	
		
	}//end all users
	public function get_household_by_id($household_id){
	
		$household_details = array();
	
		$sql = 'SELECT *
				FROM
				households WHERE household_id= "'.$household_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$household_details['household_id'] = $result['household_id'];
				$household_details['household_name'] = $result['household_name'];
				$household_details['description'] = $result['description'];
				$household_details['status'] = $result['status'];
				
			}
			return $household_details;
	
		} else return $household_details;
	
	
	}//end GET GDNDER BY ID
	
	//UPDATE ROLE
	public function update_households($household_id, $name, $description, $status,$added_by, $date_updated){
	
		
		$sql1 = "UPDATE `households`
				SET
				`household_name` = '{$name}',
				`description` = '{$description}',
				`status` = '{$status}',
				`date_updated` = '{$date_updated}',
				`updated_by` = {$added_by} WHERE `household_id` = {$household_id};";
		try{
			//run query
			$rs1 = DB::query($sql1);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			return false;
		}
		return true;
	}//end UPDATE ROLE

}


/** Default: Initialize class for use */
$Household = new Household();
?>