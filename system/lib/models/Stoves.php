<?php
/* */
class Stove extends DB{
	
	/**
	 * 
	 * 
	 * Stove functions
	 */
	public function save_stove($name, $description, $added_by, $date_added){
		
		
		$sql_check_stoves ="SELECT * FROM stoves WHERE stove_name ='{$name}'";
		try{
			//run query
			$rs = DB::query($sql_check_stoves);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if user with same username exists
		if(!empty($rs)){
			return false;
		} else{
		$sql1 = "INSERT INTO `stoves`
					(
					`stove_name`,
					`description`,
					`status`,
					`date_added`,
					`date_updated`,
					`added_by`,
					`updated_by`)
					VALUES
					(
					'{$name}',
					'{$description}',
					1,
					'{$date_added}',
					'{$date_added}',
					'{$added_by}',
					'{$added_by}'
					)";
	
	
		try{
			//run query
			$rs = DB::query($sql1);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
	
		return true;
		}
	}//End Save Stove
	public function get_all_stoves(){
		$stoves = array();
		$stove_details = array();
		
		$sql = 'SELECT * 
				FROM
				distributions
				ORDER BY id';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$stove_details['id'] = $result['id'];
				$stove_details['cpa'] = $result['cpa_number'];
				$stove_details['serial'] = $result['stove_serial'];
				$stove_details['dist'] = $result['dist_date'];
				$stove_details['by'] = $result['added_by'];
				if ($result['added_by']==1){
					$stove_details['added'] = 'Kitsao Em';
				}else $stove_details['added'] = 'Roy Rutto';
				
				array_push($stoves, $stove_details);
			}
			return $stoves;

		} else return $stoves;
	
		
	}//end all users
	public function get_stove_by_id($stove_id){
	
		$stove_details = array();
	
		$sql = 'SELECT *
				FROM
				stoves WHERE stove_id= "'.$stove_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$stove_details['stove_id'] = $result['stove_id'];
				$stove_details['stove_name'] = $result['stove_name'];
				$stove_details['description'] = $result['description'];
				$stove_details['status'] = $result['status'];
				
			}
			return $stove_details;
	
		} else return $stove_details;
	
	
	}//end GET GDNDER BY ID
	
	//UPDATE ROLE
	public function update_stoves($stove_id, $name, $description, $status,$added_by, $date_updated){
	
		
		$sql1 = "UPDATE `stoves`
				SET
				`stove_name` = '{$name}',
				`description` = '{$description}',
				`status` = '{$status}',
				`date_updated` = '{$date_updated}',
				`updated_by` = {$added_by} WHERE `stove_id` = {$stove_id};";
		try{
			//run query
			$rs1 = DB::query($sql1);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			return false;
		}
		return true;
	}//end UPDATE ROLE

}


/** Default: Initialize class for use */
$Stove = new Stove();
?>