<?php
/* */
class Permission extends DB{
	
	/**
	 * 
	 * 
	 * Permission functions
	 */
	public function save_permission($description, $added_by, $date_added){
		
		
		$sql_checkpermissions ="SELECT * FROM permissions WHERE description ='{$description}'";
		try{
			//run query
			$rs = DB::query($sql_checkpermissions);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if user with same username exists
		if(!empty($rs)){
			return false;
		} else{
		$sql1 = "INSERT INTO `permissions`
					(
					`description`,
					`date_added`,
					`date_updated`,
					`added_by`)
					VALUES
					(
					'{$description}',
					'{$date_added}',
					'{$date_added}',
					'{$added_by}'
					)";
	
	
		try{
			//run query
			$rs = DB::query($sql1);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
	
		return true;
		}
	}//End Save Permission
	public function get_all_permissions(){
		$permissions = array();
		$permission_details = array();
		
		$sql = 'SELECT * 
				FROM
				permissions
				ORDER BY description';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$permission_details['id'] = $result['id'];
				$permission_details['description'] = $result['description'];
				
				array_push($permissions, $permission_details);
			}
			return $permissions;

		} else return $permissions;
	
		
	}//end all users
	public function get_permission_by_id($permission_id){
	
		$permission_details = array();
	
		$sql = 'SELECT *
				FROM
				permissions WHERE id= "'.$permission_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$permission_details['id'] = $result['id'];
				$permission_details['description'] = $result['description'];
				
			}
			return $permission_details;
	
		} else return $permission_details;
	
	
	}//end GET GDNDER BY ID
	
	//UPDATE GENDER
	public function update_permission($permission_id,$description,$added_by, $date_updated){
	
		
		$sql1 = "UPDATE `permissions`
				SET
				`description` = '{$description}',
				`date_updated` = '{$date_updated}',
				`updated_by` = {$added_by} WHERE `id` = {$permission_id};";
		try{
			//run query
			$rs1 = DB::query($sql1);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			return false;
		}
		return true;
	}//end UPDATE GENDER

}


/** Default: Initialize class for use */
$Permission = new Permission();
?>