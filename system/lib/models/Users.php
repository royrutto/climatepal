<?php
/* */
class Users extends DB{
	
	/**
	 * 
	 * @param Username $username
	 * @param Passowrd $password
	 * @return string
	 * 
	 * Checks login details for authentication
	 */
	public function login($username){
		if(!empty($username)){
			
			$sql = 'SELECT * 
					FROM users 
					WHERE 
					username="'.$username.'"';
			
			try{
				//run query
				$rs = DB::query($sql);
				
			}catch(MeekroDBException $e) {
				return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
				echo "Error: " . $e->getMessage() . "<br>\n";
				echo "SQL Query: " . $e->getQuery() . "<br>\n";
			}
			//check if result is not empty		
			if(!empty($rs)){
				return true;
			} else return false;
				
		} else{
			return false;
		}
	}//end check login function
	
	/**
	 * 
	 * @param unknown $session_id
	 * @param unknown $user_id
	 * @param unknown $created_date
	 * @param unknown $last_active_date
	 * @param unknown $ip_address
	 * @return boolean|string
	 * 
	 * Creates session record in database
	 */
	public function create_session($session_id, $user_id, $created_date, $last_active_date, $ip_address){
		if(!empty($session_id) && !empty($user_id) && !empty($created_date) && !empty($last_active_date) && !empty($ip_address)){
				
			$sql = "INSERT INTO 
					`sessions` 
					(`session_id`, `user_id`, `created_date`, `last_active_date`, `is_expired`, `ip_address`) 
					
					VALUES ('".$session_id."', '".$user_id."', '".$created_date."', '".$last_active_date."', 0, '".$ip_address."')";
				
			try{
				//run query
				$rs = DB::query($sql);
				return true;
	
			}catch(MeekroDBException $e) {
				return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
				echo "Error: " . $e->getMessage() . "<br>\n";
				echo "SQL Query: " . $e->getQuery() . "<br>\n";
				return false;
			}
			return true;
			
		} else{
			return false;
		}
	}//end create sesssion
	
	/**
	 * 
	 * @param Session ID $session_id
	 * @param Last Active Date $last_active_date
	 * @return boolean|string
	 * 
	 * Ends an active session in database
	 */
	public function end_session($session_id, $last_active_date){
		if(!empty($session_id) && !empty($last_active_date)){
	
			$sql = "UPDATE 
					`sessions` 
					SET `last_active_date`='".$last_active_date."', `is_expired`= 1 
					WHERE `session_id`='".$session_id."'";
	
			try{
				//run query
				$rs = DB::query($sql);
				return true;
	
			}catch(MeekroDBException $e) {
				return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
				echo "Error: " . $e->getMessage() . "<br>\n";
				echo "SQL Query: " . $e->getQuery() . "<br>\n";
				return false;
			}
			return true;
				
		} else{
			return false;
		}
	}//end end_sesssion
	/**
	 * 
	 * @param Username $username
	 * @param Password $password
	 * @return string|multitype:Ambigous <>
	 * 
	 * Get user details using username and password
	 */
	public function get_user_details($username){
		
		$user_details = array();
		
		if(!empty($username)){
				
			$sql = 'SELECT *
					FROM users
					WHERE
					username="'.$username.'"';
				
			try{
				//run query
				$rs = DB::query($sql);
	
			}catch(MeekroDBException $e) {
				return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
				echo "Error: " . $e->getMessage() . "<br>\n";
				echo "SQL Query: " . $e->getQuery() . "<br>\n";
			}
			
			$result = array();
			//check if result is not empty
			if(!empty($rs)){
				foreach($rs as $result){
					$user_details['user_id'] = $result['user_id'];
					$user_details['username'] = $result['username'];
					$user_details['password'] = $result['password'];
				}
				return $user_details;
				
			} else $user_details;
	
		} else{
			return $user_details;
		}
	}//end get_user_details
	/**
	 * 
	 * @param unknown $username
	 * @param unknown $password
	 * @return string|multitype:Ambigous <>
	 */
	public function get_all_users(){
		$users = array();
		$user_details = array();
		
		$sql = 'SELECT * 
				FROM
				users,employees, roles
				WHERE users.employee_id = employees.employee_id AND users.role_id = roles.role_id
				ORDER BY user_id';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$user_details['user_id'] = $result['user_id'];
				$user_details['username'] = $result['username'];
				$user_details['surname'] = $result['surname'];
				$user_details['firstname'] = $result['firstname'];
				$user_details['role_name'] = $result['role_name'];
				if ($result['flag']==1){
					$user_details['status'] = 'Active';
				}else $user_details['status'] = 'Disabled';
				
				array_push($users, $user_details);
			}
			return $users;

		} else return $users;
	
		
	}//end all users
	
	public function save_user($role_id, $username, $password, $flag, $surname, $firstname, $gender, $telephone,$postal_address,
					$postal_code, $town, $email, $added_by, $date_added, $locations){
		
		
		$sql_checkusers ="SELECT * FROM users WHERE username ='{$username}'";
		try{
			//run query
			$rs = DB::query($sql_checkusers);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if user with same username exists
		if(!empty($rs)){
			return false;
		} else{
		$sql1 = "INSERT INTO `employees`
					(
					`surname`,
					`firstname`,
					`gender`,
					`telephone`,
					`postal_address`,
					`postal_code`,
					`town`,
					`email`,
					`added_by`,
					`date_added`,
					`date_updated`)
					VALUES
					(
					'{$surname}',
					'{$firstname}',
					'{$gender}',
					'{$telephone}',
					'{$postal_address}',
					'{$postal_code}',
					'{$town}',
					'{$email}',
					'{$added_by} ',
					'{$date_added}',
					'{$date_added}'
					)";
	
	
		try{
			//run query
			$rs = DB::query($sql1);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		
		
		$sql = "SELECT employee_id FROM `employees` WHERE email ='{$email}'";
		
		try{
			//run query
			$rs = DB::query($sql);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$employee_id = $result['employee_id'];
			}
		}
			
		$sql2 = "INSERT INTO `users`
		(
		`employee_id`,
		`role_id`,
		`username`,
		`password`,
		`flag`,
		`date_added`,
		`date_updated`)
		VALUES
		(
		'{$employee_id}',
		'{$role_id}',
		'{$username}',
		'{$password}',
		'{$flag}',
		'{$date_added}',
		'{$date_added}'
		)";
		
		try{
			//run query
			$rs = DB::query($sql2);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		
		$location_ids = explode(',', $locations);
		
		foreach ($location_ids as $locationid){
			$sql3 = "INSERT INTO `location_permissions`
					(
					`employee_id`,
					`location_id`)
					VALUES
					(
						'{$employee_id}',
						'{$locationid}'
					)";
			
					try{
						//run query
						$rs = DB::query($sql3);
					}catch(MeekroDBException $e) {
						return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
						echo "Error: " . $e->getMessage() . "<br>\n";
						echo "SQL Query: " . $e->getQuery() . "<br>\n";
					}
		}
		return true;
		}
	}//End Save Users
	/**
	 * 
	 * @param User ID $user_id
	 * @return string|multitype:
	 * 
	 * Get user details by ID
	 */
	public function get_user_by_id($user_id){
	
		$user_details = array();
	
		$sql = 'SELECT *
				FROM
				users,employees, roles
				WHERE users.employee_id = employees.employee_id AND users.role_id = roles.role_id
				AND users.user_id  = "'.$user_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$user_details['user_id'] = $result['user_id'];
				$user_details['username'] = $result['username'];
				$user_details['surname'] = $result['surname'];
				$user_details['firstname'] = $result['firstname'];
				$user_details['role_name'] = $result['role_name'];
				$user_details['gender'] = $result['gender']; 
				$user_details['telephone'] = $result['telephone'];
				$user_details['postal_address'] = $result['postal_address'];
				$user_details['postal_code'] = $result['postal_code'];
				$user_details['town'] = $result['town'];
				$user_details['email'] = $result['email'];
				$user_details['username'] = $result['username'];
				$user_details['password'] = $result['role_name'];
				$user_details['role_id'] = $result['role_id'];
			}
			return $user_details;
	
		} else return $user_details;
	
	
	}//end GET USER BY ID
	
	public function update_user($user_id,$role_id, $username,$flag, $surname, $firstname, $gender, $telephone,$postal_address,
					$postal_code, $town, $email, $added_by, $date_updated,$locations){
	
		
		$sql1 = "UPDATE `users`
				SET
				`role_id` = '{$role_id}',
				`username` = '{$username}',
				`date_updated` = '{$date_updated}'
				WHERE `user_id` = '{$user_id}'";
		
		$sql2= "SELECT employee_id FROM `users` WHERE user_id ='{$user_id}'";
	
		try{
			//run query
			$rs1 = DB::query($sql1);
			$rs2 = DB::query($sql2);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			return false;
		}
		
		//check if result is not empty
		if(!empty($rs2)){
			foreach($rs2 as $result){
				
			$employee_id = $result['employee_id'];
		}
		
	
		} else $employee_id='';
		
		
		
		$sql3 = "UPDATE `employees`
		SET
		`surname` = '{$surname}',
		`firstname` = '{$firstname}',
		`gender` = '{$gender}',
		`telephone` = '{$telephone}',
		`postal_address` = '{$postal_address}',
		`postal_code` = '{$postal_code}',
		`town` = '{$town}',
		`email` = '{$email}',
		`date_updated` = '{$date_updated}'
		WHERE `employee_id` = '{$employee_id}'";
		
		try{
			//run query
			$rs1 = DB::query($sql3);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			return false;
		}
		
		
		$delete_existing = "DELETE FROM `location_permissions`
		WHERE employee_id={$employee_id}";
		try{
			//run query
			$rs = DB::query($delete_existing);
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$location_ids = explode(',', $locations);
		foreach ($location_ids as $locationid){
			
			
			$sql3 = "INSERT INTO `location_permissions`
			(
			`employee_id`,
			`location_id`)
			VALUES
			(
			'{$employee_id}',
			'{$locationid}'
			)";
				
			try{
			//run query
			$rs = DB::query($sql3);
			}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
						echo "Error: " . $e->getMessage() . "<br>\n";
					echo "SQL Query: " . $e->getQuery() . "<br>\n";
			}
			}
		
		
		return true;
	}//end UPDATE USER
	
	/**
	 * 
	 * @param unknown $user_id
	 * @return string|multitype:Ambigous <>
	 */
	public function get_userid_by_sessionid($session_id){
	
		$user_id = null;
	
		$sql = 'SELECT user_id
				FROM
				sessions
				WHERE session_id = "'.$session_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$user_id = $result['user_id'];
			}
			return $user_id;
	
		} else return $user_id;
	
	
	}//end GET USER BY SESSION ID
	
	public function get_all_roles(){
		$roles = array();
		$role_details = array();
	
		$sql = 'SELECT *
				FROM
				roles
				ORDER BY role_id';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$role_details['role_id'] = $result['role_id'];
				$role_details['role_name'] = $result['role_name'];
				$role_details['description'] = $result['description'];
				$role_details['status'] = $result['status'];
	
				array_push($roles, $role_details);
			}
			return $roles;
	
		} else return $roles;
	
	
	}//end all roles
	
	public function get_user_role_by_session_id($session_id){
	
		$user_details = array();
	
		$sql = 'SELECT s.user_id, u.role_id
				FROM
				sessions s inner join users u on u.user_id = s.user_id
				WHERE session_id = "'.$session_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$user_details['user_id'] = $result['user_id'];
				$user_details['role_id'] = $result['role_id'];
			}
			return $user_details['role_id'];
	
		} else return $user_details['role_id'];
	
	
	}//end GET ROLE ID
	public function get_all_locations(){
		$locations = array();
		$location_details = array();
	
		$sql = 'SELECT * FROM locations ORDER BY id;';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$location_details['id'] = $result['id'];
				$location_details['name'] = $result['location'];
	
				array_push($locations, $location_details);
			}
			return $locations;
	
		} else return $locations;
	
	
	}//end all locations
	public function get_all_allowed_locations($user_id){
		$locations = array();	
		$sql = "SELECT sl.id, sl.sublocation, sl.location, u.user_id, e.employee_id, lp.location_id
		FROM sublocations sl
		INNER JOIN locations l on sl.location = l.id
		INNER JOIN users u on u.user_id={$user_id}
		INNER JOIN employees e on u.employee_id = e.employee_id
		INNER JOIN location_permissions lp where lp.employee_id = e.employee_id and l.id=lp.location_id
		GROUP BY lp.location_id ORDER BY sl.id";
	
		try{
		//run query
		$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
		return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
				echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
		foreach($rs as $result){
			array_push($locations, $result['location_id']);
		}
		return $locations;
	
		} else return $locations;
	
	
		}//end all sub
		
		public function get_upload_info($user_id){
			$upload_data = array();
			$sql = "SELECT * FROM distributions WHERE added_by = '{$user_id}'";
			$sql2 = "SELECT * FROM quest_field_responses WHERE survey_id =1 AND user_id= '{$user_id}'";
			$sql3 = "SELECT * FROM quest_field_responses WHERE survey_id =2 AND user_id = '{$user_id}'";
		
			try{
			//run query
			$rs = DB::query($sql);
			$counter1 = DB::count();
			$rs = DB::query($sql2);
			$counter2 = DB::count();
			$rs = DB::query($sql3);
			$counter3 = DB::count();
			}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
					echo "SQL Query: " . $e->getQuery() . "<br>\n";
			}
				
		$result = array();
				//check if result is not empty
		
			$upload_data['distributions_uploaded'] =$counter1;
			$upload_data['baseline_uploaded'] =$counter2;
			$upload_data['monitoring_uploaded'] =$counter3;
			
		return $upload_data;
		
	
		}
		

}


/** Default: Initialize class for use */
$Users = new Users();
?>