<?php
/* */
class Location extends DB{
	
	/**
	 * 
	 * 
	 * Location functions
	 */
	public function save_location($description, $added_by, $date_added){
		
		
		$sql_checklocations ="SELECT * FROM locations WHERE location ='{$description}'";
		try{
			//run query
			$rs = DB::query($sql_checklocations);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if user with same username exists
		if(!empty($rs)){
			return false;
		} else{
		$sql1 = "INSERT INTO `locations` (`location` ,`date_added` ,`date_updated` ,`added_by` ,`updated_by`) VALUES ('{$description}','{$date_added}','{$date_added}','{$added_by}','{$added_by}')";
	
	
		try{
			//run query
			$rs = DB::query($sql1);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
	
		return true;
		}
	}//End Save Location
	public function get_all_locations(){
		$locations = array();
		$location_details = array();
		
		$sql = 'SELECT * FROM locations ORDER BY id;';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$location_details['id'] = $result['id'];
				$location_details['name'] = $result['location'];
				
				array_push($locations, $location_details);
			}
			return $locations;

		} else return $locations;
	
		
	}//end all users
	public function get_location_by_id($location_id){
	
		$location_details = array();
	
		$sql = 'select locations.id as lID, locations.location as name from locations WHERE locations.id= "'.$location_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$location_details['id'] = $result['lID'];
				$location_details['name'] = $result['name'];
				
			}
			return $location_details;
	
		} else return $location_details;
	
	
	}//end GET LOCATION BY ID
	
	//UPDATE LOCATION
	public function update_location($location_id,$added_by, $date_updated){
	
		
		$sql1 = "UPDATE `locations` SET `location` = '{$description}',`date_updated` = '{$date_updated}',`updated_by` = {$added_by} WHERE `id` = {$location_id};";
		try{
			//run query
			$rs1 = DB::query($sql1);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			return false;
		}
		return true;
	}//end UPDATE LOCATION
	
	/*public function get_all_districts(){
		$districts = array();
		$district_details = array();
		
		$sql = 'SELECT * 
				FROM
				districts
				ORDER BY id';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$district_details['id'] = $result['id'];
				$district_details['name'] = $result['district'];
				
				array_push($districts, $district_details);
			}
			return $districts;

		} else return $districts;
	
		
	}//end all districts*/

}


/** Default: Initialize class for use */
$Location = new Location();
?>