<?php
/* */
class StoveType extends DB{
	
	/**
	 * 
	 * 
	 * Gender functions
	 */
	public function save_stove_type($description, $added_by, $date_added){
		
		
		$sql_checkstovetypes ="SELECT * FROM stove_types WHERE description ='{$description}'";
		try{
			//run query
			$rs = DB::query($sql_checkstovetypes);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if user with same username exists
		if(!empty($rs)){
			return false;
		} else{
		$sql1 = "INSERT INTO `stove_types`
					(
					`description`,
					`date_added`,
					`date_updated`,
					`added_by`)
					VALUES
					(
					'{$description}',
					'{$date_added}',
					'{$date_added}',
					'{$added_by}'
					)";
	
	
		try{
			//run query
			$rs = DB::query($sql1);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
	
		return true;
		}
	}//End Save Gender
	public function get_all_stove_types(){
		$stove_types = array();
		$stove_type_details = array();
		
		$sql = 'SELECT * 
				FROM
				stove_types
				ORDER BY description';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$stove_type_details['id'] = $result['id'];
				$stove_type_details['description'] = $result['description'];
				
				array_push($stove_types, $stove_type_details);
			}
			return $stove_types;

		} else return $stove_types;
	
		
	}//end all users
	//GET STOVE TYPE BY ID
	public function get_stovetype_by_id($id){
	
		$stovetype_details = array();
	
		$sql = 'SELECT *
				FROM
				stove_types WHERE id= "'.$id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$stovetype_details['id'] = $result['id'];
				$stovetype_details['description'] = $result['description'];
				
			}
			return $stovetype_details;
	
		} else return $stovetype_details;
	
	
	}//end GET STOVE TYPE BY ID
	
	//UPDATE STOVE TYPE
	public function update_stove_type($id,$description,$added_by, $date_updated){
	
		
		$sql1 = "UPDATE `stove_types`
				SET
				`description` = '{$description}',
				`date_updated` = '{$date_updated}',
				`updated_by` = {$added_by} WHERE `id` = {$id};";
		try{
			//run query
			$rs1 = DB::query($sql1);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			return false;
		}
		return true;
	}//end UPDATE STOVE TYPE

}


/** Default: Initialize class for use */
$StoveType = new StoveType();
?>