<?php
/* */
class Role extends DB{
	
	/**
	 * 
	 * 
	 * Role functions
	 */
	public function save_role($name, $description, $added_by, $date_added){
		
		
		$sql_check_roles ="SELECT * FROM roles WHERE role_name ='{$name}'";
		try{
			//run query
			$rs = DB::query($sql_check_roles);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if user with same username exists
		if(!empty($rs)){
			return false;
		} else{
		$sql1 = "INSERT INTO `roles`
					(
					`role_name`,
					`description`,
					`status`,
					`date_added`,
					`date_updated`,
					`added_by`,
					`updated_by`)
					VALUES
					(
					'{$name}',
					'{$description}',
					1,
					'{$date_added}',
					'{$date_added}',
					'{$added_by}',
					'{$added_by}'
					)";
	
	
		try{
			//run query
			$rs = DB::query($sql1);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
	
		return true;
		}
	}//End Save Role
	public function get_all_roles(){
		$roles = array();
		$role_details = array();
		
		$sql = 'SELECT * 
				FROM
				roles
				ORDER BY role_id';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$role_details['role_id'] = $result['role_id'];
				$role_details['role_name'] = $result['role_name'];
				$role_details['description'] = $result['description'];
				$role_details['status'] = $result['status'];
				
				array_push($roles, $role_details);
			}
			return $roles;

		} else return $roles;
	
		
	}//end all users
	public function get_role_by_id($role_id){
	
		$role_details = array();
	
		$sql = 'SELECT *
				FROM
				roles WHERE role_id= "'.$role_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$role_details['role_id'] = $result['role_id'];
				$role_details['role_name'] = $result['role_name'];
				$role_details['description'] = $result['description'];
				$role_details['status'] = $result['status'];
				
			}
			return $role_details;
	
		} else return $role_details;
	
	
	}//end GET GDNDER BY ID
	
	//UPDATE ROLE
	public function update_roles($role_id, $name, $description, $status,$added_by, $date_updated){
	
		
		$sql1 = "UPDATE `roles`
				SET
				`role_name` = '{$name}',
				`description` = '{$description}',
				`status` = '{$status}',
				`date_updated` = '{$date_updated}',
				`updated_by` = {$added_by} WHERE `role_id` = {$role_id};";
		try{
			//run query
			$rs1 = DB::query($sql1);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			return false;
		}
		return true;
	}//end UPDATE ROLE

}


/** Default: Initialize class for use */
$Role = new Role();
?>