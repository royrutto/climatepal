<?php
/* */
class District extends DB{
	
	/**
	 * 
	 * 
	 * District functions
	 */
	public function save_district($description, $lat, $long, $added_by, $date_added){
		
		
		$sql_checkdistricts ="SELECT * FROM districts WHERE district ='{$description}'";
		try{
			//run query
			$rs = DB::query($sql_checkdistricts);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if user with same username exists
		if(!empty($rs)){
			return false;
		} else{
		$sql1 = "INSERT INTO `districts` (`district` ,`latitude` ,`longitude` ,`date_added` ,`date_updated` ,`added_by` ,`updated_by`)
					VALUES ('{$description}','{$lat}','{$long}','{$date_added}','{$date_added}','{$added_by}','{$added_by}')";
	
	
		try{
			//run query
			$rs = DB::query($sql1);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
	
		return true;
		}
	}//End Save District
	public function get_all_districts(){
		$districts = array();
		$district_details = array();
		
		$sql = 'SELECT * 
				FROM
				districts
				ORDER BY id';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$district_details['id'] = $result['id'];
				$district_details['name'] = $result['district'];
				$district_details['latitude'] = $result['latitude'];
				$district_details['longitude'] = $result['longitude'];
				
				array_push($districts, $district_details);
			}
			return $districts;

		} else return $districts;
	
		
	}//end all users
	public function get_district_by_id($district_id){
	
		$district_details = array();
	
		$sql = 'SELECT *
				FROM
				districts WHERE id= "'.$district_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$district_details['id'] = $result['id'];
				$district_details['name'] = $result['district'];
				$district_details['latitude'] = $result['latitude'];
				$district_details['longitude'] = $result['longitude'];
				
			}
			return $district_details;
	
		} else return $district_details;
	
	
	}//end GET GDNDER BY ID
	
	//UPDATE GENDER
	public function update_district($district_id,$description, $lat, $long,$added_by, $date_updated){
	
		
		$sql1 = "UPDATE `districts` SET `district` = '{$description}',`latitude` = '{$lat}',`longitude` = '{$long}',`date_updated` = '{$date_updated}',`updated_by` = {$added_by} WHERE `id` = {$district_id};";
		try{
			//run query
			$rs1 = DB::query($sql1);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			return false;
		}
		return true;
	}//end UPDATE GENDER

}


/** Default: Initialize class for use */
$District = new District();
?>