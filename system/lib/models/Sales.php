<?php
/* */
class Sales extends DB{
	
	/*SALES FUNCTIONS*/
	//SAVE DISTRIBUTION
	public function save_sale($cpa, $serial, $date, $name, $middlename, $householdname,$gender, $id, $mobile, $location, $latitude, $longitude, $old, $added_by, $date_added){
		$sql_checkhouseholds ="SELECT * FROM households WHERE name ='{$name}' AND id_number='{$id}'";
		try{
			//run query
			$rs = DB::query($sql_checkhouseholds);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if household with same name and id exists
		if(!empty($rs)){
			return false;
		} else{
		$sql1 = "INSERT INTO `households` (`id` ,`name` ,`middlename` ,`surname` ,`gender` ,`id_number` ,`phone` ,`location` ,`old_stove` ,`latitude` ,`longitude` ,`date_added` ,`date_updated` ,`added_by` ,`updated_by`) 
		VALUES (NULL ,  '{$name}',  '{$middlename}', '{$householdname}' ,'{$gender}',  '{$id}',  '{$mobile}',  '{$location}', '{$old}',  '{$latitude}',  '{$longitude}',  '{$date_added}',  '{$date_added}',  '{$added_by}',  '{$added_by}');";
	
	
		try{
			//run query
			$rs = DB::query($sql1);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		//get id of household just inserted
// 		$mysql_link = mysql_connect("localhost", "root", "root"); 
// 		mysql_select_db("climatepal") or die("Could not select database");
// 		$last_query = mysql_query("SELECT MAX(id) as maximum FROM households WHERE name ='{$name}' AND id_number='{$id}';",$mysql_link) or die(mysql_error());
// 		$last_values = mysql_fetch_assoc($last_query);
// 		$last_row = $last_values['maximum'];
		
		try{
			//run query
			$rs = DB::query("SELECT MAX(id) as maximum FROM households WHERE name ='{$name}' AND id_number='{$id}'");
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if household with same name and id exists
		if(!empty($rs)){
		foreach($rs as $result){
					$last_row = $result['maximum'];
				}
		}
		//end get id of household
		//Check if distribution already exists
		$sql = "SELECT * FROM  `distributions` WHERE household ='{$last_row}' AND stove_serial = '{$serial}';";
		
		try{
			//run query
			$rs = DB::query($sql);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			return false;
		}else{
			
		$sql2 = "INSERT INTO  `distributions` (`id` ,`cpa_number` ,`household` ,`stove_serial` ,`dist_date` ,`added_by` ,`updated_by` ,`date_added` ,`date_updated`) VALUES (NULL ,  '{$cpa}',  '{$last_row}',  '{$serial}',  '{$date}',  '{$added_by}',  '{$added_by}',  '{$date_added}',  '{$date_added}');";
		
		try{
			//run query
			$rs = DB::query($sql2);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			}
		}
	
		return true;
		}
	}//END SAVE DISTRIBUTION
	//Get Sale by ID
	public function get_sale_by_id($sale_id){
	
		$district_details = array();
	
		$sql = 'SELECT *
				FROM
				districts WHERE id= "'.$district_id.'"';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$district_details['id'] = $result['id'];
				$district_details['name'] = $result['district'];
				$district_details['latitude'] = $result['latitude'];
				$district_details['longitude'] = $result['longitude'];
				
			}
			return $district_details;
	
		} else return $district_details;
	
	
	}//end GET SALE BY ID
	public function get_all_sales(){
		$sales = array();
		$sale_details = array();
		
// 		$sql = 'select households.id as hh, households.name as nn, households.phone as mobi, distributions.cpa_number as cpa, distributions.stove_serial as serial_no, distributions.dist_date as distribute, stove_types.description as describ FROM households, distributions, stove_types 
// 				WHERE households.id=distributions.household AND households.old_stove=stove_types.id ORDER BY households.id;';

		$sql = 'SELECT
				dist.id as id, 
				hsholds.name as stove_user_name, 
				hsholds.phone as phone,
				dist.cpa_number as cpa,
				dist.dist_date as dist_date, 
				dist.stove_serial as stove_serial, 
				dist.dist_date as date_distributed,
				dist.sub_location_name as sublocation,
				dist.fo_name as fo
				
				FROM households hsholds
				INNER JOIN distributions dist ON hsholds.id = dist.household 
				WHERE hsholds.deleted=0
				ORDER BY dist.id;';
		
		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				
				$sale_details['id'] = $result['id'];
				$sale_details['name'] = $result['stove_user_name'];
				$sale_details['phone'] = $result['phone'];
				$sale_details['cpa'] = $result['cpa'];
				$sale_details['dist_date'] = $result['dist_date'];
				$sale_details['stove_serial'] = $result['stove_serial'];
				$sale_details['sublocation'] = $result['sublocation'];
				$sale_details['fo'] = $result['fo'];
				
				array_push($sales, $sale_details);
			}
			return $sales;

		} else return $sales;
	
		
	}//end all sales
	//GET GENDER
	public function get_all_gender(){
		$gender = array();
		$gender_details = array();
		
		$sql = 'SELECT * FROM gender ORDER BY id;';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$gender_details['id'] = $result['id'];
				$gender_details['name'] = $result['description'];
								
				array_push($gender, $gender_details);
			}
			return $gender;

		} else return $gender;
	
		
	}//end all gender
	
	//GET STOVE TYPES
	public function get_all_stovetypes(){
		$stovetype = array();
		$stovetype_details = array();
		
		$sql = 'SELECT * FROM stove_types ORDER BY id;';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$stovetype_details['id'] = $result['id'];
				$stovetype_details['name'] = $result['description'];
								
				array_push($stovetype, $stovetype_details);
			}
			return $stovetype;

		} else return $stovetype;
	
		
	}//end all stovetype
	
	//GET LOCATIONS
	public function get_all_allowed_sublocations($user_id){
		$sub = array();
		$sub_details = array();
		
		$sql = "SELECT sl.id, sl.sublocation, sl.location, u.user_id, e.employee_id
				FROM sublocations sl
				INNER JOIN locations l on sl.location = l.id
				INNER JOIN users u on u.user_id={$user_id}
				INNER JOIN employees e on u.employee_id = e.employee_id
				INNER JOIN location_permissions lp where lp.employee_id = e.employee_id and l.id=lp.location_id
				ORDER BY sl.id";

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$sub_details['id'] = $result['id'];
				$sub_details['name'] = $result['sublocation'];
								
				array_push($sub, $sub_details);
			}
			return $sub;

		} else return $sub;
	
		
	}//end all sub
	
	public function getNewDistUIData($option, $user_id=null){
		
		if(!empty($option)){
			switch ($option){
				case 'locations':
					$rs =  $this->get_all_allowed_sublocations($user_id);
				break;
				case 'stovetypes':
					$rs = $this->get_all_stovetypes();
				break;
				
			}	
			//result
			$result = array();
			if(!empty($rs)){
				$html = '';
				foreach($rs as $result){
				$html .= '<option value="'.$result['id'].'">'.$result['name'].'</option>';
				}
				return $html;
			}
		}//end if option provided
	}//end GET_UI_DATA function
	
	public function upload_excel_data($excel_array, $added_by, $date_added){
		
		$count=0;
		foreach ($excel_array as $rowdata){
			$count++;
			
			
			
			 $housholdname= $rowdata['B'];
			 $id_number = $rowdata['C'];
			 $phone_number = $rowdata['D'];
			 $location = $rowdata['E'];
			 $sub_location = $rowdata['F'];
			 $date = $rowdata['G'];
			 $field_officer = $rowdata['H'];
			 $stove_serial = $rowdata['I'];
			 
			
			$sql1 = "INSERT INTO `households` (`name` ,`id_number` ,`phone` ,`added_by`) 
			VALUES ( '{$housholdname}',  '{$id_number}',  '{$phone_number}', '{$added_by}');";
			
			
			try{
				//run query
				$rs = DB::query($sql1);
			
			}catch(MeekroDBException $e) {
				return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
				echo "Error: " . $e->getMessage() . "<br>\n";
				echo "SQL Query: " . $e->getQuery() . "<br>\n";
			}
			
			try{
				//run query
				$rs = DB::query("SELECT MAX(id) as maximum FROM households WHERE id_number='{$id_number}'");
			
			}catch(MeekroDBException $e) {
				return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
				echo "Error: " . $e->getMessage() . "<br>\n";
				echo "SQL Query: " . $e->getQuery() . "<br>\n";
			}
			$result = array();
			//check if household with same name and id exists
			if(!empty($rs)){
				foreach($rs as $result){
					$last_row = $result['maximum'];
				}
			}
			
			
			$sql2 = "INSERT INTO  `distributions` (`household` ,`stove_serial` ,`dist_date` ,`added_by` ,`updated_by` ,`date_added` ,`date_updated`, `sub_location_name`, `fo_name`) 
			VALUES ('{$last_row}',  '{$stove_serial}',  '{$date}',  '{$added_by}',  '{$added_by}',  '{$date_added}',  '{$date_added}',  '{$sub_location}',  '{$field_officer}');";
			
			try{
				//run query
				$rs = DB::query($sql2);
			
			}catch(MeekroDBException $e) {
				return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
				echo "Error: " . $e->getMessage() . "<br>\n";
				echo "SQL Query: " . $e->getQuery() . "<br>\n";
			}
			
			
		}
			return $count;
	}
	
	
	public function delete_records($delete_ids, $deleted_by, $date_updated){
	
	
			$sql1 = "UPDATE `households` 
					SET 
					`deleted`=1, 
					`date_updated`='{$date_updated}',
					`updated_by`={$deleted_by}
					WHERE `id` IN (".$delete_ids.");";
				
				
			try{
			//run query
				$rs = DB::query($sql1);
					
			}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
				echo "Error: " . $e->getMessage() . "<br>\n";
				echo "SQL Query: " . $e->getQuery() . "<br>\n";
			}
			
			if(!empty($rs)){
				return true;
			}
								
	return false;
	}

}


/** Default: Initialize class for use */
$Sales = new Sales();
?>