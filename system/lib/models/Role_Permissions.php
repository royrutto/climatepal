<?php
/* */
class RolePermission extends DB{
	
	/**
	 * 
	 * 
	 * Role functions
	 */
	public function save_role_permission($role, $permission, $added_by, $date_added){
		
		
		$sql_check_role_permissions ="SELECT * FROM role_permissions WHERE role ='{$role}' AND permission ='{$permission}'";
		try{
			//run query
			$rs = DB::query($sql_check_role_permissions);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
		$result = array();
		//check if user with same username exists
		if(!empty($rs)){
			return false;
		} else{
		$sql1 = "INSERT INTO `role_permissions`
					(
					`role`,
					`permission`,
					`date_added`,
					`date_updated`,
					`added_by`,
					`updated_by`)
					VALUES
					(
					'{$role}',
					'{$permission}',
					'{$date_added}',
					'{$date_added}',
					'{$added_by}',
					'{$added_by}'
					)";
	
	
		try{
			//run query
			$rs = DB::query($sql1);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
	
		return true;
		}
	}//End Save Role
	public function get_all_role_permissions(){
		$role_permissions = array();
		$role_permission_details = array();
		
		$sql = 'Select role_permissions.id as rp_id, roles.role_name as r_name, permissions.description as p_desc from role_permissions, roles, permissions WHERE role_permissions.role = roles.role_id AND role_permissions.permission = permissions.id ORDER BY rp_id ASC;';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$role_permission_details['id'] = $result['rp_id'];
				$role_permission_details['role'] = $result['r_name'];
				$role_permission_details['permission'] = $result['p_desc'];
								
				array_push($role_permissions, $role_permission_details);
			}
			return $role_permissions;

		} else return $role_permissions;
	
		
	}//end all users
	public function get_role_permission_by_id($rp_id){
	
		$role_permission_details = array();
	
		$sql = 'Select role_permissions.id as rp_id, roles.role_id as role_ID, roles.role_name as r_name, permissions.id as per_ID, permissions.description as p_desc from role_permissions, roles, permissions WHERE role_permissions.role = roles.role_id AND role_permissions.permission = permissions.id AND role_permissions.id = "'.$rp_id.'";';
	
		try{
			//run query
			$rs = DB::query($sql);
	
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$role_permission_details['id'] = $result['rp_id'];
				$role_permission_details['role'] = $result['r_name'];
				$role_permission_details['permission'] = $result['p_desc'];
				$role_permission_details['roleID'] = $result['role_ID'];
				$role_permission_details['perID'] = $result['per_ID'];				
			}
			return $role_permission_details;
	
		} else return $role_permission_details;
	
	
	}//end GET ROLE PERMISSION BY ID
	
	//UPDATE ROLE PERMISSION
	public function update_role_permissions($rp_id, $role, $permission, $added_by, $date_updated){
	
		
		$sql1 = "UPDATE `role_permissions`
				SET
				`role` = '{$role}',
				`permission` = '{$permission}',
				`date_updated` = '{$date_updated}',
				`updated_by` = {$added_by} WHERE `id` = {$rp_id};";
		try{
			//run query
			$rs1 = DB::query($sql1);
		
		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
			return false;
		}
		return true;
	}//end UPDATE ROLE PERMISSION
	
	//GET ALL ROLES
	public function get_all_roles(){
		$roles = array();
		$role_details = array();
		
		$sql = 'SELECT role_id, role_name FROM roles WHERE status=1;';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$role_details['id'] = $result['role_id'];
				$role_details['name'] = $result['role_name'];
								
				array_push($roles, $role_details);
			}
			return $roles;

		} else return $roles;
	
		
	}//end all roles
	
	//GET ALL ROLES
	public function get_all_permissions(){
		$permissions = array();
		$permission_details = array();
		
		$sql = 'SELECT id, description FROM permissions;';

		try{
			//run query
			$rs = DB::query($sql);

		}catch(MeekroDBException $e) {
			return '{"success":"no", "msg":"Oops, something went wrong while retrieving data."}';
			echo "Error: " . $e->getMessage() . "<br>\n";
			echo "SQL Query: " . $e->getQuery() . "<br>\n";
		}
			
		$result = array();
		//check if result is not empty
		if(!empty($rs)){
			foreach($rs as $result){
				$permission_details['id'] = $result['id'];
				$permission_details['name'] = $result['description'];
								
				array_push($permissions, $permission_details);
			}
			return $permissions;

		} else return $permissions;
	
		
	}//end all permissions

}


/** Default: Initialize class for use */
$RolePermission = new RolePermission();
?>