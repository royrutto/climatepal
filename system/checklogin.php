<?php
require 'PasswordHash.php';
if(session_id() == '') {
	session_start();
}

if(isset($_SESSION['session_id'])){
	include(DIR_WEB.'/index.php');
}else{
	if(!isset($device_type)) $device_type = 'computer';
	$login_details = checkLogin($Users,$username,$password, $device_type);
}

/**
 * 
 * @param the Application object $Users
 * 
 * Generates checks the login details
 */
function checkLogin($Users,$username,$password, $device_type){
	$login_details = array();
	//Check if username or password is empty
	if($username=="" || $password==""){
		if ($device_type=='mobile'){
			$login_details['login_status'] = false;
			return $login_details;
		}else{
			if($_POST['username']=="") $username_error=1;
			if($_POST['password']=="") $password_error=1;
			include('login.php');
		}
		
	} else{
		
		$username_exists = $Users->login($username);
		if($username_exists){
			$user_details = $Users->get_user_details($username);
			$stored_hash = $user_details['password'];
			$password_matches = check_password($password, $stored_hash);
			if ($password_matches){
				$user_details = $Users->get_user_details($username);
					
				$session_id = generate_random_session_id();
				$user_id = $user_details['user_id'];
				$created_date = date(DATE_FORMAT_DEFAULT);
				$last_active_date = $created_date;
				$ip_address = get_user_ip_address();
					
				//save session in database and redirect to index.php
				if($Users->create_session($session_id, $user_id, $created_date, $last_active_date, $ip_address)){
					$_SESSION['session_id']= $session_id;
					$_SESSION['username']=$user_details['username'];
					$inTwoMonths = 60 * 60 * 24 * 60 + time();
					setcookie('climatepal_session', $session_id, $inTwoMonths);
					if ($device_type=='mobile'){
						$login_details['login_status'] = true;
						$login_details['login_userid'] = $user_id;
						$login_details['login_username'] = $username;
						$login_details['login_sessionid'] = $session_id;
						return $login_details;
					}else{
						header('Location: index.php');
					}
				}
						
			}else{
				//redirect to login if password does not match
				if ($device_type=='mobile'){
					$login_details['login_status'] = false;
					return $login_details;
				} else{
					$login_error=1;
					include('login.php');
				}
				
			}
		}else{	
			//redirect to login if username does not exist
			if ($device_type=='mobile'){
				$login_details['login_status'] = false;
				return $login_details;
			}else{
				$login_error=1;
				include('login.php');
			}
			
		}
	}

}


/**
 * 
 * @return Ambigous <string, Ramdom session id>
 * 
 * Generates a rendom session id
 */
function generate_random_session_id()
{
	$valid_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$length = 20;
	// start with an empty random string
	$random_string = "";

	// count the number of chars in the valid chars string so we know how many choices we have
	$num_valid_chars = strlen($valid_chars);

	// repeat the steps until we've created a string of the right length
	for ($i = 0; $i < $length; $i++)
	{
	// pick a random number from 1 up to the number of valid chars
		$random_pick = mt_rand(1, $num_valid_chars);

		// take the random character out of the string of valid chars
		// subtract 1 from $random_pick because strings are indexed starting at 0, and we started picking at 1
		$random_char = $valid_chars[$random_pick-1];

		// add the randomly-chosen char onto the end of our string so far
		$random_string .= $random_char;
	}

	// return our finished random string
	return $random_string;
}
/**
 * 
 * @return ip address
 * 
 * Returns users IP Address
 */
function get_user_ip_address()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	{
		$ip=$_SERVER['HTTP_CLIENT_IP'];
	}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	{
		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else
	{
		$ip=$_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}

function check_password($password, $stored_hash){
	require_once DIR_SYS.'/PasswordHash.php';
	$t_hasher = new PasswordHash(8, FALSE);
	// Check that the password is correct, returns a boolean
	$check = $t_hasher->CheckPassword($password, $stored_hash);
	return $check;	
}

?>