<?php

date_default_timezone_set('Africa/Nairobi');
$last_active_date = date("Y-m-d H:i:s");
$Users->end_session($_SESSION['session_id'], $last_active_date);
unset($_SESSION['session_id'], $_SESSION['username']);
session_destroy();
if (isset($device_type)&&$device_type=='mobile'){
	return true;
}else{
	header("location: index.php");
}
exit;
?>