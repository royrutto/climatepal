<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $page_title ?> - Climate Pal</title>
    

    <!-- Styles -->
    <link href="<?php echo BASE_URL ;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ;?>css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ;?>css/datepicker.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ;?>css/demo_table.css" rel="stylesheet">
       
 
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      #searchform {
        padding-bottom:0px;
        margin-top:0px;
        padding-right:20px;
      }

      #actions {
        padding-bottom:10px;
        
      }
      #actionbutton {
        padding-top:5px;
        
      }
      .chk{width: 10px;}
      
      #survey{width:300px;}
       
      .location{width:300px;}
      .coordinates{width:300px;}
      img {
 		 max-width: none;
		}
      
      html{
       height: 100%;
      }
      
      body {
        height: 100%;
        padding-top: 60px;
        padding-bottom: 40px;
        /* The html and body elements cannot have any padding or margin. */
      }

      /* Wrapper for page content to push down footer */
      #wrap {
        min-height: 100%;
        height: auto !important;
        height: 100%;
        /* Negative indent footer by it's height */
        margin: 0 auto -60px;
      }

      /* Set the fixed height of the footer here */
      #push,
      #footer {
        height: 60px;
      }
      #footer {
        background-color: #f5f5f5;
      }

      /* Lastly, apply responsive CSS fixes as necessary */
      @media (max-width: 767px) {
        #footer {
          margin-left: -20px;
          margin-right: -20px;
          padding-left: 20px;
          padding-right: 20px;
        }
       }
   	.locations_container { border:2px solid #ccc; width:250px; height: 100px; overflow-y: scroll; padding:5px 10px;}
    </style>
    <style type="text/css">
		select {
			width: 250;
		} 
		.chart_container{
			border-left: 2px solid #DDD;
			border-bottom: 2px solid #DDD;
			border-right: 2px solid #DDD;
			padding: 3px;
			margin-bottom:15px;
			width:950px;
			margin-left:auto;
			margin-right:auto
		}
		 
		.xtop{
		min-width:1300px;
		}
		#Larger_Graph{
		width:900px;
		}
	</style>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDH_ntiSdZv1tfAJA3V-sVdBV30wEX4PIU&sensor=false"></script>
	<script type="text/javascript">
    //<![CDATA[

    var customIcons = {
      restaurant: {
        icon: 'http://maps.gstatic.com/mapfiles/ridefinder-images/mm_20_blue.png',
        shadow: 'http://maps.gstatic.com/mapfiles/ridefinder-images/mm_20_shadow.png'
      },
      bar: {
        icon: 'http://maps.gstatic.com/mapfiles/ridefinder-images/mm_20_red.png',
        shadow: 'http://maps.gstatic.com/mapfiles/ridefinder-images/mm_20_shadow.png'
      }
    };

    function load() {
      var map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(-1.309507,36.813903),
        zoom: 12,
        mapTypeId: 'roadmap'
      });
      var infoWindow = new google.maps.InfoWindow;

      // Change this depending on the name of your PHP file
      downloadUrl("<?php echo BASE_URL;?>system/mapcon.php", function(data) {
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
         	
			var name = markers[i].getAttribute("household");
			var address = markers[i].getAttribute("fieldofficer");
			var type = markers[i].getAttribute("type");
			var point = new google.maps.LatLng(
              parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));
          	var html = "<b>" + name + "</b> <br/>" + address;
          	var icon = customIcons[type] || {};
          	var marker = new google.maps.Marker({
	            map: map,
	            position: point,
	            icon: icon.icon,
	            shadow: icon.shadow
          	});
          	bindInfoWindow(marker, map, infoWindow, html);
        }
      });
    }

    function bindInfoWindow(marker, map, infoWindow, html) {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
    }

    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }
   
    function doNothing() {}

    //]]>

  </script>
   <SCRIPT LANGUAGE="Javascript" SRC="FusionCharts/Code/FusionCharts/FusionCharts.js"></SCRIPT>
    <SCRIPT LANGUAGE="Javascript" SRC="FusionCharts/Code/FusionCharts/FusionMaps.js"></SCRIPT> 
  </head>
  <body onload="load()">
  <!-- Part 1: Wrap all page content here -->
    <div id="wrap">
  	<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="home"><img src="<?php echo BASE_URL ;?>img/menu_logo.png"> Climate Pal</a>
          <div class="nav-collapse collapse">
            
              <div class="btn-group pull-right">
  				<a class="btn btn-primary" href="#"><i class="icon-user icon-white"></i> <?php echo $_SESSION["username"]; ?> </a>
  				<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
  				<ul class="dropdown-menu">
				    <li><a href="#"><i class="icon-pencil"></i> View Profile</a></li>
				    <li><a href="#"><i class="icon-trash"></i> Manage Account</a></li>
				    <li><a href="<?php echo BASE_URL ;?>logout"><i class="icon-ban-circle"></i> Log Out</a></li>
				    <li class="divider"></li>
				    <li><a href="help.html" target="_blank"><i class="i"></i>Help</a></li>
  				</ul>
			</div>
			<p class="navbar-text pull-right">
              Logged in as &nbsp;&nbsp;</p>
            
            <ul class="nav">
              <li class="<?php if($page_id ==1)echo 'active';?>">
              <a href="<?php echo BASE_URL ;?>distributions">Distributions</a></li>
              <li class="<?php if($page_id ==2)echo 'active';?>">
              <a href="<?php echo BASE_URL ;?>surveys">Surveys</a></li>
              <li class="<?php if($page_id ==3)echo 'active';?>">
              <a href="<?php echo BASE_URL ;?>reports">Reports</a></li>

            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        
         <!--SIDE BAR-->
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Quick Navigate</li>
              <li class="<?php if ($page_id=='home')echo 'active'; ?>">
              	<a href="<?php echo BASE_URL ;?>home"><i class="icon-home"></i> Home</a>
              </li>
  			  <li class="<?php if ($page_title=='Add Distribution')echo 'active'; ?>">
              	<a href="<?php echo BASE_URL ;?>distributions/add"><i class="icon-plus"></i> New Distribution</a>
              </li>
              <li class="<?php if ($page_title=='Fill Questionnare')echo 'active'; ?>">
              	<a href="<?php echo BASE_URL ;?>surveys/conduct"><i class="icon-pencil"></i> Fill Questionnaire</a>
              </li>
			<?php $roleid = $Users->get_user_role_by_session_id($_COOKIE['climatepal_session']);?>
			<?php if($roleid==1){?>
              <li class="nav-header">Settings</li>
              <li class="<?php if ($page_title=='Users')echo 'active'; ?>">
              	<a href="<?php echo BASE_URL ;?>users"><i class="icon-user"></i> Users</a>
              </li>
              <li class="<?php if ($page_title=='Roles')echo 'active'; ?>">
              	<a href="<?php echo BASE_URL ;?>roles">
              <i class=" icon-ok-circle"></i> Roles & Permissions</a></li>
             <li class="<?php if ($page_title=='Genders')echo 'active'; ?>">
              <a href="<?php echo BASE_URL ;?>genders"><i class="icon-resize-full"></i> Gender</a>
              </li>
              
              <li class="<?php if ($page_title=='Locations')echo 'active'; ?>">
              <a href="<?php echo BASE_URL ;?>locations"><i class="icon-globe"></i> Locations</a>
              </li>
              <li class="<?php if ($page_title=='Stove Types')echo 'active'; ?>">
              <a href="<?php echo BASE_URL ;?>stovetypes"><i class="icon-fire"></i> Stove Types</a>
              </li>
              <li class="divider"></li>
              <?php }?>
              <li><a href="help.html" target="_blank"><i class="i"></i> Help</a></li>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
<!--=============================Everything in the page section comes here=========================-->
	
		<?php include $page;?>
        
<!--=============================Everything in the page section comes here=========================-->    
        
        
	</div><!--/row-->
  <!--Modal Section-->
  <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="myModalLabel">Delete</h3>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete the selected <strong></strong>?</p>
     
        <form action="index.php" method="post">
        <input name="form_name" type="hidden" value="delete_record">
        <input type="hidden" name="deletefrom" id="deletefrom" value=""/>
        <input type="hidden" name="delete_ids" id="delete_ids" value=""/>
        
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button type="submit"class="btn btn-danger" aria-hidden="true"><i class="icon-trash icon-white"></i> Delete</button>
      </div>
      </form>
  </div>

</div><!--/.fluid-container-->
<div id="push"></div>
</div>

<div id="footer">
      <div class="container">
        <p> &copy; Climate Pal 2013</p>
      </div>
</div>

<?php

?>
      <!-- 
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

  <script type="text/" src="<?php echo BASE_URL ;?>js/widgets.js"></script>
  <script src="<?php echo BASE_URL ;?>js/jquery.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-transition.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-alert.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-modal.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-dropdown.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-scrollspy.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-tab.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-tooltip.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-popover.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-button.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-collapse.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-carousel.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-typeahead.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-affix.js"></script>
  <script src="<?php echo BASE_URL ;?>js/bootstrap-datepicker.js"></script>
  <script src="<?php echo BASE_URL ;?>js/holder.js"></script>
  <script src="<?php echo BASE_URL ;?>js/prettify.js"></script>
  <script src="<?php echo BASE_URL ;?>js/application.js"></script>
  <script src="<?php echo BASE_URL ;?>js/form-validation.js"></script>
  <script>
		$(function(){
			window.prettyPrint && prettyPrint();
			$('#dp1').datepicker({
				format: 'mm-dd-yyyy'
			});
			$('#dp2').datepicker();
			$('#dp3').datepicker();
			$('#dp3').datepicker();
			$('#dpYears').datepicker();
			$('#dpMonths').datepicker();
			
			
			var startDate = new Date(2012,1,20);
			var endDate = new Date(2012,1,25);
			$('#dp4').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						$('#alert').show().find('strong').text('The start date can not be greater then the end date');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					}
					$('#dp4').datepicker('hide');
				});
			$('#dp5').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() < startDate.valueOf()){
						$('#alert').show().find('strong').text('The end date can not be less then the start date');
					} else {
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').text($('#dp5').data('date'));
					}
					$('#dp5').datepicker('hide');
				});
		});
	</script>
	
	<script type="text/javascript" language="javascript" src="<?php echo BASE_URL ;?>js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$(function() {
			    var colCount = 0;
			    $('tr:nth-child(1) td').each(function () {
			        if ($(this).attr('colspan')) {
			            colCount += +$(this).attr('colspan');
			        } else {
			            colCount++;
			        }
			    });
			    
			   var counted_columns = [];
			   for (var i = 0; i <colCount; i++){
				  if (i==0){
					 counted_columns.push({"sType": "numeric"});
				  } else
					  counted_columns.push(null);  
				}

			   $('#example').dataTable( {
					"aaSorting": [ [1,'asc']],
					"aoColumnDefs": [{bSortable: false,aTargets: [0,-1],}],
					"aoColumns": counted_columns,
					      
			    } );

			   $('#example2').dataTable( {
					"aaSorting": []
					      
			    } );
			});

			 
			//highlight table row when selected
			$('td input[type="checkbox"]').click(function(){
				 var parent = $(this).parent();
	        	 var parent2 = $(parent).parent();
	        	 var element = $('#delete');
	        	 var atLeastOneIsChecked = $('td input[type="checkbox"]:checked').length > 0;
		        if ($(this).is(':checked')){
			        parent2.addClass('warning');
		            element.removeClass('hidden'); 
		        } else if(parent2.is('.warning')) {
		            parent2.removeClass('warning');
		          
		 			if (atLeastOneIsChecked){
		 				element.removeClass('hidden');
		 			} else element.addClass('hidden');
		        }
		    });
		} );
</script>
<script type="text/javascript" charset="utf-8">
		$(document).on("click", "#delete", function () {
			 var deletefrom = $('#deletefrom').val();
		     var item_name = $(this).data('itemname');
		     var selected_items = new Array();
		     var count = 0;
		     $("input:checkbox[name=item_id]:checked").each(function()
		    		 {
		    		//add $(this).val() to your array
	    		 		selected_items[count]=$(this).val();
	    		 		count++;
		    		 });
		     $(".modal-body #deletefrom").val(deletefrom);
		     $(".modal-body #delete_ids").val(selected_items.toString());
		     $("strong").empty();
		     $(".modal-body strong").append(count+' records');

		     
		});
</script>

<script type="text/javascript"> 
var geocoder;

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
} 
//Get the latitude and the longitude;
function successFunction(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    codeLatLng(lat, lng);
}

function errorFunction(){
    //alert("Geocoder failed");
}

function initialize() {
    geocoder = new google.maps.Geocoder();
}

function codeLatLng(lat, lng) {
	var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
	if (status == google.maps.GeocoderStatus.OK) {
		console.log(results);
        if (results[1]) {
            //formatted address
            //alert(results[0].formatted_address)
            //find country name
            for (var i=0; i<results[0].address_components.length; i++) {
            	for (var b=0;b<results[0].address_components[i].types.length;b++) {
					//there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                	if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
	                    //this is the object you are looking for
	                    city= results[0].address_components[i];
	                    break;
                	}
                }
        	}
        //city data
        //alert(city.short_name + " " + city.long_name)
        //set the values of form inputs with the lat and long values
        $(".location").val(results[0].formatted_address);
        $(".coordinates").val('Lat: '+lat+', Long: '+lng);
        $(".latitude").val(lat);
        $(".longitude").val(lng);
        

        } else {
          //alert("No results found");
        }
      } else {
        //alert("Geocoder failed due to: " + status);
      }
    });
}

$(document).ready(initialize());
</script>
<script type="text/javascript">

$(function() {
	 $('#cg_password').hide();
	 $('#cg_password2').hide();
	 $('#defaultpass').bind('click', function(){            
		 if($('#defaultpass').is(':checked')) {
		        //alert('checked');
		        $('#cg_password').hide();
		        $('#cg_password2').hide();
		    }else {
		    	$('#cg_password').show();
		    	 $('#cg_password2').show();
		    }
     });
	
});
</script>
<script>
$('#response_saved').hide();
<?php if(isset($response_saved) && $response_saved) {?>
$('#distribution_saved').html('Response Saved');
$('#response_saved').slideDown("slow").delay(3000).slideUp("slow");
<?php } ?>

$('#distribution_saved').hide();
<?php if(isset($distribution_saved) && $distribution_saved) {?>
$('#distribution_saved').html("<?php echo $alert_message;?>");
$('#distribution_saved').slideDown("slow").delay(7000).slideUp("slow");
<?php } ?>

//check all checkboxes
function checkAll(bx) {
	  var cbs = document.getElementsByTagName('input');
	  for(var i=0; i < cbs.length; i++) {
	    if(cbs[i].type == 'checkbox') {
	      cbs[i].checked = bx.checked;
	      if (bx.checked){
		      $(cbs[i]).parent().parent().addClass('warning');
		      $('#delete').removeClass('hidden');
	      }else {
		      $(cbs[i]).parent().parent().removeClass('warning');
		      $('#delete').addClass('hidden');
	      }
	    }
	  }
	}

</script>

<!-----------------------------------FOR FUTURE USE(SURVEY FORM DESIGN)------------------------------>
<!--script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script-->
<!--script src="<?php echo BASE_URL ;?>js/jquery.formbuilder.js"></script-->
<!--link href="<?php echo BASE_URL ;?>css/jquery.formbuilder.css" media="screen" rel="stylesheet"/>
<!--style type="text/css">
    a, body { font-family: "Tahoma", "Verdana", sans-serif; font-size: 12px; }
</style-->
<script>
/*$(function(){
	$('#my-form-builder').formbuilder({
		'save_url': 'example-save.php',
		'load_url': 'example-json.php',
		'useJson' : true
	});
	$(function() {
		$("#my-form-builder ul").sortable({ opacity: 0.6, cursor: 'move'});
	});
});*/
</script>
<!-----------------------------------FOR FUTURE USE(SURVEY FORM DESIGN)------------------------------>

</body>
</html>