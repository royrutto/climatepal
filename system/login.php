<?php
if(session_id() == '') {
	session_start();
}

if (isset($_SESSION['session_id'])){
	header('Location: index.php');
}
else{
?>

<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Login · ClimatePal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Styles -->
    <link href="<?php echo BASE_URL ;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ;?>css/bootstrap-responsive.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
      .error {
      	color:red;
      }

    </style>
    

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->   	
  </head>

  <body>

    <div class="container">

      <form class="form-signin" action="<?php echo BASE_URL ;?>index.php" method="post">
        <h2 class="form-signin-heading"><img src="<?php echo BASE_URL ;?>img/logo.png"></h2>
        <p class="lead">Login</p>
        <?php
        if (isset($login_error)){
			if($login_error ==1) echo ('<p class="error"> Invalid Username or Password </p>');
		}
        ?>
        <input name="form_name" type="hidden" value="login">
        <?php
        if (isset($username_error)){
			if($username_error ==1) echo ('<p class="error"> Username cannot be blank </p>');
		}
        ?>
        <input name="username" type="text" class="input-block-level" placeholder="Username" value="<?php  if (isset($username))echo $username;?>">
        
        <?php
        if (isset($password_error)){
			if($password_error ==1) echo ('<p class="error"> Password cannot be blank </p>');
		}
        ?>
        <input name ="password" type="password" class="input-block-level" placeholder="Password">
        
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo BASE_URL ;?>js/jquery.js"></script>
    <script src="<?php echo BASE_URL ;?>js/bootstrap-transition.js"></script>
    <script src="<?php echo BASE_URL ;?>js/bootstrap-alert.js"></script>
    <script src="<?php echo BASE_URL ;?>js/bootstrap-modal.js"></script>
    <script src="<?php echo BASE_URL ;?>js/bootstrap-dropdown.js"></script>
    <script src="<?php echo BASE_URL ;?>js/bootstrap-scrollspy.js"></script>
    <script src="<?php echo BASE_URL ;?>js/bootstrap-tab.js"></script>
    <script src="<?php echo BASE_URL ;?>js/bootstrap-tooltip.js"></script>
    <script src="<?php echo BASE_URL ;?>js/bootstrap-popover.js"></script>
    <script src="<?php echo BASE_URL ;?>js/bootstrap-button.js"></script>
    <script src="<?php echo BASE_URL ;?>js/bootstrap-collapse.js"></script>
    <script src="<?php echo BASE_URL ;?>js/bootstrap-carousel.js"></script>
    <script src="<?php echo BASE_URL ;?>js/bootstrap-typeahead.js"></script>

  

</body></html>
<?php }?>