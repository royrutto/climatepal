<?php
$form_name = $_POST['form_name'];
switch($form_name){
	case 'add_user':
		$Model = $Users;
		include(DIR_BUNDLES.'/users/validate_add_user.php');
	break;
	case 'edit_user':
		$Model = $Users;
		include(DIR_BUNDLES.'/users/validate_add_user.php');
	break;
	case 'add_sale':
		$Model = $Sales;
		include(DIR_BUNDLES.'/sales/validate_add_sale.php');
	break;
	case 'edit_sale':
		$Model = $Sales;
		include(DIR_BUNDLES.'/sales/validate_add_sale.php');
	break;
	case 'add_gender':
		$Model = $Gender;
		include(DIR_BUNDLES.'/users/validate_add_gender.php');
	break;
	case 'edit_gender':
		$Model = $Gender;
		include(DIR_BUNDLES.'/users/validate_add_gender.php');
	break;
	case 'add_stove_type':
		$Model = $StoveType;
		include(DIR_BUNDLES.'/sales/stoves/validate_add_stovetype.php');
	break;
	case 'edit_stove_type':
		$Model = $StoveType;
		include(DIR_BUNDLES.'/sales/stoves/validate_add_stovetype.php');
	break;
	case 'add_role':
		$Model = $Role;
		include(DIR_BUNDLES.'/settings/validate_add_roles.php');
	break;
	case 'edit_role':
		$Model = $Role;
		include(DIR_BUNDLES.'/settings/validate_add_roles.php');
	break;
	case 'add_permission':
		$Model = $Permission;
		include(DIR_BUNDLES.'/settings/validate_permissions.php');
	break;
	case 'edit_permission':
		$Model = $Permission;
		include(DIR_BUNDLES.'/settings/validate_permissions.php');
	break;
	case 'add_role_permissions':
		$Model = $RolePermission;
		include(DIR_BUNDLES.'/settings/validate_role_permissions.php');
	break;
	case 'edit_role_permissions':
		$Model = $RolePermission;
		include(DIR_BUNDLES.'/settings/validate_role_permissions.php');
	break;
	case 'add_district':
		$Model = $District;
		include(DIR_BUNDLES.'/locations/validate_districts.php');
	break;
	case 'edit_district':
		$Model = $District;
		include(DIR_BUNDLES.'/locations/validate_districts.php');
	break;
	case 'add_location':
		$Model = $Location;
		include(DIR_BUNDLES.'/locations/validate_locations.php');
	break;
	case 'edit_location':
		$Model = $Location;
		include(DIR_BUNDLES.'/locations/validate_locations.php');
	break;
	case 'add_sublocation':
		$Model = $SubLocation;
		include(DIR_BUNDLES.'/locations/validate_sublocations.php');
	break;
	case 'edit_sublocation':
		$Model = $SubLocation;
		include(DIR_BUNDLES.'/locations/validate_sublocations.php');
	break;
	case 'edit_household':
		$Model = $Household;
		include(DIR_BUNDLES.'/locations/validate_households.php');
	break;
	case 'add_response':
		$Model = $Surveys;
		include(DIR_BUNDLES.'/surveys/validate_add_response.php');
	break;
	
	case 'upload-excel':
		$Model = $Sales;
		include(DIR_BUNDLES.'/sales/upload-excel-file.php');
	break;
	
	case 'delete_record':
		$Model = $Sales;
		if($_POST["deletefrom"]=='distributions'){
			$Model = $Sales;
			include(DIR_BUNDLES.'/sales/delete-record.php');
		}
		
	break;
}
?>