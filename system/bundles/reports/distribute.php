<?php 
include("DBConn.php");
$link = connectToDB();
if(!isset($_GET['date_from'])&&!isset($_GET['date_to'])){
	$year = date('Y');
//echo $year;
$monthnames=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
$sql_months = "select distinct MONTH(dist_date) as month from distributions WHERE YEAR(dist_date) = $year order by month asc;";
$sql_result_months = mysql_query($sql_months) or die(mysql_error());
$months = array();
$counter = 0;
while($month = mysql_fetch_assoc($sql_result_months)){
	$months[$counter] = $month['month'];
	$counter++;
}

$sql_fieldofficers = "SELECT users.user_id, employees.surname FROM users, employees WHERE users.employee_id = employees.employee_id
AND users.role_id =3;";
$sql_result_fieldofficers = mysql_query($sql_fieldofficers) or die("Error "+mysql_error());
$fieldofficers = array();
while($fieldofficer = mysql_fetch_assoc($sql_result_fieldofficers)){
	$fieldofficers[$fieldofficer['user_id']] = $fieldofficer['surname'];
	//echo $fieldofficers[$fieldofficer['id']].'<br />';
	//$fieldofficers[$fieldofficer['sname']] = $fieldofficer['sname'];
}
?>

<chart caption='For the year <?php echo $year;?>' subcaption='Who Distributed What' lineThickness='1' showValues='0' formatNumberScale='0' anchorRadius='2'   divLineAlpha='20' divLineColor='CC3300' divLineIsDashed='1' showAlternateHGridColor='1' alternateHGridAlpha='5' alternateHGridColor='CC3300' shadowAlpha='40' labelStep='2' numvdivlines='5' chartRightMargin='35' bgColor='FFFFFF,CC3300' bgAngle='270' bgAlpha='10,10' xAxisName='Months' yAxisName='Stoves Distributed'>
<categories >

<?php 
foreach($months as $month){?>
	<category label='<?php echo $monthnames[$month-1]." ".$year;?>' />
<?php }
?>
</categories>

<?php 
$counter = 0;
$fieldofficer_ids = array_keys($fieldofficers);
foreach($fieldofficers as $fieldofficer){echo $fieldofficer;?>
	<dataset seriesName='<?php echo $fieldofficer;?>' >
<?php 
	foreach($months as $month){
		$sql_get_data = "SELECT COUNT(*) as total_distributed FROM distributions WHERE MONTH(dist_date)=$month AND added_by='".$fieldofficer_ids[$counter]."';";
	
		$sql_result_get_data = mysql_query($sql_get_data) or die("Error ".mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		?>
		<set value='<?php echo $distributed;?>' link = 'fo.php?officer=<?php echo $fieldofficer_ids[$counter]; ?>&year=<?php echo $year;?>' />
	<?php }?>
	</dataset>
	<?php 
	$counter++;
}
}
else{
	$from = $_GET['date_from'];
	$to = $_GET['to'];
	$sql_months = "select distinct MONTH(dist_date) as month, YEAR(dist_date) as year from distributions WHERE dist_date BETWEEN '".$from."' AND '".$to."';";
$sql_result_months = mysql_query($sql_months) or die(mysql_error());
$months = array();
$years = array();
$counter = 0;
while($row = mysql_fetch_assoc($sql_result_months)){
	$months[$counter] = $row['month'];
	$years[$counter] = $row['year'];
	$counter++;
}

$sql_fieldofficers = "SELECT users.user_id, employees.surname FROM users, employees WHERE users.employee_id = employees.employee_id
AND users.role_id =3;";
$sql_result_fieldofficers = mysql_query($sql_fieldofficers) or die("Error "+mysql_error());
$fieldofficers = array();
while($fieldofficer = mysql_fetch_assoc($sql_result_fieldofficers)){
	$fieldofficers[$fieldofficer['user_id']] = $fieldofficer['surname'];
	//echo $fieldofficers[$fieldofficer['id']].'<br />';
	//$fieldofficers[$fieldofficer['sname']] = $fieldofficer['sname'];
}
?>

<chart caption='From: <?php echo $from;?> To: <?php echo $to;?>' subcaption='Who Distributed What' lineThickness='1' showValues='0' formatNumberScale='0' anchorRadius='2'   divLineAlpha='20' divLineColor='CC3300' divLineIsDashed='1' showAlternateHGridColor='1' alternateHGridAlpha='5' alternateHGridColor='CC3300' shadowAlpha='40' labelStep='2' numvdivlines='5' chartRightMargin='35' bgColor='FFFFFF,CC3300' bgAngle='270' bgAlpha='10,10' xAxisName='Months' yAxisName='Stoves Distributed'>
<categories >

<?php 
$x=0;
foreach($months as $month){?>
	<category label='<?php echo $monthnames[$month-1]." ".$years[$x];?>' />
<?php $x++; }
?>
</categories>

<?php 
$counter = 0;
$fieldofficer_ids = array_keys($fieldofficers);
foreach($fieldofficers as $fieldofficer){echo $fieldofficer;?>
	<dataset seriesName='<?php echo $fieldofficer;?>' >
<?php 
	foreach($months as $month){
		$sql_get_data = "SELECT COUNT(*) as total_distributed FROM distributions WHERE MONTH(dist_date)=$month AND added_by='".$fieldofficer_ids[$counter]."';";
	
		$sql_result_get_data = mysql_query($sql_get_data) or die("Error ".mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		?>
		<set value='<?php echo $distributed;?>' link = 'fo.php?officer=<?php echo $fieldofficer_ids[$counter]; ?>&year=<?php echo $year;?>' />
	<?php }?>
	</dataset>
	<?php 
	$counter++;
}
}

?>
	<styles>                
		<definition>
                         
			<style name='CaptionFont' type='font' size='12' />
		</definition>
		<application>

			<apply toObject='CAPTION' styles='CaptionFont' />
			<apply toObject='SUBCAPTION' styles='CaptionFont' />
		</application>
	</styles>

</chart>
