<?php
//We've included ../Includes/FusionCharts.php and ../Includes/DBConn.php, which contains
//functions to help us easily embed the charts and connect to a database.
include("FusionCharts/Code/PHP/Includes/FusionCharts.php");
include("DBConn.php");
//echo date(DATE_FORMAT_DATEPICKER);
//echo date(DATE_FORMAT_DATEPICKER_MONTH);
// Connect to the DB
$monthnames=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
?>

<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/reports/tab_menu.php'?>
  		</ul>
        <div class="tab-content">
    	<div class="tab-pane active" id="tab1">		
    			<div id="actions">

            <div id="actionbutton">
            <form class="form-horizontal"  action="" method="post" name="filtered" id="filtered">
      	<input name="form_name" type="hidden" value="filter_dist_rate">
      	 
      	 <legend>Customize Here</legend>
         <table width="100%" border="0" align="left" style="float:left;">
  	<tr>
    <td style="float:left;"><label class="control-label" for="inputFrom">From:</label>&nbsp;&nbsp;<div class="input-append date" id="dp4" data-date-format="yyyy-mm-dd">
				<input id="from" name="from" class="span8" size="16" type="text" value="<?php echo date(DATE_DEFAULT); ?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div></td>
              <td style="float:left;"><label class="control-label" for="inputFrom">To:</label>&nbsp;&nbsp;<div class="input-append date" id="dp5" data-date-format="yyyy-mm-dd">
				<input id="to" name="to" class="span8" size="16" type="text" value="<?php echo date(DATE_DEFAULT); ?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div></td>
    <td><button type="submit" class="btn btn-primary" name="ok" id="ok"> 
  		  		<i class="icon-filter"></i> Filter
  		  	</button></td>
  </tr>
</table>
</form><div class="clear"></div>

  			
            </div>
           </div>


<CENTER>

<?php
	//In this example, we show how to connect FusionCharts to a database.
	//For the sake of ease, we've used an MySQL databases containing two
	//tables.
		
	// Connect to the DB
	$link = connectToDB();
	if(isset($_POST['form'])&&isset($_POST['to'])){
    $from = $_POST['from'];
	$to = $_POST['to'];
	echo $from." ".$to;
	
	$sql_months = "SELECT DISTINCT MONTH(dist_date) as month, YEAR(dist_date) as year FROM distributions WHERE dist_date BETWEEN '".$from."' AND '".$to."' ASC;";
	$sql_result_months = mysql_query($sql_months) or die(mysql_error());
	$months = array();
	$years = array();
	$counter = 0;
	while($month = mysql_fetch_assoc($sql_result_months)){
		$months[$counter] = $month['month'];
		$years[$counter] =  $month['year'];
		//echo $monthnames[$months[$counter]-1];
		$counter++;
	}

	//$strXML will be used to store the entire XML document generated
	//Generate the chart element
	$strXML = "<chart caption='Monthly Distributions Summary' subCaption='From ".$from." To ".$to."' lineThickness='1' showValues='0' formatNumberScale='0' xAxisName='Month' yAxisName='Stove Distributions' numberSuffix=' Stoves'>";
	$x=0;
	foreach($months as $dist_month){
		$sql_get_data = "SELECT COUNT(*) AS total_distributed FROM distributions WHERE MONTH(dist_date) = $dist_month AND YEAR(dist_date) = '".$years[$x]."';";
		//echo $sql_get_data;
		$sql_result_get_data = mysql_query($sql_get_data) or die(mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		//echo $distributed;
		$strXML .= "<set label='" . $monthnames[$dist_month-1] ." ".$years[$x]. "' value='" . $distributed . "' link = 'dist_rate_monthly?month=".$dist_month."' />";
			//free the resultset
			//mysql_free_result($get_data_resultset);
			//echo $monthnames[1]."  ".$distributed.'<br />';
			$x++;
	}
	mysql_close($link);

	//Finally, close <chart> element
	$strXML .= "</chart>";
	
	//Create the chart - Pie 3D Chart with data from $strXML
	echo renderChart("FusionCharts/Charts/Line.swf", "", $strXML, "ChartId", "800", "400", "0", "1");
	}
	else{
		$result = date('Y');
		$sql_months = "SELECT DISTINCT MONTH(dist_date) as month, YEAR(dist_date) as year FROM distributions ORDER BY month ASC;";
	$sql_result_months = mysql_query($sql_months) or die(mysql_error());
	$months = array();
	$years = array();
	$counter = 0;
	while($month = mysql_fetch_assoc($sql_result_months)){
		$months[$counter] = $month['month'];
		$years[$counter] =  $month['year'];
		//echo $monthnames[$months[$counter]-1];
		$counter++;
	}

	//$strXML will be used to store the entire XML document generated
	//Generate the chart element
	$strXML = "<chart caption='Monthly Distributions Summary' subCaption='for the year ".$result."' lineThickness='1' showValues='0' formatNumberScale='0' anchorRadius='2'   divLineAlpha='20' divLineColor='1D8BD1' divLineIsDashed='1' showAlternateHGridColor='1' alternateHGridAlpha='5' alternateHGridColor='1D8BD1' shadowAlpha='40' labelStep='2' numvdivlines='5' chartRightMargin='35' bgColor='FFFFFF,1D8BD1' bgAngle='270' bgAlpha='10,10' xAxisName='Month' yAxisName='Stove Distributions' numberSuffix=' Stoves' useRoundEdges='1'>";
	$x=0;
	foreach($months as $dist_month){
		$sql_get_data = "SELECT COUNT(*) AS total_distributed FROM distributions WHERE MONTH(dist_date) = $dist_month;";
		//echo $sql_get_data;
		$sql_result_get_data = mysql_query($sql_get_data) or die(mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		//echo $distributed;
		$strXML .= "<set label='" . $monthnames[$dist_month-1] ." ".$years[$x]. "' value='" . $distributed . "' link = 'dist_rate_monthly?month=".$dist_month."' />";
			//free the resultset
			//mysql_free_result($get_data_resultset);
			//echo $monthnames[1]."  ".$distributed.'<br />';
			$x++;
	}
	mysql_close($link);

	//Finally, close <chart> element
	$strXML .= "</chart>";
	
	//Create the chart - Pie 3D Chart with data from $strXML
	echo renderChart("FusionCharts/Charts/Line.swf", "", $strXML, "ChartId", "800", "400", "0", "1");
	}
?>
</CENTER>
</div> <!-- End Tab 1-->
</div> <!-- End Tab Content-->
</div> <!--/End, Tabbable-->
</div><!--/span 9-->
