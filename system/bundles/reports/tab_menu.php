<li class="<?php if($page_title =='Old Stoves')echo 'active';?>">
<a href="<?php echo BASE_URL?>oldstoves" data-toggle="tab1">
<span>Old Stoves</span></a>
 </li>
 <li class="<?php if($page_title =='Locations')echo 'active';?>">
 <a href="<?php echo BASE_URL?>reports" data-toggle="tab2">
 <span>By Locations</span></a>
 </li>
 <li class="<?php if($page_title =='Rate of Distribution')echo 'active';?>">
<a href="<?php echo BASE_URL?>dist_rate" data-toggle="tab3">
<span>Rate of Distribution</span></a>
 </li>
<li class="<?php if($page_title =='Who Distributed What')echo 'active';?>">
<a href="<?php echo BASE_URL?>who" data-toggle="tab4">
<span>Who Distributed What</span></a>
</li>
<li class="<?php if($page_title =='Number of Stoves in Use')echo 'active';?>">
<a href="<?php echo BASE_URL?>nostove" data-toggle="tab4">
<span>No. of Stoves in Use</span></a>
</li>

<li class="<?php if($page_title =='Freq of Stove Use')echo 'active';?>">
<a href="<?php echo BASE_URL?>freqstove" data-toggle="tab4">
<span>Freq of Stoves Use</span></a>
</li>

<li class="<?php if($page_title =='Satisfaction Level')echo 'active';?>">
<a href="<?php echo BASE_URL?>satisfaction" data-toggle="tab4">
<span>Satisfaction Level</span></a>
</li>

<li class="<?php if($page_title =='Tradition Stoves')echo 'active';?>">
<a href="<?php echo BASE_URL?>tradstove" data-toggle="tab4">
<span>Tradition Stoves</span></a>
</li>

<li class="<?php if($page_title =='Damaged Stoves')echo 'active';?>">
<a href="<?php echo BASE_URL?>damagedstoves" data-toggle="tab4">
<span>Damaged Stoves</span></a>
</li>