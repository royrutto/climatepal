<SCRIPT LANGUAGE="Javascript" SRC="FusionCharts/Code/FusionCharts/FusionCharts.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
		//We store the XML data as a string
		
		/*
		 * updateChart method is called, when user clicks the button
		 * Here, we change the chart from Column to line
		*/
		//$(document).ready(function() {
		function updateChart(){
			//var date_from = $("#from").attr('value');
			//var date_to = $("#to").attr('value');
			var date_from = $("#from").val();
			var date_to = $("#to").val();
			date_from = encodeURIComponent(date_from);
			date_to = encodeURIComponent(date_to);
			queryString = [];
			queryString.push('kutoka=' + date_from);
			queryString.push('hadi=' + date_to);
			//alert(date_from+" "+date_to);
			var strXML = 'xml/distribute.php?parameters='+date_from+'_'+date_to;
			alert(strXML);
			//Create another instance of the chart.
		   var chart = new FusionCharts("FusionCharts/Charts/MSLine.swf", "ChartId", "900", "550", "0", "1");
		   chart.setDataURL(strXML);	
		   chart.addParam("WMode", "Transparent");	   
		   chart.render("Distributed");
		}
		//});
	</SCRIPT>
<style type="text/css">
select {
	width: 250;
} 
.chart_container{
	border-left: 2px solid #DDD;
	border-bottom: 2px solid #DDD;
	border-right: 2px solid #DDD;
	padding: 3px;
	margin-bottom:15px;
	width:950px;
	margin-left:auto;
	margin-right:auto
}
 
.xtop{
min-width:1300px;
}
#Larger_Graph{
width:900px;
}
 

</style>

<div class="span9">
	<div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/reports/tab_menu.php'?>
  		</ul>
        <?php
		//echo $_POST['from']." ".$_POST['to'];
		?>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">		
    			<div id="actions">

            <div id="actionbutton">
            
         <table width="100%" border="1" align="left" style="float:left;">
  	<tr>
    <form class="form-horizontal"  action="" method="post" name="filtered" id="filtered">
      	<input name="form_name" type="hidden" value="filter_dist_rate">
      	 
      	 <legend>Customize Here</legend>
    <td style="float:left;"><label class="control-label" for="inputFrom">From:</label>&nbsp;&nbsp;<div class="input-append date" id="dp4" data-date-format="yyyy-mm-dd">
				<input id="from" name="from" class="span8" size="16" type="text" value="<?php echo date(DATE_DEFAULT); ?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div></td>
              <td style="float:left;"><label class="control-label" for="inputFrom">To:</label>&nbsp;&nbsp;<div class="input-append date" id="dp5" data-date-format="yyyy-mm-dd">
				<input id="to" name="to" class="span8" size="16" type="text" value="<?php echo date(DATE_DEFAULT); ?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div></td></form>
              <td style="float:left;">
<button type="submit" class="btn btn-primary" name="ok" id="ok" onClick="javaScript:updateChart();"> 
  		  		<i class="icon-filter"></i> Filter
  		  	</button></td>
    </tr>
</table>

<div class="clear"></div>

  			
            </div>
           </div>


</div></div>
<div id="Distributed" style="z-index: -20"></div>
</div>
</div>
</div>
</div> 

 <script type="text/javascript">
   var chart = new FusionCharts("FusionCharts/Charts/MSLine.swf", "ChartId", "900", "550", "0", "1");
   chart.setDataURL("xml/distribute.php");	
   chart.addParam("WMode", "Transparent");	   
   chart.render("Distributed");
</script>	