<SCRIPT LANGUAGE="Javascript" SRC="FusionCharts/Code/FusionCharts/FusionCharts.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
		function updateChart(){
			//var date_from = $("#from").attr('value');
			//var date_to = $("#to").attr('value');
			var date_from = $("#from").val();
			var date_to = $("#to").val();
			date_from = encodeURIComponent(date_from);
			date_to = encodeURIComponent(date_to);
			queryString = [];
			queryString.push('kutoka=' + date_from);
			queryString.push('hadi=' + date_to);
			//alert(date_from+" "+date_to);
			var strXML = 'xml/damaged_xml.php?parameters='+date_from+'_'+date_to;
			alert(strXML);
			//Create another instance of the chart.
		   var chart = new FusionCharts("FusionCharts/Charts/Area2D.swf", "ChartId", "900", "550", "0", "1");
		   chart.setDataURL(strXML);	
		   chart.addParam("WMode", "Transparent");	   
		   chart.render("Distributed");
		}
		function showGrid(){
			var myGrid = new FusionCharts("FusionCharts/Charts/SSGrid.swf", "myGrid1", "900", "400", "0", "1");
			myGrid.setDataURL("xml/damaged_xml.php");
			myGrid.addVariable('showPercentValues', '0');
			myGrid.addVariable('showShadow', '1');
			//myGrid.addVariable('numberItemsPerPage','3');
			myGrid.render("Distributed");

		}
		function exportCharts(exportType)
		{
			//Get reference to chart.
			var chartObject = getChartFromId('ChartId');
			
			// Now, we proceed with exporting only if chart has finished rendering.
			if (chartObject.hasRendered() != true)
			{
				alert("Please wait for the chart to finish rendering, before you can invoke exporting");
				return;
			}
			
			// call exporting function
			chartObject.exportChart( {exportFormat: exportType} );
		}
		function FC_Rendered(){
             //Get reference to the chart object
            var chartObj = getChartFromId('ChartId');
            //Simply alert the CSV Data 
            chartObj.getDataAsCSV();
			
            return;
      }
	  function printChart(){
	  FusionCharts.printManager.enabled(true);
                                  
      FusionCharts.addEventListener ( 
          FusionChartsEvents.PrintReadyStateChange , 
          function (identifier, parameter) {
            if(parameter.ready){ 
               alert("Chart is now ready for printing.");
               document.getElementById('printButton').disabled = false;
            }
        });
	  }
	  </SCRIPT>
<style type="text/css">
select {
	width: 250;
} 
.chart_container{
	border-left: 2px solid #DDD;
	border-bottom: 2px solid #DDD;
	border-right: 2px solid #DDD;
	padding: 3px;
	margin-bottom:15px;
	width:950px;
	margin-left:auto;
	margin-right:auto;
}

    
    .myButton {
        
        -moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
        -webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
        box-shadow:inset 0px 1px 0px 0px #ffffff;
        
        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ffffff), color-stop(1, #f6f6f6));
        background:-moz-linear-gradient(top, #ffffff 5%, #f6f6f6 100%);
        background:-webkit-linear-gradient(top, #ffffff 5%, #f6f6f6 100%);
        background:-o-linear-gradient(top, #ffffff 5%, #f6f6f6 100%);
        background:-ms-linear-gradient(top, #ffffff 5%, #f6f6f6 100%);
        background:linear-gradient(to bottom, #ffffff 5%, #f6f6f6 100%);
        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#f6f6f6',GradientType=0);
        
        background-color:#ffffff;
        
        -moz-border-radius:6px;
        -webkit-border-radius:6px;
        border-radius:6px;
        
        border:1px solid #dcdcdc;
        
        display:inline-block;
        color:#666666;
        font-family:arial;
        font-size:15px;
        font-weight:bold;
        padding:6px 24px;
        text-decoration:none;
        
        text-shadow:0px 1px 0px #ffffff;
        
    }
    .myButton:hover {
        
        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #f6f6f6), color-stop(1, #ffffff));
        background:-moz-linear-gradient(top, #f6f6f6 5%, #ffffff 100%);
        background:-webkit-linear-gradient(top, #f6f6f6 5%, #ffffff 100%);
        background:-o-linear-gradient(top, #f6f6f6 5%, #ffffff 100%);
        background:-ms-linear-gradient(top, #f6f6f6 5%, #ffffff 100%);
        background:linear-gradient(to bottom, #f6f6f6 5%, #ffffff 100%);
        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f6f6f6', endColorstr='#ffffff',GradientType=0);
        text-decoration:none;
        
        background-color:#999;
    }
    .myButton:active {
        position:relative;
        top:1px;
    }

</style>

<div class="span9">
	<div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/reports/tab_menu.php'?>
  		</ul>
        <?php
		//echo $_POST['from']." ".$_POST['to'];
		?>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">		
    			<div id="actions">

            <div id="actionbutton">
            <table class="table table-condensed">
<thead>
    <tr>
        <td>From:</td>
        <td><div class="input-append date" id="dp4" data-date-format="yyyy-mm-dd">
				<input id="from" name="from" class="span8" size="" type="text" value="<?php echo date(DATE_DEFAULT); ?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div></td>
        <td>To:</td>
         <td><div class="input-append date" id="dp5" data-date-format="yyyy-mm-dd">
				<input id="to" name="to" class="span8" size="" type="text" value="<?php echo date(DATE_DEFAULT); ?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div></td>
        <td><button type="submit" class="btn btn-primary" style="width:125px;" name="ok" id="ok" onClick="javaScript:updateChart();"> 
  		  		<i class="icon-filter"></i> Filter
  		  	</button></td>
    </tr>
    <tr>
        <td>Report Type:</td>
        <td><select name ="gender" style="width:160px;">
					<option value="chart" selected>Chart</option>
				 
		     		<option value="table" onclick="javaScript:showGrid()">Grid</option>
                    </select></td>
         <td><select name ="location" style="width:160px;">
					<option value="chart" selected>Select Location</option>
                    <option value="" >Nyaduwa Ukatulie Kwani Masisha ya Ndoa</option>
				 
		     		</select></td>
        <td><input type="button" class="myButton" style="width:160px;" value="Monthly" onclick=""></td>
        <td><input type="button" class="myButton" style="width:125px;" value="Weekly" onclick=""></td>
     </tr>
     <tr>
        <td><input type="button" class="myButton" value="Export as PDF" onClick="javascript:exportCharts('PDF');"></td>
        <td><input type="button" class="myButton" style="width:160px;" value="Export as CSV" onclick="FC_Rendered()"></td>
        <td><input type="button" class="myButton" style="width:160px;" value="Export as PNG" onclick="exportCharts('PNG')"></td>
        <td><input type="button" class="myButton" style="width:160px;" value="Export as JPG" onclick="exportCharts('JPG')"></td>
        <td><input type="button" class="myButton" value="Print Chart" onclick="javascript:window.print()"></td>
     </tr>
</thead>
</table>
<div class="clear"></div>

  			
            </div>
           </div>


</div></div>
<div id="Distributed" style="z-index: -20"></div>
</div>
</div>
</div>
</div> 

 <script type="text/javascript">
   var chart = new FusionCharts("FusionCharts/Charts/Area2D.swf", "ChartId", "900", "400", "0", "1");
   chart.setDataURL("xml/damaged_xml.php");	
   chart.addParam("WMode", "Transparent");	   
   chart.render("Distributed");
</script>	
<!-- Google Analytics Tracker Code Starts -->
<script type="text/javascript">
// analytics
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost 
	+ "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
if (typeof(_gat) == "object") {
	var pageTracker = _gat._getTracker("UA-215295-3"); pageTracker._initData(); pageTracker._trackPageview();
}
</script>
<!-- Google Analytics Tracker Code Ends -->