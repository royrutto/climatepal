<?php
//We've included ../Includes/FusionCharts.php and ../Includes/DBConn.php, which contains
//functions to help us easily embed the charts and connect to a database.
include("FusionCharts/Code/PHP/Includes/FusionCharts.php");
include("DBConn.php");
?>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/reports/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">		
    			<div id="actions">

            <div id="actionbutton">


<CENTER>
<?php
	//In this example, we show how to connect FusionCharts to a database.
	//For the sake of ease, we've used an MySQL databases containing two
	//tables.
		
	// Connect to the DB
	$link = connectToDB();

	//$strXML will be used to store the entire XML document generated
	//Generate the chart element
	$strXML = "<chart caption='Sub Locations' subCaption='By Locations' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix=' Sub Locations'>";

	// Fetch all factory records
	$strQuery = "select * from locations";
	$result = mysql_query($strQuery) or die(mysql_error());
    
	//Iterate through each factory
	if ($result) {
		while($ors = mysql_fetch_array($result)) {
			//Now create a second query to get details for this factory
			$strQuery = "select Count(*) as TotOutput from sublocations where location=" . $ors['id'];
			$result2 = mysql_query($strQuery) or die(mysql_error()); 
			$ors2 = mysql_fetch_array($result2);
			//Generate <set label='..' value='..' />        
			$strXML .= "<set label='" . $ors['location'] . "' value='" . $ors2['TotOutput'] . "' />";
			//free the resultset
			mysql_free_result($result2);
		}
	}
	mysql_close($link);

	//Finally, close <chart> element
	$strXML .= "</chart>";
	
	//Create the chart - Pie 3D Chart with data from $strXML
	echo renderChart("FusionCharts/Charts/Pie2D.swf", "", $strXML,"ChartId", "800", "400", "0", "0");
?>
</CENTER>
</div> <!-- End Tab 2-->

  				</div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->