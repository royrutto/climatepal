<?php 
	$all_users = $Model->get_all_users();
?>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/reports/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab4">		
    			<div id="actions">

            <div id="actionbutton">
      		<a href="<?php echo BASE_URL ;?>dist/add" class="btn btn-primary"><i class="icon-user icon-white"></i> Add Distribution</a>
  
            <div class="btn-group">
 				<a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#">
 					<i class="icon-filter"></i> Filter
    				<span class="caret"></span>
  				</a>
  				<ul class="dropdown-menu">
    			<!-- dropdown menu links -->
    				<li><a href="#">by Location</a></li>
					<li><a href="#">by Field Officer</a></li>
					<li><a href="#">by Name</a></li>
  				</ul>
			</div>
			<a href="#" class="btn btn-inverse"><i class="icon-refresh icon-white"></i> Refresh</a>
            <a id='delete' href="#myModal" class="btn btn-danger hidden" data-toggle="modal"><i class="icon-trash icon-white"></i> Delete Selection</a>
            </div>
           </div>
	
      					    
			
    			</div> <!-- End Tab 1-->
    			

    			 <div class="tab-pane" id="tab2">
      				<p>Howdy, I'm in Section 2.</p>
      				
          				<?php echo date();?>

    			 </div> <!-- End Tab 2-->

  				</div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`roles` (`role_name`, `decription`, `date_added`, `date_updated`) VALUES ('Administrator', 'Controls the whole system', '2013-02-07', '');
       -->
	