<?php
//We've included ../Includes/FusionCharts.php and ../Includes/DBConn.php, which contains
//functions to help us easily embed the charts and connect to a database.
include("FusionCharts/Code/PHP/Includes/FusionCharts.php");
include("DBConn.php");
//echo date(DATE_FORMAT_DATEPICKER);
//echo date(DATE_FORMAT_DATEPICKER_MONTH);
// Connect to the DB
	$link = connectToDB();
?>

<BODY>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/reports/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">		
    			<div id="actions">

            <div id="actionbutton">
            <form class="form-horizontal"  action="" method="post" name="filtered" id="filtered">
      	<input name="form_name" type="hidden" value="filter_dist_rate">
      	 
      	 <legend>Customize Here</legend>
         <table width="100%" border="0" align="left" style="float:left;">
  	<tr>
    <td style="float:left;"><label class="control-label" for="inputFrom">From:</label>&nbsp;&nbsp;<div class="input-append date" id="dp4" data-date-format="yyyy-mm-dd">
				<input id="from" name="from" class="span8" size="16" type="text" value="<?php echo date(DATE_DEFAULT); ?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div></td>
              <td style="float:left;"><label class="control-label" for="inputFrom">To:</label>&nbsp;&nbsp;<div class="input-append date" id="dp5" data-date-format="yyyy-mm-dd">
				<input id="to" name="to" class="span8" size="16" type="text" value="<?php echo date(DATE_DEFAULT); ?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div></td>
    <td><button type="submit" class="btn btn-primary" name="ok" id="ok"> 
  		  		<i class="icon-filter"></i> Filter
  		  	</button></td>
  </tr>
</table>
</form><div class="clear"></div>

  			
            </div>
           </div>


<CENTER>

<?php
	//In this example, we show how to connect FusionCharts to a database.
	//For the sake of ease, we've used an MySQL databases containing two
	//tables.
		
	// Connect to the DB
	$link = connectToDB();
	if(isset($_GET['year'])){
    $result = $_GET['year'];
	}
	else{
		$result = date('Y');
	}
	//echo $result;
	
	$monthnames=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	$sql_months = "select distinct MONTH(date_added) as month from quest_field_results WHERE YEAR(date_added) = $result order by month asc;";
	$sql_result_months = mysql_query($sql_months) or die(mysql_error());
	$months = array();
	$counter = 0;
	while($month = mysql_fetch_assoc($sql_result_months)){
		$months[$counter] = $month['month'];
		//echo $monthnames[$months[$counter]-1];
		$counter++;
	}

	//$strXML will be used to store the entire XML document generated
	//Generate the chart element
	$strXML = "<chart palette='2' caption='Number of stoves (in use and not in use)' subCaption='For the year ".$result."' lineThickness='1' showValues='0' decimals='0' formatNumberScale='0' xAxisName='Month' yAxisName='Number of Stoves' numberSuffix=' ' useRoundEdges='0'>";
	$strXML .="<categories>";
	foreach($months as $dist_month){
		$sql_get_data = "SELECT COUNT(*) AS total_distributed FROM quest_field_results WHERE MONTH(date_added) = $dist_month;";
		//echo $sql_get_data;
		$sql_result_get_data = mysql_query($sql_get_data) or die(mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		//echo $distributed;
		$strXML .= "<category label='". $monthnames[$dist_month-1] . "'/>";
			//free the resultset
			//mysql_free_result($get_data_resultset);
			//echo $monthnames[1]."  ".$distributed.'<br />';
	}
	$strXML .="</categories>";
	$strXML .="<dataset seriesName='In Use'  >";
	foreach($months as $dist_month){
		$sql_get_data = "SELECT COUNT(*) AS total_distributed FROM quest_field_results WHERE field_id IN (SELECT field_id FROM quest_fields WHERE field_name='Are you still using the Hifadhi stove?') AND value='Yes'AND MONTH(date_added) = $dist_month;";
		//echo $sql_get_data;
		$sql_result_get_data = mysql_query($sql_get_data) or die(mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		//echo $distributed;
		$strXML .= "<set value='".$distributed."'/>";
			//free the resultset
			//mysql_free_result($get_data_resultset);
			//echo $monthnames[1]."  ".$distributed.'<br />';
	}
	$strXML .="</dataset>";
	$strXML .="<dataset seriesName='Not In Use'  >";
	foreach($months as $dist_month){
		$sql_get_data = "SELECT COUNT(*) AS total_distributed FROM quest_field_results WHERE field_id IN (SELECT field_id FROM quest_fields WHERE field_name='Are you still using the Hifadhi stove?') AND value='No'AND  MONTH(date_added) = $dist_month;";
		//echo $sql_get_data;
		$sql_result_get_data = mysql_query($sql_get_data) or die(mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		//echo $distributed;
		$strXML .= "<set value='".$distributed."'/>";
			//free the resultset
			//mysql_free_result($get_data_resultset);
			//echo $monthnames[1]."  ".$distributed.'<br />';
	}
	$strXML .="</dataset>";
	mysql_close($link);

	//Finally, close <chart> element
	$strXML .= "</chart>";
	
	//Create the chart - Pie 3D Chart with data from $strXML
	echo renderChart("FusionCharts/Charts/StackedColumn2D.swf", "", $strXML, "ChartId", "800", "400", "0", "1");
?>
</CENTER>
</div> <!-- End Tab 2-->

  				</div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->
