<?php
include("DBConn.php");
$link = connectToDB();
$year = date('Y');
?>
 <script type="text/javascript">
   var chart = new FusionCharts("FusionCharts/Charts/MSLine.swf", "ChartId", "900", "550", "1", "1");
   chart.setDataURL("xml/who.php");	
   chart.addParam("WMode", "Transparent");	   
   chart.render("Distributed");
</script>
<SCRIPT LANGUAGE="Javascript" SRC="FusionCharts/Code/FusionCharts/FusionCharts.js"></SCRIPT>
<script type="text/javascript">
$(document).ready(function() {
	getReports();
	   $(".Reporting_Year").change(function() { 
			getReports();
	   });
});
function getReports(){
	
   	var year_chosen = $("#Reporting_Year").attr('value');
   	$.get('who.php',{ year: year_chosen }, function(data) {
		  $('#Distributed').html(data);
	});	
}
</script>
<style type="text/css">
select {
	width: 250;
} 
.chart_container{
	border-left: 2px solid #DDD;
	border-bottom: 2px solid #DDD;
	border-right: 2px solid #DDD;
	padding: 3px;
	margin-bottom:15px;
	width:950px;
	margin-left:auto;
	margin-right:auto
}
 
.xtop{
min-width:1300px;
}
#Larger_Graph{
width:900px;
}
 

</style>

<div class="span9">
	<div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/reports/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">		
    			<div id="actions">

            <div id="actionbutton">
            Year: <select name="Reporting_Year" id="Reporting_Year" class="Reporting_Year">
	<?php
	$sql_year = "SELECT distinct YEAR(dist_date) as yr from distributions order by dist_date desc";
	$sql_result_year = mysql_query($sql_year) or die(mysql_error());
	$years= array();
	$counter = 0;

	while($year_resultset = mysql_fetch_assoc($sql_result_year)){
		$years[$counter] = $year_resultset['yr'];	
		//mysql_data_seek($sql_result_year);
		echo $years[$counter];
		$counter++;
	}
	foreach($years as $year_object){ 
		if($year == $year_object){?>
			<option value=<?php echo $year_object?> selected><?php echo  $year_object?>
			</option>
		<?php }
		else{
		?>
			<option value=<?php echo $year_object?>><?php echo $year_object?>
			</option>
		<?php
		}	
	}
	?>
</select>


</div></div>
<div id="Distributed" style="z-index: -20"></div>
</div>
</div>
</div>
</div> 

 <script type="text/javascript">
   var chart = new FusionCharts("FusionCharts/Charts/MSLine.swf", "ChartId", "900", "550", "1", "1");
   chart.setDataURL("xml/who.php");	
   chart.addParam("WMode", "Transparent");	   
   chart.render("Distributed");
</script>	