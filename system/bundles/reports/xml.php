<?php
//We've included ../Includes/FusionCharts.php and ../Includes/DBConn.php, which contains
//functions to help us easily embed the charts and connect to a database.
include("FusionCharts/Code/PHP/Includes/FusionCharts.php");
include("DBConn.php");
//echo date(DATE_FORMAT_DATEPICKER);
//echo date(DATE_FORMAT_DATEPICKER_MONTH);
// Connect to the DB
	$link = connectToDB();
?>
<HEAD>
 
<script type="text/javascript">
$(document).ready(function() {
	
	getReport();
	$('#filtered').submit(function() { 
		   var year_chosen = $("#distdate").attr('value');
		   var year = this.id;
		   var chart = new FusionCharts("FusionCharts/Charts/Line.swf", "ChartId", "900", "550", "0", "1");
		   chart.setDataURL("xml/dist_rate.php?year="+year_chosen);	
		   chart.addParam("WMode", "Transparent");	   
		   chart.render("Dist_Rate_Chart");
		});
	   $(".distdate").change(function() { 
			getReport();
	   });
	   
	 });
function getReport(){

   	var year_chosen = $("#distdate").attr('value');
   	$.get('xml/dist_rate.php',{ year: year_chosen}, function(data) {
		  $('#Dist_Rate_Chart').html(data);
		  
	});	
		
}
</script>
<style type="text/css">
select {
	width: 250;
} 
.chart_container{
	border-left: 2px solid #DDD;
	border-bottom: 2px solid #DDD;
	border-right: 2px solid #DDD;
	padding: 3px;
	margin-bottom:15px;
	width:950px;
	margin-left:auto;
	margin-right:auto
}
 
.xtop{
min-width:1300px;
}
#Larger_Graph{
width:900px;
}
 

</style>
	<TITLE>
	Climate Pal - Reports
	</TITLE>
	<?php
	//You need to include the following JS file, if you intend to embed the chart using JavaScript.
	//Embedding using JavaScripts avoids the "Click to Activate..." issue in Internet Explorer
	//When you make your own charts, make sure that the path to this JS file is correct. Else, you would get JavaScript errors.
	?>	
	<SCRIPT LANGUAGE="Javascript" SRC="FusionCharts/Code/FusionCharts/FusionCharts.js"></SCRIPT>
    <style type="text/css">
	<!--
	body {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	.text{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	
	-->
	</style>
</HEAD>
<BODY>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/reports/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab3">	
        
    			<div id="actions">

            <div id="actionbutton">
      		
  		<form class="form-horizontal"  action="" method="post" name="filtered" id="filtered">
      	<input name="form_name" type="hidden" value="filter_dist_rate">
      	 
      	 <legend>Customize Here</legend>
         <table width="100%" border="0" align="left" style="float:left;">
  	<tr>
    <td style="float:left;"><label class="control-label" for="inputFrom">Select Year:</label>&nbsp;&nbsp;<div class="input-append date" id="yearsOnly">
				<input id="distdate" name="distdate" class="span8" size="16" type="text" value="<?php echo " ".date(DATE_FORMAT_DATEPICKER_YEAR); ?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div></td>
    <td><button type="submit" class="btn btn-primary" name="ok" id="ok"> 
  		  		<i class="icon-filter"></i> Filter
  		  	</button></td>
  </tr>
</table>
</form>
  			
            </div>
           </div>


<div id="Dist_Rate_Chart" style="z-index: -20">The chart will appear within this DIV. This text will be replaced by the chart.</div>
</div>
</div>
</div>

 <script type="text/javascript">
   var chart = new FusionCharts("FusionCharts/Charts/Line.swf", "ChartId", "900", "550", "0", "1");
   chart.setDataURL("xml/dist_rate.php");	
   chart.addParam("WMode", "Transparent");	   
   chart.renderChart("Dist_Rate_Chart");
</script>	
</div> <!--/End, Tabbable-->
</div><!--/span 9-->
</BODY>
