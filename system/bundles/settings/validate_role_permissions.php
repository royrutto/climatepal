<?php
if(session_id() == '') {
	session_start();
}
if(!isset($_SESSION['session_id'])){
	include(DIR_WEB.'/index.php');
	exit();
}
$edit_mode=false;
if($form_name =='edit_role_permissions'){
	$edit_mode=true;
	$role_permission_id=$_POST['role_permission_id'];
}
//required fields
$role_error=0;
$permission_error=0;
$empty_input=false;
$role_permission_exists=false;
//posted values
$role = $_POST['role'];
$permission = $_POST['perm'];

$flag = 1;
$added_by = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session']);;
$date_added =  date(DATE_FORMAT_DEFAULT);
$date_updated = date(DATE_FORMAT_DEFAULT);

if (validateInput($edit_mode)){
	$page_id='8'; $page_title= 'Add Role Permission'; $path = '/settings/add_role_permissions.php';
	$page = DIR_BUNDLES.$path;
	include(DIR_TEMPLATE.'/template.php');
}else{
	if($edit_mode){
		$saved = $RolePermission->update_role_permissions($role_permission_id, $role, $permission, $added_by, $date_updated);
	}
	else{
	$saved = $RolePermission->save_role_permission($role, $permission, $added_by, $date_added);
	}
	if (isset($saved) &&! $saved){
		$role_permission_exists=true;
		$page_id='8'; $page_title= 'Add Role Permission'; $path = '/settings/add_role_permissions.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}else{
	
		$page_id='8'; $page_title= 'Role Permissions'; $path = '/settings/role_permissions.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}
}



/**
 * 
 * @return boolean
 */
//Check if the required fields are empty
function validateInput($edit_mode){
		global $role_error, $permission_error;		
		if($_POST['role']==""){
			$empty_input = true;
			$role_error=1;
		}
		if($_POST['perm']==""){
			$empty_input = true;
			$permission_error=1;
		}
				
	return $empty_input;
	
}

?>