<?php
if(session_id() == '') {
	session_start();
}
if(!isset($_SESSION['session_id'])){
	include(DIR_WEB.'/index.php');
	exit();
}
$edit_mode=false;
if($form_name =='edit_permission'){
	$edit_mode=true;
	$permission_id=$_POST['permission_id'];
}
//required fields
$description_error=0;
$empty_input=false;
$permission_exists=false;
//posted values
//$permission_id = $_POST['permission_id'];
$description = $_POST['description'];

$flag = 1;
$added_by = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session']);;
$date_added =  date(DATE_FORMAT_DEFAULT);
$date_updated = date(DATE_FORMAT_DEFAULT);

if (validateInput($edit_mode)){
	$page_id='9'; $page_title= 'Add Permission'; $path = '/settings/add_permissions.php';
	$page = DIR_BUNDLES.$path;
	include(DIR_TEMPLATE.'/template.php');
}else{
	if($edit_mode){
		$saved = $Permission->update_permission($permission_id,$description, $added_by, $date_updated);
	}
	else{
	$saved = $Permission->save_permission($description, $added_by, $date_added);
	}
	if (isset($saved) &&! $saved){
		$permission_exists=true;
		$page_id='9'; $page_title= 'Add Permission'; $path = '/settings/add_permissions.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}else{
	
		$page_id='9'; $page_title= 'Permissions'; $path = '/settings/permissions.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}
}



/**
 * 
 * @return boolean
 */
//Check if the required fields are empty
function validateInput($edit_mode){
		global $description_error;		
		if($_POST['description']==""){
			$empty_input = true;
			$description_error=1;
		}
		
	return $empty_input;
	
}

?>