<?php 
	$all_roles = $Model->get_all_roles();
?>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/settings/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">
      	<div id="actions">
 
            <div id="actionbutton">
      		<a href="<?php echo BASE_URL ;?>roles/add" class="btn btn-primary"><i class="icon-user icon-white"></i> Add New Role</a>
  			
            <div class="btn-group">
 				<a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#">
 					<i class="icon-filter"></i> Filter
    				<span class="caret"></span>
  				</a>
  				<ul class="dropdown-menu">
    			<!-- dropdown menu links -->
    				<li><a href="#">Active Roles</a></li>
					<li><a href="#">Inactive Roles</a></li>
				</ul>
			</div>
            
			<a href="<?php echo BASE_URL ;?>roles" class="btn btn-inverse"><i class="icon-refresh icon-white"></i> Refresh</a>
            <a id = 'delete' href="#myModal" class="btn btn-danger hidden" data-toggle="modal"><i class="icon-trash icon-white"></i> Delete Selection</a>
            </div>
           </div>
	
      					<table  cellpadding="0" cellspacing="0" border="0" class="display table table-bordered table-hover" id="example" width="100%">	<thead>
		<tr>
			<th class="chk"><input type="checkbox" name="vehicle" value="Bike"></th>					   
					      		<th>Count</th>
                                <th>Role Name</th>
					      		<th>Role Description</th>
                                <th>Role Status</th>
					      		<th>Actions</th>
		</tr>
	</thead>
	<tbody>
							<?php foreach ($all_roles as $role){
								?>
							   <tr>
  								<td class="chk"><input type="checkbox" name="gender_id" value="<?php echo $role['role_id']; ?>"></td>
  								<td><?php echo $role['role_id']; ?></td>
                                <td><?php echo $role['role_name']; ?></td>
  								<td><?php echo $role['description']; ?></td>
                                <td><?php if($role['status']==1) echo "Active"; else echo "Inactive"; ?></td>
  								<td>
  								  <div class="bs-docs-example tooltip-demo">
                      				<a href="#" title="View"><span class="badge badge-success"><i class="icon-search icon-white"></i></span></a> 
                      				<a href="<?php echo BASE_URL ;?>roles/edit/<?php echo $role['role_id']; ?>" title="Edit"><span class="badge badge-info"><i class="icon-pencil icon-white"></i></span></a> 
                      				<a href="#myModal" title="Delete" data-toggle="modal"><span class="badge badge-important"><i class="icon-trash icon-white"></i></span></a>
                    			  </div>
								  </td>
							   </tr>
							   <?php } ?>
	
	</tbody>
	<tfoot>
		<tr>
			<th class="chk"><input type="checkbox" name="vehicle" value="Bike"></th>					   
					      		<th>Count</th>
                                <th>Role Name</th>
					      		<th>Role Description</th>
                                <th>Role Status</th>
					      		<th>Actions</th>
		</tr>
	</tfoot>
						    </table>
            	</div> <!-- End Tab 1-->

    			 </div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`roles` (`role_name`, `decription`, `date_added`, `date_updated`) VALUES ('Administrator', 'Controls the whole system', '2013-02-07', '');
       -->
	