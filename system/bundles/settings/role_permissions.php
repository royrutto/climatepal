<?php 
	$all_role_permissions = $Model->get_all_role_permissions();
?>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/settings/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">
      	<div id="actions">
 
            <div id="actionbutton">
      		<a href="<?php echo BASE_URL ;?>role_permissions/add" class="btn btn-primary"><i class="icon-user icon-white"></i> Add New Role Permission</a>
  			
            <a href="<?php echo BASE_URL ;?>role_permissions" class="btn btn-inverse"><i class="icon-refresh icon-white"></i> Refresh</a>
            <a id = 'delete' href="#myModal" class="btn btn-danger hidden" data-toggle="modal"><i class="icon-trash icon-white"></i> Delete Selection</a>
            </div>
           </div>
	
      					<table  cellpadding="0" cellspacing="0" border="0" class="display table table-bordered table-hover" id="example" width="100%">	<thead>
		<tr>
			<th class="chk"><input type="checkbox" name="vehicle" value="Bike"></th>					   
					      		<th>Count</th>
                                <th>Role Name</th>
					      		<th>Permission</th>
                                <th>Actions</th>
		</tr>
	</thead>
	<tbody>
							<?php foreach ($all_role_permissions as $permission){
								?>
							   <tr>
  								<td class="chk"><input type="checkbox" name="permission_id" value=" <?php echo $permission['id']; ?>"></td>
  								<td><?php echo $permission['id']; ?></td>
                                <td><?php echo $permission['role']; ?></td>
  								<td><?php echo $permission['permission']; ?></td>
                                <td>
  								  <div class="bs-docs-example tooltip-demo">
                      				<a href="#" title="View"><span class="badge badge-success"><i class="icon-search icon-white"></i></span></a> 
                      				<a href="<?php echo BASE_URL ;?>role_permissions/edit/<?php echo $permission['id']; ?>" title="Edit"><span class="badge badge-info"><i class="icon-pencil icon-white"></i></span></a> 
                      				<a href="#myModal" title="Delete" data-toggle="modal"><span class="badge badge-important"><i class="icon-trash icon-white"></i></span></a>
                    			  </div>
								  </td>
							   </tr>
							   <?php } ?>
	
	</tbody>
	<tfoot>
		<tr>
			<th class="chk"><input type="checkbox" name="vehicle" value="Bike"></th>					   
					      		<th>Count</th>
                                <th>Role Name</th>
					      		<th>Permission</th>
                                <th>Actions</th>
		</tr>
	</tfoot>
						    </table>
            	</div> <!-- End Tab 1-->

    			 </div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`roles` (`role_name`, `decription`, `date_added`, `date_updated`) VALUES ('Administrator', 'Controls the whole system', '2013-02-07', '');
       -->
	