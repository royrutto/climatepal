<?php
if(session_id() == '') {
	session_start();
}
if(!isset($_SESSION['session_id'])){
	include(DIR_WEB.'/index.php');
	exit();
}
$edit_mode=false;
if($form_name =='edit_role'){
	$edit_mode=true;
	$role_id=$_POST['role_id'];
}
//required fields
$description_error=0;
$empty_input=false;
$role_exists=false;
//posted values
$name = $_POST['name'];
$description = $_POST['description'];
$status = $_POST['status'];

$flag = 1;
$added_by = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session']);;
$date_added =  date(DATE_FORMAT_DEFAULT);
$date_updated = date(DATE_FORMAT_DEFAULT);

if (validateInput($edit_mode)){
	$page_id='8'; $page_title= 'Add Role'; $path = '/settings/add_roles.php';
	$page = DIR_BUNDLES.$path;
	include(DIR_TEMPLATE.'/template.php');
}else{
	if($edit_mode){
		$saved = $Role->update_roles($role_id, $name,$description, $status, $added_by, $date_updated);
	}
	else{
	$saved = $Role->save_role($name, $description, $added_by, $date_added);
	}
	if (isset($saved) &&! $saved){
		$role_exists=true;
		$page_id='8'; $page_title= 'Add Role'; $path = '/settings/add_roles.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}else{
	
		$page_id='8'; $page_title= 'Roles'; $path = '/settings/roles.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}
}



/**
 * 
 * @return boolean
 */
//Check if the required fields are empty
function validateInput($edit_mode){
		global $description_error, $name_error, $status_error;	
		$empty_input = false;
		if($_POST['name']==""){
			$empty_input = true;
			$name_error=1;
		}
		if($_POST['description']==""){
			$empty_input = true;
			$description_error=1;
		}
		if($_POST['status']==""){
			$empty_input = true;
			$status_error=1;
		}
		
	return $empty_input;
	
}

?>