<?php 
$edit_mode = false;
if (isset ($item_id) && !$item_id==''){
	$edit_mode =true;
}

if($edit_mode){
	$role_details  = $Model->get_role_by_id($item_id);
	
	if(count($role_details)>0){
	
		$role_id = $role_details['role_id'];
		$name = $role_details['role_name'];
		$description = $role_details['description'];
		$status = $role_details['status'];
		
	}
}



?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab">
	    		<span><?php echo $page_title?></span></a>
	    	</li>		
  		</ul>
  		<?php if (isset($empty_input) && $empty_input){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
      	<form class="form-horizontal"  action="<?php echo BASE_URL ;?>index.php" method="post">
        <?php if ($edit_mode){?>
      	 	<input name="form_name" type="hidden" value="edit_role">
      	 	<input name="role_id" type="hidden" value="<?php echo $item_id ;?>">
      	 <?php }else {?>
      	 <input name="form_name" type="hidden" value="add_role">
         <?php } ?>
      	 <legend>Role Details</legend>
      	 <div class="control-group">
      	 <span class="add-on">The fields marked <i class="icon-asterisk"></i> are mandatory.</span> 
      	 </div>
         <div class="control-group <?php if (isset($name_error) && $name_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Role Name</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input  name="name" id="appendedInput" type="text" placeholder="Role Name" value="<?php if(isset($_POST['name'])) { echo $_POST['name'];}else if(isset($name)) {echo $name;}?>">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		       <?php if(isset($name_error) && $name_error==1){?><span class="help-inline">Role Name is required.</span><?php }?>
		    </div>
		  </div>
		   <div class="control-group <?php if (isset($description_error) && $description_error==1){echo 'error';}?>">
           
		    <label class="control-label" for="inputEmail"> Role Description</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input  name="description" id="appendedInput" type="text" placeholder="Role Description" value="<?php if(isset($_POST['description'])) { echo $_POST['description'];}else if(isset($description)) {echo $description;}?>">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		       <?php if(isset($description_error) && $description_error==1){?><span class="help-inline">Role Description is required.</span><?php }?>
		    </div>
		  </div>
          <div class="control-group" <?php if (isset($status_error) && $status_error==1){echo 'error';}?>>
		    <label class="control-label" for="inputStatus">Role Status</label>
		    <div class="controls"><div class="input-append">
		     	<select name="status" class="">
		     	<?php if(isset($_POST['status'])||isset($status)) {
		       	if (isset($_POST['status'])){
					$status = $_POST['status'];
				}else $status = $status;
		       }?>
		       		<option value=" " selected>----------Select Status------------</option>
                    <option value="0" <?php if (isset($status) && $status == 0)echo 'selected';?>>Inactive</option>
				 	<option value="1" <?php if (isset($status) && $status == 1)echo 'selected';?>>Active</option>
				  	
  				</select>
  				
		     <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		       <?php if(isset($status_error) && $status_error==1){?><span class="help-inline">Role Status is required.</span><?php }?>
		    </div>
		  <div class="form-actions">
  		  	<button type="submit" class="btn btn-primary">
  		  		<i class="icon-hdd icon-white"></i> 
  		  		 	<?php if ($edit_mode){?>Save
  		  		 	<?php }else{?>
  		  		 		Save Role
  		  		 	<?php }?>
  		  	</button>
  			<a class="btn" href="<?php echo BASE_URL ;?>users">Cancel</a>
		</div>
		</form>
		
      </div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`employees` (`surname`, `firstname`, `gender`, `telephone`, `postal_address`, `postal_code`, `town`, `email`, `added_by`, `date_added`, `date_updated`) VALUES ('Doe', 'John', 1, '0722123456', 'P.O. BOX 123456', '00200', 'Nairobi', 'name@example.com', 1, '2013-02-07', '2013-02-07'); -->
	