<?php 
$edit_mode = false;
if (isset ($item_id) && !$item_id==''){
	$edit_mode =true;
}

if($edit_mode){
	$role_permission_details  = $Model->get_role_permission_by_id($item_id);
	
	if(count($role_permission_details)>0){
		
		$id = $role_permission_details['id'];
		$role = $role_permission_details['role'];
		$permission = $role_permission_details['permission'];
		$roleID = $role_permission_details['roleID'];
		$perID = $role_permission_details['perID'];
		
		/*$role_query = mysql_query("Select role_id from roles where role_name = '$role'") or die(mysql_error());
		$role_result = mysql_fetch_array($role_query);
		$role_row = $role_result[0];
		
		$permission_query = mysql_query("Select id from permissions where description = '$permission'") or die(mysql_error());
		$permission_result = mysql_fetch_array($role_query);
		$permission_row = $permission_result[0];*/
		
	}
}



?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab">
	    		<span><?php echo $page_title?></span></a>
	    	</li>		
  		</ul>
  		<?php if (isset($empty_input) && $empty_input){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
      	<form class="form-horizontal"  action="<?php echo BASE_URL ;?>index.php" method="post">
        <?php if ($edit_mode){?>
      	 	<input name="form_name" type="hidden" value="edit_role_permissions">
      	 	<input name="role_permission_id" type="hidden" value="<?php echo $item_id ;?>">
      	 <?php }else {?>
      	 <input name="form_name" type="hidden" value="add_role_permissions">
         <?php } ?>
      	 <legend>Role Permission Details</legend>
      	 <div class="control-group <?php if (isset($role_error) && $role_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Role</label>
		    <div class="controls">
		       <div class="input-append">
		       <select name ="role" class="" >
		       <?php if(isset($_POST['role'])||isset($role)) {
		       	if (isset($_POST['role'])){
					$role = $_POST['role'];
				}else $role = $role;
		       }
				?>
                <?php if($edit_mode){ ?>
					<option value="<?php echo $roleID; ?>" selected><?php echo $role; ?></option>
				 <?php $roles1  = $Model->get_all_roles();
					foreach($roles1 as $rp1){
					?>
		     		<option value="<?php echo $rp1['id']; ?>"> <?php echo $rp1['name'];?></option>
                    <?php }}else{
				 ?>
		       		<option value="" selected>------------Select Role-----------</option>
                    <?php $roles2  = $Model->get_all_roles();
					foreach($roles2 as $rp2){
					?>
		     		<option value="<?php echo $rp2['id']; ?>"> <?php echo $rp2['name'];?></option>
                    <?php } } ?>
				 	
  				</select>
		 		<span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($role_error) && $role_error==1){?><span class="help-inline">Please Select the appropriate Role.</span><?php }?>
		    </div>
		  </div>
		   <div class="control-group <?php if (isset($permission_error) && $permission_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Permission</label>
		    <div class="controls">
		       <div class="input-append">
		       <select name ="perm" class="" >
		       <?php if(isset($_POST['perm'])||isset($permission)) {
		       	if (isset($_POST['perm'])){
					$permission = $_POST['perm'];
				}else $permission = $permission;
		       }
				?>
                <?php if($edit_mode){ ?>
					<option value="<?php echo $perID; ?>" selected><?php echo $permission; ?></option><?php $permit  = $Model->get_all_permissions();
					foreach($permit as $r_p){
					?>
		     		<option value="<?php echo $r_p['id']; ?>"><?php echo $r_p['name'];?></option>
                    <?php } }else{
				 ?>
		       		<option value="" selected>-------Select Permission------</option>
                    <?php $permit  = $Model->get_all_permissions();
					foreach($permit as $r_p){
					?>
		     		<option value="<?php echo $r_p['id']; ?>"><?php echo $r_p['name'];?></option>
                    <?php } } ?>
				 	
  				</select>
		 		<span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($permission_error) && $permission_error==1){?><span class="help-inline">Please Select the appropriate Permission.</span><?php }?>
		    </div>
		  </div>
		  <div class="form-actions">
  		  	<button type="submit" class="btn btn-primary">
  		  		<i class="icon-hdd icon-white"></i> 
  		  		 	<?php if ($edit_mode){?>Save
  		  		 	<?php }else{?>
  		  		 		Save Role Permission
  		  		 	<?php }?>
  		  	</button>
  			<a class="btn" href="<?php echo BASE_URL ;?>role_permissions">Cancel</a>
		</div>
		</form>
		
      </div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`employees` (`surname`, `firstname`, `gender`, `telephone`, `postal_address`, `postal_code`, `town`, `email`, `added_by`, `date_added`, `date_updated`) VALUES ('Doe', 'John', 1, '0722123456', 'P.O. BOX 123456', '00200', 'Nairobi', 'name@example.com', 1, '2013-02-07', '2013-02-07'); -->
	