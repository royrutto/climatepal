<?php 
$edit_mode = false;
if (isset ($item_id) && !$item_id==''){
	$edit_mode =true;
}
if($edit_mode){
	$user_details  = $Model->get_user_by_id($item_id);
	
	if(count($user_details)>0){
	
		$surname = $user_details['surname'];
		$firstname = $user_details['firstname'];
		$gender = $user_details['gender'];
		$telephone = $user_details['telephone'];
		$postal_address = $user_details['postal_address'];
		$postal_code = $user_details['postal_code'];
		$postal_town = $user_details['town'];
		$email = $user_details['email'];
		$username = $user_details['username'];
		$role_id = $user_details['role_id'];
		$selected_locations  = $Model->get_all_allowed_locations($item_id);
	}
}

$all_locations = $Model->get_all_locations();
if (isset($_POST['locations']))
$selected_locations = $_POST['locations'];
?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab">
	    		<span><?php echo $page_title?></span></a>
	    	</li>		
  		</ul>
  		<?php if (isset($empty_input_exists) && $empty_input_exists){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
      	<form class="form-horizontal"  action="<?php echo BASE_URL ;?>index.php" method="post">
      	<?php if ($edit_mode){?>
      	 	<input name="form_name" type="hidden" value="edit_user">
      	 	<input name="user_id" type="hidden" value="<?php echo $item_id ;?>">
      	 <?php }else {?>
      	 	 <input name="form_name" type="hidden" value="add_user">
      	 <?php }?>
      	 <legend>User Details</legend>
      	 <div class="control-group">
      	 <span class="add-on">The fields marked <i class="icon-asterisk"></i> are mandatory.</span> 
      	 </div>
		   <div class="control-group <?php if (isset($surname_error) && $surname_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Surname</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input  name="surname" id="appendedInput" type="text" placeholder="Surname" 
		       	value="<?php if(isset($_POST['surname'])) {echo $_POST['surname'];} else if(isset($surname)) {echo $surname;}?>" class="surname" 
		       	data-toggle="popover" data-placement="right" data-content="Enter letters only." title="" data-original-title="Only text required">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		       <?php if(isset($surname_error) && $surname_error==1){?><span class="help-inline">Surname is required.</span><?php }?>
		    </div>
		  </div>
		   <div class="control-group <?php if (isset($firstname_error) && $firstname_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">First name</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="firstname" id="appendedInput" type="text" placeholder="First name" 
		       	value="<?php if(isset($_POST['firstname'])) {echo $_POST['firstname'];}else if(isset($firstname)) {echo $firstname;}?>" class="firstname"
		       	data-toggle="popover" data-placement="right" data-content="Enter letters only." title="" data-original-title="Only text required">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($firstname_error) && $firstname_error==1){?><span class="help-inline">Firstname is required.</span><?php }?>
		    </div>
		  </div>
		   <div class="control-group <?php if (isset($gender_error) && $gender_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Gender</label>
		    <div class="controls">
		       <div class="input-append">
		       <select name ="gender" class="" >
		       <?php if(isset($_POST['gender'])||isset($gender)) {
		       	if (isset($_POST['gender'])){
					$gender = $_POST['gender'];
				}else $gender = $gender;
		       }
				?>
		       		<option value="0" selected>--------------Select-------------</option>
		     		<option value="1" <?php if (isset($gender) && $gender == 1)echo 'selected';?>>Male</option>
				 	<option value="2" <?php if (isset($gender) && $gender == 2)echo 'selected';?>>Female</option>
				
  				</select>
		 		<span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($gender_error) && $gender_error==1){?><span class="help-inline">Gender is required.</span><?php }?>
		    </div>
		  </div>
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Mobile</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="telephone" id="appendedInput" type="text" placeholder="e.g. 0722123456" 
		       	value="<?php if(isset($_POST['telephone'])) {echo $_POST['telephone'];}else if(isset($telephone)) {echo $telephone;}?>" class="mobile_number"
		       	data-toggle="popover" data-placement="right" data-content="Enter digits only." title="" data-original-title="Only numbers required">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		   <div class="control-group <?php if (isset($email_error) && $email_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Email</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="email" id="appendedInput" type="text" placeholder="name@example.com" 
		       	value="<?php if(isset($_POST['email'])) {echo $_POST['email'];}else if(isset($email)) {echo $email;}?>">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($email_error) && $email_error==1){?><span class="help-inline">Email is required.</span><?php }?>
		    </div>
		  </div>
		   <div class="control-group <?php if (isset($postal_address_error) && $postal_address_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputError">Address</label>
		    <div class="controls">
		       <div class="input-append">
		       	<textarea name="postal_address" rows="3">
		       	<?php if(isset($_POST['postal_address'])) {echo $_POST['postal_address'];}else if(isset($postal_address)) {echo $postal_address;}?>
		       	</textarea>
		       </div>
		         <?php if (isset($postal_address_error) && $postal_address_error==1){?><span class="help-inline">Postal Address is required.</span><?php }?>
		    </div>
		  </div>
		   <div class="control-group <?php if (isset($postal_code_error) && $postal_code_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Postal Code</label>
		    <div class="controls">
		       
		       	<input name="postal_code" id="appendedInput" type="text" placeholder="e.g. 00200" 
		       	value="<?php if(isset($_POST['postal_code'])) {echo $_POST['postal_code'];}else if(isset($postal_code)) {echo $postal_code;}?>"?>
		       
		      
		        <?php if (isset($postal_code_error) && $postal_code_error==1){?><span class="help-inline">Postal Code is required</span><?php }?>
		    </div>
		  </div>
		   <div class="control-group <?php if (isset($town_error) && $town_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Town</label>
		    <div class="controls">
		      
		       	<input name="town" id="appendedInput" type="text" placeholder="e.g. Nairobi" 
		       	value="<?php if(isset($_POST['town'])) {echo $_POST['town'];}else if(isset($postal_town)) {echo $postal_town;}?>">
		       	
		      
		        <?php if (isset($town_error) && $town_error==1){?><span class="help-inline">Town is required.</span><?php }?>
		    </div>
		  </div>
		  <legend>Access Details</legend>
		    <?php if(isset($user_exists) && $user_exists){?>
		  <div class="control-group error">
		    <label class="control-label" for="inputPassword"></label>
		    <div class="controls">
  				<span class="help-inline">Username already exists. Try different username.</span>
		    </div>
		  </div>
		  <?php }?>
		  <div class="control-group <?php if (isset($username_error) && $username_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Username</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input  name="username" id="appendedInput" type="text" placeholder="Username" 
		       	value="<?php if(isset($_POST['username'])) {echo $_POST['username'];} else if(isset($username)) {echo $username;}?>" >
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		       <?php if (isset($username_error) && $username_error==1){?><span class="help-inline">Username is required.</span><?php }?>
		    </div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputPassword">Password</label>
		    <div class="controls">
				<div class="input-append">
		     		<label class="checkbox">
      				<input name="defaultpass" id="defaultpass" type="checkbox" checked="checked"> Use default password
    				</label>
					</div>
			</div>
		</div>
		  <?php if (!$edit_mode){?>
		  <div class="control-group <?php if (isset($password_error) && $password_error==1){echo 'error';}?>" id="cg_password">
		    <label class="control-label" for="inputPassword">Password</label>
		    <div class="controls">
		    	<div class="input-append">
		      <input name="password" type="password" id="inputPassword" placeholder="Password" value="<?php if(isset($_POST['password'])) echo $_POST['password'];?>">
		       <span class="add-on"><i class="icon-asterisk"></i></span>
		      </div>
		       <?php if (isset($password_error) && $password_error==1){?><span class="help-inline">Password is required.</span><?php }?>
		    </div>
		  </div>
		  <div class="control-group <?php if (isset($password2_error) && $password2_error==1){echo 'error';}?>" id="cg_password2">
		    <label class="control-label" for="inputPasswords">Repeat password.</label>
		    <div class="controls">
		    <div class="input-append">
		      <input name="password2"type="password" id="inputPassword" placeholder="Retype Password" value="<?php if(isset($_POST['password2'])) echo $_POST['password2'];?>">
		   	  <span class="add-on"><i class="icon-asterisk"></i></span>
		   	</div>
		   	 <?php if (isset($password2_error) && $password2_error==1){?><span class="help-inline">Re-type password again</span><?php }?>
		    </div>
		  </div>
		  
		  <?php if(isset($passwords_match) && !$passwords_match){?>
		  <div class="control-group error">
		    <label class="control-label" for="inputPassword"></label>
		    <div class="controls">
  				<span class="help-inline">Passwords do not match</span>
		    </div>
		  </div>
		  <?php }?>
		  <?php }?>
		  <div class="control-group <?php if (isset($role_error) && $role_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">User Role</label>
		    <div class="controls">
		       <div class="input-append">
		       <select name ="role" class="" >
		       <?php if(isset($_POST['role'])||isset($role_id)) {
		       	if (isset($_POST['role'])){
					$role_id = $_POST['role'];
				}else $role_id = $role_id;
		       }
				?>
                <?php if($edit_mode){ ?>
			
				 <?php $roles  = $Model->get_all_roles();
					foreach($roles as $role){
					?>
		     		<option <?php if($role['role_id']==$role_id)echo 'selected';?> value="<?php echo $role['role_id']; ?>"> <?php echo $role['role_name'];?></option>
                    <?php }}else{
				 ?>
		       		<option value="" selected>------------Select Role-----------</option>
                    <?php
                    $roles  = $Model->get_all_roles();
					foreach($roles as $role){
					?>
		     		<option value="<?php echo $role['role_id']; ?>" <?php if (isset($role_id) && $role_id == $role['role_id'])echo 'selected';?>> <?php echo $role['role_name'];?></option>
                    <?php } } ?>
				 	
  				</select>
		 		<span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($role_error) && $role_error==1){?><span class="help-inline">Please Select the appropriate Gender.</span><?php }?>
		    </div>
		  </div>
		 <div class="control-group <?php if (isset($location_error) && $location_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputPassword">Locations</label>
		    <div class="controls">
		      <?php if (isset($location_error) && $location_error==1){?><span class="help-inline">Please select a location.</span><?php }?>
  				<div class="locations_container">
  				<?php foreach ($all_locations as $location){
  						$selected = false;
  						if(isset($selected_locations)) {
							for ($i=0; $i<count($selected_locations); $i++) {
								$locationid =$selected_locations[$i];
								if ($locationid==$location['id'])$selected=true;
							}
						}
  					?>
  				<label class="checkbox">
				    <input type="checkbox" name="locations[]" value ="<?php echo $location['id'];?>" <?php if($selected)echo 'checked';?>/> <?php echo $location['name'];?><br />
				 </label>	
				<?php }?>
				</div>
				
		    </div>
		  </div>
		  <div class="form-actions">
  		  	<button type="submit" class="btn btn-primary">
  		  		<i class="icon-hdd icon-white"></i> 
  		  		 	<?php if ($edit_mode){?>Save
  		  		 	<?php }else{?>
  		  		 		Save User
  		  		 	<?php }?>
  		  	</button>
  			<a class="btn" href="<?php echo BASE_URL ;?>users">Cancel</a>
		</div>
		</form>
		
      </div><!--/span 9-->