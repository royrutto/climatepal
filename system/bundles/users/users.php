<?php 
	$all_users = $Model->get_all_users();
?>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab1">
	    		<span><?php echo $page_title?></span></a>
	    	</li>
	    	
	    			
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">
      	<div id="actions">
 
            <div id="actionbutton">
      		<a href="<?php echo BASE_URL ;?>users/add" class="btn btn-primary"><i class="icon-user icon-white"></i> Add New User</a>
  
            <div class="btn-group">
 				<a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#">
 					<i class="icon-filter"></i> Filter
    				<span class="caret"></span>
  				</a>
  				<ul class="dropdown-menu">
    			<!-- dropdown menu links -->
    				<li><a href="#">Active users</a></li>
					<li><a href="#">Inactive users</a></li>
					<li><a href="#">Logged-in users</a></li>
  				</ul>
			</div>
			<a href="<?php echo BASE_URL ;?>users"class="btn btn-inverse"><i class="icon-refresh icon-white"></i> Refresh</a>
            <a id = 'delete' href="#myModal" class="btn btn-danger hidden" data-toggle="modal"><i class="icon-trash icon-white"></i> Delete Selection</a>
            </div>
           </div>
	
      					    
						    <table  cellpadding="0" cellspacing="0" border="0" class="display table table-bordered table-hover" id="example" width="100%">
	<thead>
		<tr>
			<th class="chk"><input type="checkbox" name="vehicle" value="Bike"></th>					   
					      		<th>Username</th>
					      		<th>Firstname</th>
					      		<th>Surname</th>
					      		<th>Role</th>
					      		<th>Status</th>
					      		<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($all_users as $user){
								?>
							   <tr>
  								<td class="chk"><input type="checkbox" name="user_id" value="<?php echo $user['user_id']; ?>"></td>
  								<td><?php echo $user['username']; ?></td>
  								<td><?php echo $user['firstname']; ?></td>
  								<td><?php echo $user['surname']; ?></td>
  								<td><?php echo $user['role_name']; ?></td>
  								<td><?php echo $user['status']; ?></td>
								  <td>
  								  <div class="bs-docs-example tooltip-demo">
                      				<a href="#" title="View"><span class="badge badge-success"><i class="icon-search icon-white"></i></span></a> 
                      				<a href="<?php echo BASE_URL ;?>users/edit/<?php echo $user['user_id']; ?>" title="Edit"><span class="badge badge-info"><i class="icon-pencil icon-white"></i></span></a> 
                      				<a href="#myModal" title="Delete" data-toggle="modal" class="deleteitem" 
                      				data-id="<?php echo $user['user_id']; ?>"
                      				data-itemname="<?php echo $user['username']; ?>">
                      				<span class="badge badge-important">
                      				<i class="icon-trash icon-white"></i></span>
                      				</a>
                    			  </div>
								  </td>
							   </tr>
							   <?php } ?>
	
	</tbody>
	<tfoot>
		<tr>
			<th class="chk"><input type="checkbox" name="vehicle" value="Bike"></th>					   
					      		<th>Username</th>
					      		<th>Firstname</th>
					      		<th>Surname</th>
					      		<th>Role</th>
					      		<th>Status</th>
					      		<th>Actions</th>
		</tr>
	</tfoot>
</table>
    			</div> <!-- End Tab 1-->

    			 <div class="tab-pane" id="tab2">
      				<p>Howdy, I'm in Section 2.</p>
      				
          				<?php echo date();?>

    			 </div> <!-- End Tab 2-->

  				</div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`roles` (`role_name`, `decription`, `date_added`, `date_updated`) VALUES ('Administrator', 'Controls the whole system', '2013-02-07', '');
       -->
	