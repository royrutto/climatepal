<?php
if(session_id() == '') {
	session_start();
}
if(!isset($_SESSION['session_id'])){
	include(DIR_WEB.'/index.php');
	exit();
}
$edit_mode=false;
if($form_name =='edit_user'){
	$edit_mode=true;
	$user_id=$_POST['user_id'];
}
//required fields
$surname_error=0;
$firstname_error=0;
$gender_error=0;
$telephone_error=0;
$email_error=0;
$username_error=0;
$password_error=0;
$password2_error=0;
$role_error=0;
$location_error=0;
$empty_input_exists=false;
$passwords_match=true;
$user_exists=false;
//posted values
$surname = $_POST['surname'];
$firstname = $_POST['firstname'];
$gender = $_POST['gender'];
$telephone = $_POST['telephone'];
$postal_address = $_POST['postal_address'];
$postal_code = $_POST['postal_code'];
$postal_town = $_POST['town'];
$email = $_POST['email'];
$username = $_POST['username'];
if (!$edit_mode){
	$password = $_POST['password'];
	$password2 = $_POST['password2'];
	if (isset($_POST['defaultpass'])){
		$password = '123456';
		$password2 = '123456';
	}
}
$role_id = $_POST['role'];
$locations="";
if (isset($_POST['locations'])) {
	$optionArray = $_POST['locations'];
	for ($i=0; $i<count($optionArray); $i++) {
		if ($i==0){
		$locations.=$optionArray[$i];
		}else{
			$locations.=','.$optionArray[$i];
		}
		
	}
}

$flag = 1;
$added_by = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session']);;

$date_added =  date(DATE_FORMAT_DEFAULT);
$date_updated =date(DATE_FORMAT_DEFAULT);
if (validateInput($edit_mode)){
	$page_id='4'; $page_title= 'Add Users'; $path = '/users/add_users.php';
	$page = DIR_BUNDLES.$path;
	include(DIR_TEMPLATE.'/template.php');
}else{
	
	if($edit_mode){
		$saved = $Users->update_user($user_id,$role_id, $username,$flag, $surname, $firstname, $gender, $telephone,$postal_address,
					$postal_code, $postal_town, $email, $added_by, $date_updated, $locations);
	}else{
		$password = secure_password($password);
		$saved = $Users->save_user($role_id, $username, $password, $flag, $surname, $firstname, $gender, $telephone, $postal_address, $postal_code, $postal_town, $email, $added_by, $date_added, $locations);
	}
	if (isset($saved) &&! $saved){
		$user_exists=true;
		$page_id='4'; $page_title= 'Add Users'; $path = '/users/add_users.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}else{
		$page_id='4'; $page_title= 'Users'; $path = '/users/users.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}
}


/**
 * 
 * @return boolean
 */
//Check if the required fields are empty
function validateInput($edit_mode){
		global $surname_error,
				$firstname_error,
				$gender_error,
				$telephone_error,
				$email_error,
				$username_error,
				$password_error,
				$password2_error,
				$role_error,
				$location_error,
				$empty_input_exists,
				$passwords_match;		
		if($_POST['surname']==""){
			$empty_input_exists = true;
			$surname_error=1;
		}
		if($_POST['firstname']==""){
			$empty_input_exists = true;
			$firstname_error=1;
		}
		if($_POST['gender']=="0"){
			$empty_input_exists = true;
			$gender_error=1;
		}
		if($_POST['telephone']==""){
			$empty_input_exists = true;
			$telephone_error=1;
		}
			
		if($_POST['email']==""){
			$empty_input_exists = true;
			$email_error=1;
		}
		if($_POST['username']==""){
			$empty_input_exists = true;
			$username_error=1;
		}
		if($_POST['username']==""){
			$empty_input_exists = true;
			$username_error=1;
		}
		
		if ($_POST['role']==""){
			$empty_input_exists = true;
			$role_error=1;
		}
		if (!isset($_POST['locations'])){
			$empty_input_exists = true;
			$location_error=1;
		}
		
		if (!isset($_POST['defaultpass'])){
			if(!$edit_mode && $_POST['password']==""){
				$empty_input_exists = true;
				$password_error=1;
			}
			if(!$edit_mode &&$_POST['password2']==""){
				$empty_input_exists = true;
				$password2_error=1;
			}
			
			if(!$edit_mode && $_POST['password']!=$_POST['password2']){
				$empty_input_exists = true;
				$passwords_match=false;
			}
		}
		
		if (strlen(!$edit_mode && $_POST['password']) > 72) { die('Password must be 72 characters or less'); }
		
			
	return $empty_input_exists;
	
}

function secure_password($plain_text_password){
	require_once DIR_SYS.'/PasswordHash.php';
	$t_hasher = new PasswordHash(8, FALSE);
	$hash = $t_hasher->HashPassword($plain_text_password);
	return $hash;
}

	





?>