<?php 
$edit_mode = false;
if (isset ($item_id) && !$item_id==''){
	$edit_mode =true;
}

if($edit_mode){
	$gender_details  = $Model->get_gender_by_id($item_id);
	
	if(count($gender_details)>0){
	
		$id = $gender_details['id'];
		$description = $gender_details['description'];
		
	}
}

?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab">
	    		<span><?php echo $page_title?></span></a>
	    	</li>		
  		</ul>
  		<?php if (isset($empty_input) && $empty_input){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
      	<form class="form-horizontal"  action="<?php echo BASE_URL ;?>index.php" method="post">
        <?php if ($edit_mode){?>
      	 	<input name="form_name" type="hidden" value="edit_gender">
      	 	<input name="gender_id" type="hidden" value="<?php echo $item_id ;?>">
      	 <?php }else {?>
      	 <input name="form_name" type="hidden" value="add_gender">
         <?php } ?>
      	 <legend>Gender Details</legend>
      	 <div class="control-group">
      	 <span class="add-on">The fields marked <i class="icon-asterisk"></i> are mandatory.</span> 
      	 </div>
		   <div class="control-group <?php if (isset($description_error) && $description_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Description</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input  name="description" id="appendedInput" type="text" placeholder="Gender Description" value="<?php if(isset($_POST['description'])) { echo $_POST['description'];}else if(isset($description)) {echo $description;}?>">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		       <?php if(isset($description_error) && $description_error==1){?><span class="help-inline">Gender Description is required.</span><?php }?>
		    </div>
		  </div>
		   <div class="form-actions">
  		  	<button type="submit" class="btn btn-primary">
  		  		<i class="icon-hdd icon-white"></i> 
  		  		 	<?php if ($edit_mode){?>Save
  		  		 	<?php }else{?>
  		  		 		Save Gender
  		  		 	<?php }?>
  		  	</button>
  			<a class="btn" href="<?php echo BASE_URL ;?>users">Cancel</a>
		</div>
		</form>
		
      </div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`employees` (`surname`, `firstname`, `gender`, `telephone`, `postal_address`, `postal_code`, `town`, `email`, `added_by`, `date_added`, `date_updated`) VALUES ('Doe', 'John', 1, '0722123456', 'P.O. BOX 123456', '00200', 'Nairobi', 'name@example.com', 1, '2013-02-07', '2013-02-07'); -->
	