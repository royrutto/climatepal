<?php
if(session_id() == '') {
	session_start();
}
if(!isset($_SESSION['session_id'])){
	include(DIR_WEB.'/index.php');
	exit();
}
$edit_mode=false;
if($form_name =='edit_location'){
	$edit_mode=true;
	$location_id=$_POST['location_id'];
}
//required fields
$description_error=0;
//posted values
//$location_id = $_POST['location_id'];
$description = $_POST['description'];
$empty_input=false;
$location_exists=false;

$flag = 1;
$added_by = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session']);;
$date_added =  date(DATE_FORMAT_DEFAULT);
$date_updated = date(DATE_FORMAT_DEFAULT);

if (validateInput($edit_mode)){
	$page_id='11'; $page_title= 'Add Locations'; $path = '/locations/add_location.php';
	$page = DIR_BUNDLES.$path;
	include(DIR_TEMPLATE.'/template.php');
}else{
	if($edit_mode){
		$saved = $Location->update_location($location_id, $description, $added_by, $date_updated);
	}
	else{
	$saved = $Location->save_location($description, $added_by, $date_added);
	}
	if (isset($saved) &&! $saved){
		$location_exists=true;
		$page_id='11'; $page_title= 'Add Locations'; $path = '/locations/add_location.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}else{
	
		$page_id='11'; $page_title= 'Locations'; $path = '/locations/locations.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}
}



/**
 * 
 * @return boolean
 */
//Check if the required fields are empty
function validateInput($edit_mode){
		global $description_error;		
		if($_POST['description']==""){
			$empty_input = true;
			$description_error=1;
		}
		
		
	return $empty_input;
	
}

?>