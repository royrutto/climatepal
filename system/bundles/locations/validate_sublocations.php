<?php
if(session_id() == '') {
	session_start();
}
if(!isset($_SESSION['session_id'])){
	include(DIR_WEB.'/index.php');
	exit();
}
$edit_mode=false;
if($form_name =='edit_sublocation'){
	$edit_mode=true;
	$sublocation_id=$_POST['sublocation_id'];
}
//required fields
$location_error = 0;
$description_error=0;
$empty_input=false;
$sublocation_exists=false;
//posted values
//$sublocation_id = $_POST['sublocation_id'];
$description = $_POST['description'];
$location = $_POST['location'];

$flag = 1;
$added_by = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session']);;
$date_added =  date(DATE_FORMAT_DEFAULT);
$date_updated = date(DATE_FORMAT_DEFAULT);

if (validateInput($edit_mode)){
	$page_id='11'; $page_title= 'Add Sub Locations'; $path = '/locations/add_sublocation.php';
	$page = DIR_BUNDLES.$path;
	include(DIR_TEMPLATE.'/template.php');
}else{
	if($edit_mode){
		$saved = $SubLocation->update_sublocation($sublocation_id, $description, $location, $added_by, $date_updated);
	}
	else{
	$saved = $SubLocation->save_sublocation($description, $location,$added_by, $date_added);
	}
	if (isset($saved) &&! $saved){
		$sublocation_exists=true;
		$page_id='11'; $page_title= 'Add Sub Locations'; $path = '/locations/add_sublocation.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}else{
	
		$page_id='11'; $page_title= 'Sub Locations'; $path = '/locations/sublocations.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}
}



/**
 * 
 * @return boolean
 */
//Check if the required fields are empty
function validateInput($edit_mode){
		global $description_error,$location_error;		
		if($_POST['description']==""){
			$empty_input = true;
			$description_error=1;
		}
		if($_POST['location']==""){
			$empty_input = true;
			$location_error=1;
		}
			
	return $empty_input;
	
}

?>