<?php
if(session_id() == '') {
	session_start();
}
if(!isset($_SESSION['session_id'])){
	include(DIR_WEB.'/index.php');
	exit();
}
$edit_mode=false;
if($form_name =='edit_district'){
	$edit_mode=true;
	$district_id=$_POST['district_id'];
}
//required fields
$description_error=0;
$latitude_error=0;
$longitude_error=0;

$empty_input=false;
$district_exists=false;
//posted values
//$district_id = $_POST['district_id'];
$description = $_POST['description'];
$lat = $_POST['latitude'];
$long = $_POST['longitude'];

$flag = 1;
$added_by = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session']);;
$date_added =  date(DATE_FORMAT_DEFAULT);
$date_updated = date(DATE_FORMAT_DEFAULT);

if (validateInput($edit_mode)){
	$page_id='10'; $page_title= 'Add Districts'; $path = '/locations/add_district.php';
	$page = DIR_BUNDLES.$path;
	include(DIR_TEMPLATE.'/template.php');
}else{
	if($edit_mode){
		$saved = $District->update_district($district_id,$description, $lat, $long, $added_by, $date_updated);
	}
	else{
	$saved = $District->save_district($description, $lat, $long, $added_by, $date_added);
	}
	if (isset($saved) &&! $saved){
		$district_exists=true;
		$page_id='10'; $page_title= 'Add Districts'; $path = '/locations/add_district.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}else{
	
		$page_id='10'; $page_title= 'Districts'; $path = '/locations/districts.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}
}



/**
 * 
 * @return boolean
 */
//Check if the required fields are empty
function validateInput($edit_mode){
		global $description_error,$latitude_error,$longitude_error;		
		if($_POST['description']==""){
			$empty_input = true;
			$description_error=1;
		}
		if($_POST['latitude']==""){
			$empty_input = true;
			$latitude_error=1;
		}
		if($_POST['longitude']==""){
			$empty_input = true;
			$longitude_error=1;
		}
		
	return $empty_input;
	
}

?>