<?php 
$edit_mode = false;
if (isset ($item_id) && !$item_id==''){
	$edit_mode =true;
}

if($edit_mode){
	$district_details  = $Model->get_district_by_id($item_id);
	
	if(count($district_details)>0){
	
		$id = $district_details['id'];
		$description = $district_details['name'];
		$latitude = $district_details['latitude'];
		$longitude = $district_details['longitude'];
		
	}
}



?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab">
	    		<span><?php echo $page_title?></span></a>
	    	</li>		
  		</ul>
  		<?php if (isset($empty_input) && $empty_input){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
      	<form class="form-horizontal"  action="<?php echo BASE_URL ;?>index.php" method="post">
        <?php if ($edit_mode){?>
      	 	<input name="form_name" type="hidden" value="edit_district">
      	 	<input name="district_id" type="hidden" value="<?php echo $item_id ;?>">
      	 <?php }else {?>
      	 <input name="form_name" type="hidden" value="add_district">
         <?php } ?>
      	 <legend>District Details</legend>
      	 <div class="control-group">
      	 <span class="add-on">The fields marked <i class="icon-asterisk"></i> are mandatory.</span> 
      	 </div>
		   <div class="control-group <?php if (isset($description_error) && $description_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">District Name</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input  name="description" id="appendedInput" type="text" placeholder="District Name" value="<?php if(isset($_POST['description'])) { echo $_POST['description'];}else if(isset($description)) {echo $description;}?>">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		       <?php if(isset($description_error) && $description_error==1){?><span class="help-inline">District Name is required.</span><?php }?>
		    </div>
		  </div>
          
          <div class="control-group <?php if (isset($latitude_error) && $latitude_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">GPS Latitude</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input  name="latitude" id="appendedInput" type="text" placeholder="GPS Latitude" value="<?php if(isset($_POST['latitude'])) { echo $_POST['latitude'];}else if(isset($latitude)) {echo $latitude;}?>">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		       <?php if(isset($latitude_error) && $latitude_error==1){?><span class="help-inline">Latitude Value is required.</span><?php }?>
		    </div>
		  </div>
          
          <div class="control-group <?php if (isset($longitude_error) && $longitude_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">GPS Longitude</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input  name="longitude" id="appendedInput" type="text" placeholder="GPS Longitude" value="<?php if(isset($_POST['longitude'])) { echo $_POST['longitude'];}else if(isset($longitude)) {echo $longitude;}?>">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		       <?php if(isset($longitude_error) && $longitude_error==1){?><span class="help-inline">GPS Longitude is required.</span><?php }?>
		    </div>
		  </div>
          
          <div class="form-actions">
  		  	<button type="submit" class="btn btn-primary">
  		  		<i class="icon-hdd icon-white"></i> 
  		  		 	<?php if ($edit_mode){?>Save
  		  		 	<?php }else{?>
  		  		 		Save District
  		  		 	<?php }?>
  		  	</button>
  			<a class="btn" href="<?php echo BASE_URL ;?>users">Cancel</a>
		</div>
		</form>
		
      </div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`employees` (`surname`, `firstname`, `district`, `telephone`, `postal_address`, `postal_code`, `town`, `email`, `added_by`, `date_added`, `date_updated`) VALUES ('Doe', 'John', 1, '0722123456', 'P.O. BOX 123456', '00200', 'Nairobi', 'name@example.com', 1, '2013-02-07', '2013-02-07'); -->
	