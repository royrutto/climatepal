<?php 
$edit_mode = false;
if (isset ($item_id) && !$item_id==''){
	$edit_mode =true;
}

if($edit_mode){
	$sale_details  = $Model->get_sale_by_id($item_id);
	
	if(count($sale_details)>0){
	
		$cpa = $sale_details['cpa'];
		$serial = $sale_details['firstname'];
		$date = $sale_details['gender'];
		$name = $sale_details['telephone'];
		$gender = $sale_details['postal_address'];
		$natID = $sale_details['postal_code'];
		$mobile = $sale_details['town'];
		$household = $sale_details['email'];
		$lat = $sale_details['salename'];
		$long = $sale_details['role_id'];
		$supplier = $sale_details['role_id'];
		$old = $sale_details['role_id'];
	}
}

$current_userid = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session'])


?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab">
	    		<span><?php echo $page_title?></span></a>
	    	</li>		
  		</ul>
  		<?php if (isset($empty_input_exists) && $empty_input_exists){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
      	<form class="form-horizontal"  action="<?php echo BASE_URL ;?>index.php" method="post">
      	<?php if ($edit_mode){?>
      	 	<input name="form_name" type="hidden" value="edit_sale">
      	 	<input name="sale_id" type="hidden" value="<?php echo $item_id ;?>">
      	 <?php }else {?>
      	 	 <input name="form_name" type="hidden" value="add_sale">
      	 <?php }?>
      	 <legend>Distribution Details</legend>
      	 <div class="control-group">
      	 <span class="add-on">The fields marked <i class="icon-asterisk"></i> are mandatory.</span> 
      	 </div>
		   <div class="control-group <?php if (isset($cpa_error) && $cpa_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">CPA Number</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input  name="cpa" id="appendedInput" type="text" placeholder="CPA Number" value="<?php if(isset($_POST['cpa'])) {echo $_POST['cpa'];}else if(isset($cpa)) {echo $cpa;}?>">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		       <?php if(isset($cpa_error) && $cpa_error==1){?><span class="help-inline">CPA Number is required.</span><?php }?>
		    </div>
		  </div>
		  <div class="control-group <?php if (isset($serial_error) && $serial_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Hifadhi Serial No.</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="serial" id="appendedInput" type="text" placeholder="Hifadhi Serial No." value="<?php if(isset($_POST['serial'])) {echo $_POST['serial'];}else if(isset($serial)) {echo $serial;}?>" class="serial">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($serial_error) && $serial_error==1){?><span class="help-inline">Hifadhi Serial Number is required.</span><?php }?>
		    </div>
		  </div>
		   <div class="control-group <?php if (isset($date_error) && $date_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Date</label>
		    <div class="controls">
		     <div class="input-append date" id="dp3" data-date="<?php echo date(DATE_DEFAULT);?>" data-date-format="yyyy-mm-dd">
				<input class="span" name="dates" size="16" type="text" value="<?php echo date(DATE_DEFAULT); if(isset($_POST['dates'])) {echo $_POST['dates'];}else if(isset($date)) {echo $date;}?>" readonly="">
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div>
		       
		        <?php if (isset($date_error) && $date_error==1){?><span class="help-inline">Date is required.</span><?php }?>
		    </div>
		  </div>
		  
		  <div class="control-group <?php if (isset($name_error) && $name_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">First Name</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="name" id="appendedInput" type="text" placeholder="User's Firstname" value="<?php if(isset($_POST['name'])) { echo $_POST['name']; }else if(isset($name)) {echo $name;}?>">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($name_error) && $name_error==1){?><span class="help-inline">First Name is required.</span><?php }?>
		    </div>
		  </div>
		  
		  <div class="control-group <?php if (isset($household_error) && $household_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Family Name</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="household" id="appendedInput" type="text" placeholder="User's Family Name" value="<?php if(isset($_POST['household'])) {echo $_POST['household'];}else if(isset($household)) {echo $household;}?>">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($household_error) && $household_error==1){?><span class="help-inline">Family Name is required.</span><?php }?>
		    </div>
		  </div>
		  
		  <div class="control-group <?php if (isset($gender_error) && $gender_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Gender</label>
		    <div class="controls">
		       <div class="input-append">
		       <select name ="gender" class="" >
		       <?php if(isset($_POST['gender'])||isset($gender)) {
		       	if (isset($_POST['gender'])){
					$gender = $_POST['gender'];
				}else $gender = $gender;
		       }
				?>
                <?php if($edit_mode){ ?>
					<option value="<?php echo $sexID; ?>" selected><?php echo $gender; ?></option>
				 <?php $sex  = $Model->get_all_gender();
					foreach($sex as $sexes){
					?>
		     		<option value="<?php echo $sexes['id']; ?>"> <?php echo $sexes['name'];?></option>
                    <?php }}else{
				 ?>
		       		<option value="" selected>------------Select Gender-----------</option>
                    <?php $sex2  = $Model->get_all_gender();
					foreach($sex2 as $sexes2){
					?>
		     		<option value="<?php echo $sexes2['id']; ?>"> <?php echo $sexes2['name'];?></option>
                    <?php } } ?>
				 	
  				</select>
		 		<span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($gender_error) && $gender_error==1){?><span class="help-inline">Please Select the appropriate Gender.</span><?php }?>
		    </div>
		  </div>
          
		  <div class="control-group <?php if (isset($user_error) && $user_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">User ID</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="user" id="appendedInput" type="text" placeholder="User ID" value="<?php if(isset($_POST['user'])) { echo $_POST['user']; }else if(isset($user)) {echo $user;}?>"  class="user_id"
		       	data-toggle="popover" data-placement="right" data-content="Enter digits only." title="" data-original-title="Only numbers required">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($user_error) && $user_error==1){?><span class="help-inline">User's National ID is required.</span><?php }?>
		    </div>
		  </div>
		  <div class="control-group <?php if (isset($mobile_error) && $mobile_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Mobile Number</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="mobile" id="appendedInput" type="text" placeholder="Mobile Number" value="<?php if(isset($_POST['mobile'])) {echo $_POST['mobile'];}else if(isset($mobile)) {echo $mpobile;}?>" class="mobile_number"
		       	data-toggle="popover" data-placement="right" data-content="Enter digits only." title="" data-original-title="Only numbers required">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($mobile_error) && $mobile_error==1){?><span class="help-inline">Mobile Phone Number is required.</span><?php }?>
		    </div>
		  </div>  
		   <div class="control-group <?php if (isset($location_error) && $location_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Location</label>
		    <div class="controls">
		       <div class="input-append">
		       <select name ="location" class="" >
		       <?php if(isset($_POST['location'])||isset($location)) {
		       	if (isset($_POST['location'])){
					$location = $_POST['location'];
				}else $location = $location;
		       }
				?>
                <?php if($edit_mode){ ?>
					<option value="<?php echo $locID; ?>" selected><?php echo $location; ?></option>
				 <?php $locs  = $Model->get_all_allowed_sublocations($current_userid);
					foreach($locs as $loc){
					?>
		     		<option value="<?php echo $loc['id']; ?>"> <?php echo $loc['name'];?></option>
                    <?php }}else{
				 ?>
		       		<option value="" selected>-----Select Location----</option>
                    <?php $locs2  = $Model->get_all_allowed_sublocations($current_userid);
					foreach($locs2 as $loc2){
					?>
		     		<option value="<?php echo $loc2['id']; ?>"> <?php echo $loc2['name'];?></option>
                    <?php } } ?>
				 	
  				</select>
		 		<span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($old_error) && $old_error==1){?><span class="help-inline">Please Select the appropriate Old Stove.</span><?php }?>
		    </div>
		  </div>
        
          <div class="control-group <?php if (isset($latitude_error) && $latitude_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">GPS Latitude</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="latitude" class="latitude" id="appendedInput" type="text" placeholder="GPS Latitude" value="<?php if(isset($_POST['latitude'])){ echo $_POST['latitude'];}else if(isset($latitude)) {echo $latitude;}?>"readonly>
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($latitude_error) && $latitude_error==1){?><span class="help-inline">GPS Latitude required.</span><?php }?>
		    </div>
		  </div>
          
          <div class="control-group <?php if (isset($longitude_error) && $longitude_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">GPS Longitude</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="longitude" class="longitude" id="appendedInput" type="text" placeholder="GPS Longitude" value="<?php if(isset($_POST['longitude'])) {echo $_POST['longitude'];}else if(isset($longitude)) {echo $longitude;}?>"readonly>
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($longitude_error) && $longitude_error==1){?><span class="help-inline">GPS Longitude is required.</span><?php }?>
		    </div>
		  </div>
          <!--
		  <div class="control-group <?php //if (isset($supplier_error) && $supplier_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Stove Supplier</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="supplier" id="appendedInput" type="text" placeholder="Stove Supplier" value="<?php //if(isset($_POST['supplier'])){ echo $_POST['supplier'];}else if(isset($supplier)) {echo $supplier;}?>">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php //if (isset($supplier_error) && $supplier_error==1){?><span class="help-inline">Stove Supplier is required.</span><?php //}?>
		    </div>
		  </div>-->
          
          <div class="control-group <?php if (isset($old_error) && $old_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Old Stove</label>
		    <div class="controls">
		       <div class="input-append">
		       <select name ="old" class="" >
		       <?php if(isset($_POST['old'])||isset($old)) {
		       	if (isset($_POST['old'])){
					$old = $_POST['old'];
				}else $old = $old;
		       }
				?>
                <?php if($edit_mode){ ?>
					<option value="<?php echo $oldID; ?>" selected><?php echo $old; ?></option>
				 <?php $old1  = $Model->get_all_stovetypes();
					foreach($old1 as $olds){
					?>
		     		<option value="<?php echo $olds['id']; ?>"> <?php echo $olds['name'];?></option>
                    <?php }}else{
				 ?>
		       		<option value="" selected>--------Select Old Stove-------</option>
                    <?php $old2  = $Model->get_all_stovetypes();
					foreach($old2 as $olds2){
					?>
		     		<option value="<?php echo $olds2['id']; ?>"> <?php echo $olds2['name'];?></option>
                    <?php } } ?>
				 	
  				</select>
		 		<span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($old_error) && $old_error==1){?><span class="help-inline">Please Select the appropriate Old Stove.</span><?php }?>
		    </div>
		  </div>
		 
		  <div class="form-actions">
  		  	<button type="submit" class="btn btn-primary" data-loading-text="Saving...">
  		  		<i class="icon-hdd icon-white"></i> 
  		  		 	<?php if ($edit_mode){?>Save
  		  		 	<?php }else{?>
  		  		 		Save Distribution
  		  		 	<?php }?>
  		  	</button>
  			<a class="btn" href="<?php echo BASE_URL ;?>distributions">Cancel</a>
		</div>
		</form>
		
      </div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`employees` (`cpa`, `firstname`, `gender`, `supplier`, `postal_address`, `postal_code`, `town`, `email`, `added_by`, `date_added`, `date_updated`) VALUES ('Doe', 'John', 1, '0722123456', 'P.O. BOX 123456', '00200', 'Nairobi', 'name@example.com', 1, '2013-02-07', '2013-02-07'); -->
	