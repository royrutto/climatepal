<?php 
require_once DIR_EXTENSIONS.'/phpexcel/Classes/PHPExcel/IOFactory.php';

$inputFileName= $file_path;
$sheetname='Data Sheet #1';

/**  Create a new Reader of the type defined in $inputFileType  **/
if ($inputFileType=="application/vnd.ms-excel")
	$objReader = new PHPExcel_Reader_Excel5();
if ($inputFileType=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	$objReader = new PHPExcel_Reader_Excel2007();

/**  Define a Read Filter class implementing PHPExcel_Reader_IReadFilter  */
class MyReadFilter implements PHPExcel_Reader_IReadFilter{
	public function  readCell($column, $row, $worksheetName=''){
		if($row>=1 && $row<=2500){
			if(in_array($column, range('B', 'I'))){
				return true;
			}
		}
		return false;
	}
}
/**  Create an Instance of our Read Filter  **/
$filterSubset= new MyReadFilter();
/**  Tell the Reader that we want to use the Read Filter  **/
$objReader->setReadFilter($filterSubset);
//$objReader->setReadDataOnly(true);
/**  Load only the rows and columns that match our filter to PHPExcel  **/
$objPHPExcel = $objReader->load($inputFileName);

$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
$added_by = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session']);
$date_added = date(DATE_FORMAT_DEFAULT);
$uploaded = $Sales->upload_excel_data($sheetData, $added_by, $date_added)
//var_dump($sheetData);

?>