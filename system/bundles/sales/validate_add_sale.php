<?php
if(session_id() == '') {
	session_start();
}
if(!isset($_SESSION['session_id'])){
	include(DIR_WEB.'/index.php');
	exit();
}
$edit_mode=false;
if(isset($form_name) && $form_name =='edit_sale'){
	$edit_mode=true;
	$sale_id=$_POST['sale_id'];
}
//required fields
$cpa_error=0;
$serial_error=0;
$date_error=0;
$name_error=0;
$gender_error=0;
$id_error=0;
$mobile_error=0;
$location_error=0;
$household_error=0;
$latitude_error=0;
$longitude_error=0;
$old_error=0;
$empty_input_exists=false;
$sale_exists=false;
if(!isset($device_type) ||$device_type!='mobile'){
	$cpa = $_POST['cpa'];
	$serial = $_POST['serial'];
	$date = $_POST['dates'];
	$name = $_POST['name'];
	$gender = $_POST['gender'];
	$id = $_POST['user'];
	$mobile = $_POST['mobile'];
	$location = $_POST['location'];
	$household = $_POST['household'];
	$latitude = $_POST['latitude'];
	$longitude = $_POST['longitude'];
	$old = $_POST['old'];
	$added_by = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session']);
}


//echo $date;

$flag = 1;
//$added_by = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session']);;

$date_added = date(DATE_FORMAT_DEFAULT);
$date_updated = date(DATE_FORMAT_DEFAULT);

if(isset($device_type) && $device_type=='mobile'){

	if($edit_mode){
		$saved = $Sales->update_sale($sale_id,$cpa, $serial, $date, $name, $gender, $id, $mobile, $location, $household, $latitude, $longitude, $old, $added_by, $date_updated);
	}else{
	
		$saved = $Sales->save_sale($cpa, $serial, $date, $firstname, $middlename, $householdname, $gender, $id, $mobile, $location, $latitude, $longitude, $old, $added_by, $date_added);
		
	}
	if (isset($saved) && !$saved){
		$data_saved=false;
	}else{
		$data_saved=true;
	}

} else {

if (validateInput($edit_mode)){
	$page_id='11'; $page_title= 'Add Distribution'; $path = '/sales/add_sale.php';
	$page = DIR_BUNDLES.$path;
	include(DIR_TEMPLATE.'/template.php');
}

else{
	
	if($edit_mode){
		$saved = $Sales->update_sale($sale_id,$cpa, $serial, $date, $name, $gender, $id, $mobile, $location, $household, $latitude, $longitude, $old, $added_by, $date_updated);
	}else{
		$saved = $Sales->save_sale($cpa, $serial, $date, $firstname, $middlename, $householdname, $gender, $id, $mobile, $location, $latitude, $longitude, $old, $added_by, $date_added);
	}
	if (isset($saved) &&! $saved){
		$sale_exists=true;
		$page_id='11'; $page_title= 'Add Distribution'; $path = '/sales/add_sale.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}else{
	
		$page_id='11'; $page_title= 'Distributions'; $path = '/sales/sales.php';
		$page = DIR_BUNDLES.$path;
		$distribution_saved=true;
		$alert_message="Distribution Saved";
		include(DIR_TEMPLATE.'/template.php');
	}
}
}



/**
 * 
 * @return boolean
 */
//Check if the required fields are empty
function validateInput($edit_mode){
		$empty_input_exists=false;
		global $cpa_error, $serial_error, $date_error, $name_error, $gender_error, $id_error, $mobile_error, $location_error, $household_error, $latitude_error, $longitude_error, $old_error;
		if($_POST['cpa']==""){
			$empty_input_exists = true;
			$cpa_error=1;
		}
		if($_POST['serial']==""){
			$empty_input_exists = true;
			$serial_error=1;
		}
		if($_POST['dates']=="0"){
			$empty_input_exists = true;
			$date_error=1;
		}
		if($_POST['name']==""){
			$empty_input_exists = true;
			$name_error=1;
		}
			
		if($_POST['gender']==""){
			$empty_input_exists = true;
			$gender_error=1;
		}
		if($_POST['user']==""){
			$empty_input_exists = true;
			$user_error=1;
		}
		if($_POST['mobile']==""){
			$empty_input_exists = true;
			$mobile_error=1;
		}
		if($_POST['location']==""){
			$empty_input_exists = true;
			$location_error=1;
		}
		if($_POST['latitude']==""){
			$empty_input_exists = true;
			$latitude_error=1;
		}
		if($_POST['longitude']==""){
			$empty_input_exists = true;
			$longitude_error=1;
		}
		if($_POST['old']==""){
			$empty_input_exists = true;
			$old_error=1;
		}
		
			
	return $empty_input_exists;
	
}

function secure_password($plain_text_password){
	require_once DIR_SYS.'/PasswordHash.php';
	$t_hasher = new PasswordHash(8, FALSE);
	$hash = $t_hasher->HashPassword($plain_text_password);
	return $hash;
}

	





?>