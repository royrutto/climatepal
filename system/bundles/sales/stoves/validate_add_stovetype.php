<?php
if(session_id() == '') {
	session_start();
}
if(!isset($_SESSION['session_id'])){
	include(DIR_WEB.'/index.php');
	exit();
}
$edit_mode=false;
if($form_name =='edit_stove_type'){
	$edit_mode=true;
	$id=$_POST['id'];
}
//required fields
$description_error=0;
//posted values
$description = $_POST['description'];

$flag = 1;
$added_by = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session']);;
$date_added =  date(DATE_FORMAT_DEFAULT);
$date_updated =  date(DATE_FORMAT_DEFAULT);

if (validateInput($edit_mode)){
	$page_id='7'; $page_title= 'Add Stove Type'; $path = '/sales/stoves/add_stove_types.php';
	$page = DIR_BUNDLES.$path;
	include(DIR_TEMPLATE.'/template.php');
}else{
	if($edit_mode){
		$saved = $StoveType->update_stove_type($id,$description, $added_by, $date_updated);
	}
	else{
	$saved = $StoveType->save_stove_type($description, $added_by, $date_added);
	}
	if (isset($saved) &&! $saved){
		$gender_exists=true;
		$page_id='7'; $page_title= 'Add Stove Types'; $path = '/sales/stoves/add_stove_types.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}else{
	
		$page_id='7'; $page_title= 'Stove Types'; $path = '/sales/stoves/stove_types.php';
		$page = DIR_BUNDLES.$path;
		include(DIR_TEMPLATE.'/template.php');
	}
}



/**
 * 
 * @return boolean
 */
//Check if the required fields are empty
function validateInput($edit_mode){
		global $description_error;		
		if($_POST['description']==""){
			$empty_input_exists = true;
			$description_error=1;
		}
		
	return $empty_input_exists;
	
}

?>