<?php 
	$all_stoves = $Model->get_all_stoves();
?>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/sales/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">
      	<div id="actions">
 
            <div class="btn-group">
 				<a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#">
 					<i class="icon-filter"></i> Filter
    				<span class="caret"></span>
  				</a>
  				<ul class="dropdown-menu">
    			<!-- dropdown menu links -->
    				<li><a href="#">By Serial</a></li>
					<li><a href="#">By CPA Number</a></li>
                    <li><a href="#">By Date Delivered</a></li>
				</ul>
			</div>
            
			<a href="<?php echo BASE_URL ;?>stoves" class="btn btn-inverse"><i class="icon-refresh icon-white"></i> Refresh</a>
            <a id = 'delete' href="#myModal" class="btn btn-danger hidden" data-toggle="modal"><i class="icon-trash icon-white"></i> Delete Selection</a>
            </div>
           </div>
	
      					<table  cellpadding="0" cellspacing="0" border="0" class="display table table-bordered table-hover" id="example" width="100%">	<thead>
		<tr>
			<th class="chk"><input type="checkbox" name="vehicle" value="Bike"></th>					   
					      		<th>CPA Number</th>
                                <th>Stove Serial</th>
                                <th>Date Delivered</th>
                                <th>Delivered By</th>
                                <th>Actions</th>
		</tr>
	</thead>
	<tbody>
							<?php foreach ($all_stoves as $stove){
								?>
							   <tr>
  								<td class="chk"><input type="checkbox" name="stove_id" value=" <?php echo $stove['id']; ?>"></td>
  								<td><?php echo $stove['cpa']; ?></td>
                                <td><?php echo $stove['serial']; ?></td>
  								<td><?php echo $stove['dist']; ?></td>
                                <td><?php echo $stove['added']; ?></td>
                                <td>
  								  <div class="bs-docs-example tooltip-demo">
                      				<a href="#" title="View"><span class="badge badge-success"><i class="icon-search icon-white"></i></span></a> 
                      				<a href="<?php echo BASE_URL ;?>stoves/edit/<?php echo $stove['stove_id']; ?>" title="Edit"><span class="badge badge-info"><i class="icon-pencil icon-white"></i></span></a> 
                      				<a href="#myModal" title="Delete" data-toggle="modal"><span class="badge badge-important"><i class="icon-trash icon-white"></i></span></a>
                    			  </div>
								  </td>
							   </tr>
							   <?php } ?>
	
	</tbody>
	<tfoot>
		<tr>
			<th class="chk"><input type="checkbox" name="vehicle" value="Bike"></th>					   
					      		<th>CPA Number</th>
                                <th>Stove Serial</th>
                                <th>Date Delivered</th>
                                <th>Delivered By</th>
                                <th>Actions</th>
		</tr>
	</tfoot>
						    </table>
            	</div> <!-- End Tab 1-->

    			 </div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`stoves` (`stove_name`, `decription`, `date_added`, `date_updated`) VALUES ('Administrator', 'Controls the whole system', '2013-02-07', '');
       -->
	