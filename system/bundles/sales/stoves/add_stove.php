<?php 
	$all_users = $Users->get_all_users();
?>
      <div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab1">
	    		<span><?php echo $page_title?></span></a>
	    	</li>
	    			
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">		
<div id="add_gender">
		<div class="section-title">ADD STOVE &nbsp;&nbsp;&nbsp;<a href="stoves.php">Click here to Refresh</a> </div>
  <div>
		<table>
			<tr>
              <td colspan="6" width="750"><em>The fields indicated asterisk (<span class="mandatory">*</span>) are mandatory.</em></td>
            </tr>
			
		</table>
		
	<!--		****************************************************************************** -->
	<div class="xtop">
	<?php  //validation errors
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		echo '<table>
				  <tr>
					<td style="width:auto" ><div class="error">';
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo '<strong>'.$msg . '</strong>'; 
		}
		echo '</div></td>
				  </tr>
				</table>';
		unset($_SESSION['ERRMSG_ARR']);
	}
?>
		<!--display the save message -->
				<?php if ($success !="")
				{
				?> 
				<table>

				  <tr>
					<td style="width:auto" >
					<div class="success">
					<?php echo  '<strong>'.' <font color="#666600">'.$success.'</strong>'.' </font>';?>
					</div>
					</td>
				  </tr>
				</table>
				<?php } ?>
				<?php if ($st !="")
						{
						?> 
						<table   >
				  <tr>
					<td style="width:auto" >
					<div class="error">
					<?php echo  '<strong>'.' <font color="#666600">'.$st.'</strong>'.' </font>';?></div>
					</td>
				  </tr>
				</table>
				<?php } ?>
		<!-- end display the save message -->
		</div>
	
	<!--		****************************************************************************** -->
		<form id="customForm"   method="get" action=""  >
        <table border="0" class="table table-bordered table-hover">
          <tr>
            <td colspan="5" class="subsection-title">Stove Information </td>
          </tr>
          <tr>
            <td><span class="mandatory">*</span> Stove Type</td>
            <td colspan="3"><div>
              <select name="type"><option value=""> --- Please Select --- </option>
          <?php $drive_query = mysql_query("Select * from stove_types;") or die("Error");
		while($drive_rows=mysql_fetch_assoc($drive_query)){
		 ?>
            <option value="<?php echo $drive_rows['id']; ?>"><?php echo $drive_rows['description']; ?></option>
          <?php } ?> 
          </select>
              <span id='nameInfo'></span></div></td>
            
          </tr>
          <tr>
            <td><span class="mandatory">*</span> Serial Number</td>
            <td colspan="3"><div>
              <input type="text" name="serial" id="serial" size="44" class="text validate[required] text-input" />
              <span id='coordinatesInfo'></span></div></td>
            
          </tr>
          <tr>
            <td><span class="mandatory">*</span> Description</td>
            <td colspan="3"><div>
              <textarea name="desc" id="desc" cols="10" rows="5" class="text validate[custom[onlyLetterSp]] text-input"></textarea>
              <span id='nameInfo'></span></div></td>
            
          </tr>
          <tr >
            <td></td>
            <td colspan="3"><input name="save" type="submit" class="button" value="Save Stove" />
                <input name="add" type="submit" class="button" value="Save & Add Stove" />
                <input name="reset" type="reset" class="button" value="Reset" /></td>
          </tr>
        </table>
	</form>
	<!--		****************************************************************************** -->
  </div>
</div>
		
 </div> <!-- End Tab 1-->

    			 <div class="tab-pane" id="tab2">
      				<p>Howdy, I'm in Section 2.</p>
      				
          				<?php echo date();?>

    			 </div> <!-- End Tab 2-->

  				</div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->