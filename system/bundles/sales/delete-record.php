<?php
if(session_id() == '') {
	session_start();
}
if(!isset($_SESSION['session_id'])){
	include(DIR_WEB.'/index.php');
	exit();
}
$delete_ids = $_POST['delete_ids'];
$deleted_by = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session']);
$date_updated = date(DATE_FORMAT_DEFAULT);
$no_of_records = count(explode($delete_ids, ','));

if ($Sales->delete_records($delete_ids, $deleted_by, $date_updated)){
	
  	$page_id='11'; $page_title= 'Distributions'; $path = '/sales/sales.php';
    $page = DIR_BUNDLES.$path;
	$distribution_saved=true;
	$alert_message="<b>".$no_of_records."</b> records have been deleted.";
	include(DIR_TEMPLATE.'/template.php');
      
} else
{
	$page_id='11'; $page_title= 'Distributions'; $path = '/sales/sales.php';
    $page = DIR_BUNDLES.$path;
    $distribution_saved=true;
	$alert_message="<b>".$no_of_records."</b> records have been deleted.";
    include(DIR_TEMPLATE.'/template.php');
}

?>