<?php
if(session_id() == '') {
	session_start();
}
if(!isset($_SESSION['session_id'])){
	include(DIR_WEB.'/index.php');
	exit();
}
$allowedExts = array("xls", "xlsx");
$extension = end(explode(".", $_FILES["file"]["name"]));
if ((($_FILES["file"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
&& ($_FILES["file"]["size"] < 5000000)
&& in_array($extension, $allowedExts))
  {
  if ($_FILES["file"]["error"] > 0)
    {
    echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
    }
  else
    {
   /* echo "Upload: " . $_FILES["file"]["name"] . "<br>";
    echo "Type: " . $_FILES["file"]["type"] . "<br>";
    echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
    echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";*/

    if (file_exists("uploads/" . $_FILES["file"]["name"]))
    {
      $page_id='11'; $page_title= 'Distributions'; $path = '/sales/upload-excel.php';
      $page = DIR_BUNDLES.$path;
      $distribution_saved=true;
      $alert_message="Excel file: <b>". $_FILES["file"]["name"]."</b> has already been uploaded.";
      include(DIR_TEMPLATE.'/template.php');
    }
    else
      {
      $file_path = "uploads/" . $_FILES["file"]["name"];
      move_uploaded_file($_FILES["file"]["tmp_name"], $file_path);
      $inputFileType = $_FILES["file"]["type"];
      include('read-excel-file.php');
   
      $page_id='11'; $page_title= 'Distributions'; $path = '/sales/sales.php';
      $page = DIR_BUNDLES.$path;
      $distribution_saved=true;
	  $alert_message="<b>". $_FILES["file"]["name"]."</b> has been uploaded. <br><b>".$uploaded."</b> records have been uploaded.";
      include(DIR_TEMPLATE.'/template.php');
      }
    }
  }
else
  {
  	$page_id='11'; $page_title= 'Distributions'; $path = '/sales/upload-excel.php';
  	$page = DIR_BUNDLES.$path;
  	$distribution_saved=true;
  	$alert_message="Invalid file type: <b>". $_FILES["file"]["type"]."</b> or file size: <b>".($_FILES["file"]["size"]/1048576)." MB</b>";
  	include(DIR_TEMPLATE.'/template.php');
  }
?>