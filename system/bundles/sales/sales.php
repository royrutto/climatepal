<?php 
	$all_sales = $Model->get_all_sales();
?>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/sales/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">
	    	<div class="alert alert-success" id="distribution_saved">
			</div>		
    		<div id="actions">

            <div id="actionbutton">
      		<a href="<?php echo BASE_URL ;?>distributions/add" class="btn btn-primary"><i class="icon-plus icon-white"></i> Add Distribution</a>
  			<a href="<?php echo BASE_URL ;?>distributions/upload-excel" class="btn btn-success"><i class="icon-arrow-up icon-white"></i> Upload Excel Sheet</a>
            <div class="btn-group">
 				<a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#">
 					<i class="icon-filter"></i> Filter
    				<span class="caret"></span>
  				</a>
  				<ul class="dropdown-menu">
    			<!-- dropdown menu links -->
    				<li><a href="#">by Location</a></li>
					<li><a href="#">by Field Officer</a></li>
					<li><a href="#">by Name</a></li>
  				</ul>
			</div>
			<a href="<?php echo BASE_URL ;?>distributions" class="btn btn-inverse"><i class="icon-refresh icon-white"></i> Refresh</a>
            <a id='delete' href="#myModal" class="btn btn-danger hidden" data-toggle="modal"><i class="icon-trash icon-white"></i> Delete Selection</a>
            <input type="hidden" name="deletefrom" id="deletefrom" value="distributions"/>
            </div>
           </div>
	
      					    
						    <table class="display table table-bordered table-hover" id="example">
	<thead>
		<tr>
								<th class="chk"><input type="checkbox" name="checkall" onclick="checkAll(this)"></th>					   
					      							   
					      		<th>Item No.</th>
					      		<th>Stove-User Name</th>
					      		<th>Sub-location</th>
					      		<th>Date Delivered</th>
					      		<th>Field Officer</th>
					      		<th>Stove Serial No.</th>
					      		<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$count=0;
		foreach ($all_sales as $sale){
		$count++;
		?>
			<tr>
  			<td><input type="checkbox" name="item_id" value=" <?php echo $sale['id']; ?>"></td>
  			<td><?php echo $count; ?></td>
  			<td><?php echo $sale['name']; ?></td>
  			<td><?php echo $sale['sublocation']; ?></td>
  			<td><?php echo $sale['dist_date']; ?></td>
  			<td><?php echo $sale['fo']; ?></td>
  			<td><?php echo $sale['stove_serial']; ?></td>
			<td class="span2">
  			<div class="bs-docs-example tooltip-demo">
  				<a href="#" title="View"><span class="badge badge-success"><i class="icon-search icon-white"></i></span></a> 
             	<a href="<?php echo BASE_URL ;?>dist/update/<?php echo $sale['id']; ?>" title="Update"><span class="badge badge-info"><i class="icon-pencil icon-white"></i></span></a> 
        
            </div>
			</td>
			</tr>
							   <?php } ?>
	
	</tbody>
	<tfoot>
		<tr>
								<th class="chk"><input type="checkbox" name="checkall" onclick="checkAll(this)"></th>					   
					      		<th>Item No.</th>
					      		<th>Stove-User Name</th>
					      		<th>Sub-location</th>
					      		<th>Date Delivered</th>
					      		<th>Field Officer</th>
					      		<th>Stove Serial No.</th>
					      		<th>Actions</th>
		</tr>
	</tfoot>
</table>
    			</div> <!-- End Tab 1-->
    			

    			 <div class="tab-pane" id="tab2">
      				<p>Howdy, I'm in Section 2.</p>
      				
          				<?php echo date();?>

    			 </div> <!-- End Tab 2-->

  				</div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->
	