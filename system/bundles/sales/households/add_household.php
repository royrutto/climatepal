<?php 
	$all_users = $Users->get_all_users();
?>
      <div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab1">
	    		<span><?php echo $page_title?></span></a>
	    	</li>
	    			
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">
      	<div id="add_households">
		<div class="section-title">ADD HOUSEHOLD &nbsp;&nbsp;&nbsp;<a href="households.php">Click here to Refresh</a> </div>
  <div>
		<table>
			<tr>
              <td colspan="6" width="750"><em>The fields indicated asterisk (<span class="mandatory">*</span>) are mandatory.</em></td>
            </tr>
			
		</table>
		
	<!--		****************************************************************************** -->
	<div class="xtop">
	<?php  //validation errors
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		echo '<table>
				  <tr>
					<td style="width:auto" ><div class="error">';
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo '<strong>'.$msg . '</strong>'; 
		}
		echo '</div></td>
				  </tr>
				</table>';
		unset($_SESSION['ERRMSG_ARR']);
	}
?>
		<!--display the save message -->
				<?php if ($success !="")
				{
				?> 
				<table>

				  <tr>
					<td style="width:auto" >
					<div class="success">
					<?php echo  '<strong>'.' <font color="#666600">'.$success.'</strong>'.' </font>';?>
					</div>
					</td>
				  </tr>
				</table>
				<?php } ?>
				<?php if ($st !="")
						{
						?> 
						<table   >
				  <tr>
					<td style="width:auto" >
					<div class="error">
					<?php echo  '<strong>'.' <font color="#666600">'.$st.'</strong>'.' </font>';?></div>
					</td>
				  </tr>
				</table>
				<?php } ?>
		<!-- end display the save message -->
		</div>
	
	<!--		****************************************************************************** -->
		<form id="customForm"   method="get" action=""  >
        <table border="0" class="table table-bordered table-hover">
          <tr>
            <td colspan="5" class="subsection-title">Household Information </td>
          </tr>
          <tr>
            <td><span class="mandatory">*</span> Household Name</td>
            <td colspan="3"><div>
              <input type="text" name="name" id="name" size="44" class="validate[required,custom[onlyLetterSp]] text-input" />
              <span id='nameInfo'></span></div></td>
            
          </tr>
          <tr>
            <td><span class="mandatory">*</span> Household Gender</td>
            <td colspan="3"><div>
              <select name="gender" id="gender" class="validate[required]"><option value=""> --- Please Select --- </option>
          <?php $gender_query = mysql_query("Select * from gender;") or die("Error");
		while($gender_rows=mysql_fetch_assoc($gender_query)){
		 ?>
            <option value="<?php echo $gender_rows['id']; ?>"><?php echo $gender_rows['description']; ?></option>
          <?php } ?> 
          </select>
              <span id='sexInfo'></span></div></td>
            
          </tr>
          <tr>
            <td><span class="mandatory">*</span> ID Number</td>
            <td colspan="3"><div>
              <input type="text" name="idno" id="idno" size="44" class="validate[required,custom[onlyNumberSp]] text-input" />
              <span id='fonInfo'></span></div></td>
            
          </tr>
          <tr>
            <td><span class="mandatory">*</span> Phone Number</td>
            <td colspan="3"><div>
              <input type="text" name="phone" id="phone" size="44" class="validate[required,custom[phone]] text-input" />
              <span id='fonInfo'></span></div></td>
            
          </tr>
          <tr>
            <td><span class="mandatory">*</span> Postal Address</td>
            <td colspan="3"><div>
              <input type="text" name="postal" id="postal" size="44" class="text validate[required] text-input" />
              <span id='postalInfo'></span></div></td>
            
          </tr>
          <tr>
            <td><span class="mandatory">*</span> Postal Code</td>
            <td colspan="3"><div>
              <input type="text" name="code" id="code" size="44" class="validate[required,custom[onlyNumberSp]] text-input" />
              <span id='codeInfo'></span></div></td>
            
          </tr>
          <tr>
            <td><span class="mandatory">*</span> Town</td>
            <td colspan="3"><div>
              <input type="text" name="town" id="town" size="44" class="validate[required,custom[onlyLetterSp]] text-input" />
              <span id='townInfo'></span></div></td>
            
          </tr>
          <tr>
            <td><span class="mandatory">*</span> Location</td>
            <td colspan="3"><div>
              <select name="location" id="location" class="validate[required]"><option value=""> --- Please Select --- </option>
          <?php $location_query = mysql_query("Select * from locations;") or die("Error");
		while($location_rows=mysql_fetch_assoc($location_query)){
		 ?>
            <option value="<?php echo $location_rows['id']; ?>"><?php echo $location_rows['description']; ?></option>
          <?php } ?> 
          </select>
              <span id='locationInfo'></span></div></td>
            
          </tr>
          <tr>
            <td><span class="mandatory">*</span> Old Stove</td>
            <td colspan="3"><div>
              <select name="stove" id="stove" class="validate[required]"><option value=""> --- Please Select --- </option>
          <?php $stove_query = mysql_query("Select * from stove_types;") or die("Error");
		while($stove_rows=mysql_fetch_assoc($stove_query)){
		 ?>
            <option value="<?php echo $stove_rows['id']; ?>"><?php echo $stove_rows['description']; ?></option>
          <?php } ?> 
          </select>
              <span id='stoveInfo'></span></div></td>
            
          </tr>
          <tr >
            <td></td>
            <td colspan="3"><input name="save" type="submit" class="button" value="Save Household" />
                <input name="add" type="submit" class="button" value="Save & Add Household" />
                <input name="reset" type="reset" class="button" value="Reset" /></td>
          </tr>
        </table>
	</form>
	<!--		****************************************************************************** -->
  </div>
</div>
    	</div> <!-- End Tab 1-->
		
    	<div class="tab-pane" id="tab2">
      		<p>Howdy, I'm in Section 2.</p>
		      	<?php echo date();?>
		</div> <!-- End Tab 2-->

  				</div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
      </div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`roles` (`role_name`, `decription`, `date_added`, `date_updated`) VALUES ('Administrator', 'Controls the whole system', '2013-02-07', '');
       -->
	