<?php 
	$all_households = $Model->get_all_households();
?>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/sales/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">
      	<div id="actions">
 
            <!--div class="btn-group">
 				<a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#">
 					<i class="icon-filter"></i> Filter
    				<span class="caret"></span>
  				</a>
  				<ul class="dropdown-menu">
    				<li><a href="#">By Location</a></li>
					<li><a href="#">By Name</a></li>
                    <li><a href="#">By Old Stove</a></li>
				</ul>
			</div-->
            
			<a href="<?php echo BASE_URL ;?>households" class="btn btn-inverse"><i class="icon-refresh icon-white"></i> Refresh</a>
            <a id = 'delete' href="#myModal" class="btn btn-danger hidden" data-toggle="modal"><i class="icon-trash icon-white"></i> Delete Selection</a>
            </div>
           </div>
				<table  cellpadding="0" cellspacing="0" border="0" class="display table table-bordered table-hover" id="example" width="100%">	<thead>
							<tr>
								<th class="chk"><input type="checkbox" name="vehicle" value=""></th>						   
					      		<th>No.</th>
					      		<th>Name</th>
					      		<th>Phone</th>
                                <th>Gender</th>
                                <th>National ID</th>
                                
                                <!--th>Family Name</th-->
                                <th>Coordinates</th>
					      		<th>Actions</th>
		</tr>
	</thead>
	<tbody>
							<?php foreach ($all_households as $household){
								?>
							   <tr>
  								<td class="chk"><input type="checkbox" name="household_id" value=" <?php echo $household['id']; ?>"></td>
  								<td><?php echo $household['id']; ?></td>
  								<td><?php echo $household['name']; ?></td>
  								<td><?php echo $household['phone']; ?></td>
                                <td><?php echo $household['sex']; ?></td>
  								<td><?php echo $household['no']; ?></td>
                                <td><?php echo 'Lat: '.$household['latitude']. '<br> Long: '.$household['longitude']; ?></td>
                          
                                <td>
  								  <div class="bs-docs-example tooltip-demo">
                      				<a href="#" title="View"><span class="badge badge-success"><i class="icon-search icon-white"></i></span></a> 
                      				<a href="<?php echo BASE_URL ;?>households/edit/<?php echo $household['household_id']; ?>" title="Edit"><span class="badge badge-info"><i class="icon-pencil icon-white"></i></span></a> 
                    			  </div>
								  </td>
							   </tr>
							   <?php } ?>
	
	</tbody>
	<tfoot>
		<tr>
			<th class="chk"><input type="checkbox" name="vehicle" value="Bike"></th>					   
					      		<th class="chk"><input type="checkbox" name="vehicle" value=""></th>						   
					      		<th>No.</th>
					      		<th>Name</th>
					      		<th>Phone</th>
                                <th>Gender</th>
                                <th>National ID</th>
                                
                                <!--th>Family Name</th-->
                                <th>Coordinates</th>
					      		<th>Actions</th>
		</tr>
	</tfoot>
						    </table>
            	</div> <!-- End Tab 1-->

    			 </div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`households` (`household_name`, `decription`, `date_added`, `date_updated`) VALUES ('Administrator', 'Controls the whole system', '2013-02-07', '');
       -->
	