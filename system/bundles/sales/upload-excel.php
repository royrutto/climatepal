<?php 
$edit_mode = false;
if (isset ($item_id) && !$item_id==''){
	$edit_mode =true;
}

if($edit_mode){
	$sale_details  = $Model->get_sale_by_id($item_id);
	
	if(count($sale_details)>0){
	
		$cpa = $sale_details['cpa'];
		$serial = $sale_details['firstname'];
		$date = $sale_details['gender'];
		$name = $sale_details['telephone'];
		$gender = $sale_details['postal_address'];
		$natID = $sale_details['postal_code'];
		$mobile = $sale_details['town'];
		$household = $sale_details['email'];
		$lat = $sale_details['salename'];
		$long = $sale_details['role_id'];
		$supplier = $sale_details['role_id'];
		$old = $sale_details['role_id'];
	}
}

$current_userid = $Users->get_userid_by_sessionid($_COOKIE['climatepal_session'])


?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab">
	    		<span><?php echo $page_title?></span></a>
	    	</li>		
  		</ul>
  		<div class="alert alert-success" id="distribution_saved">
		</div>		
  		<?php if (isset($empty_input_exists) && $empty_input_exists){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
      	<form class="form-horizontal"  action="<?php echo BASE_URL ;?>index.php" method="post" enctype="multipart/form-data">
      	 	 <input name="form_name" type="hidden" value="upload-excel">
      	 <div class="control-group">
      	 <span class="add-on">Select Excel sheet to upload distribution data.</span> 
      	 </div>
		   <div class="control-group <?php if (isset($cpa_error) && $cpa_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputFile">Filename: </label>
		    <div class="controls">
		       <div class="input-append">
		       	<input type="file" name="file" id="file">
		       </div>
		       <?php if(isset($cpa_error) && $cpa_error==1){?><span class="help-inline">File is required.</span><?php }?>
		    </div>
		  </div>
		 
		  <div class="form-actions">
  		  	<button type="submit" class="btn btn-primary" data-loading-text="Saving...">
  		  		<i class="icon-hdd icon-white"></i> 
  		  		 		Import to Distributions
  		  	</button>
  			<a class="btn" href="<?php echo BASE_URL ;?>distributions">Cancel</a>
		</div>
		</form>
		
      </div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`employees` (`cpa`, `firstname`, `gender`, `supplier`, `postal_address`, `postal_code`, `town`, `email`, `added_by`, `date_added`, `date_updated`) VALUES ('Doe', 'John', 1, '0722123456', 'P.O. BOX 123456', '00200', 'Nairobi', 'name@example.com', 1, '2013-02-07', '2013-02-07'); -->
	