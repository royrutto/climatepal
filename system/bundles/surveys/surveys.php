<?php 
	$all_surveys = $Model->get_all_surveys();
?>

<div class="span9">

      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/surveys/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">
    	<div class="alert alert-success" id="response_saved">
		</div>
    		<!--div id="actions">

            <div id="actionbutton">
      		<a href="<?php echo BASE_URL ;?>surveys/add" class="btn btn-primary"><i class="icon-plus icon-white"></i> Add a Survey</a>
  
  
			<a href="<?php echo BASE_URL ;?>surveys" class="btn btn-inverse"><i class="icon-refresh icon-white"></i> Refresh</a>
            <a id='delete' href="#myModal" class="btn btn-danger hidden" data-toggle="modal"><i class="icon-trash icon-white"></i> Delete Selection</a>
            </div>
           </div-->	
  <div class="accordion" id="accordion2">
  <?php foreach ($all_surveys as $survey){?>
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="<?php echo '#'.$survey['survey_id'];?>">
      
     
        <?php echo $survey['description']; ?>
      </a>
       
    </div>
    <div id="<?php echo $survey['survey_id'];?>" class="accordion-body collapse">
      <div class="accordion-inner">
      	    <div class="row-fluid">
      	    <table  cellpadding="0" cellspacing="0" border="0" class="display table table-bordered table-hover span5" width="100%">
	<thead>
	
	</thead>
	<tbody>
		
							   <tr class="odd gradeB">
  								<td id="survey">No. of beneficiaries interviewed</td>
  								<td><span class="badge badge-info"><?php echo $survey['num_responses']?></span></td>
							   	</tr>
							   	 <!--tr class="even gradeB">
  								<td id="survey">No. of Locations visited</td>
  								<td><span class="badge badge-info">0</span></td>
							   	</tr-->
							    <tr class="even gradeB">
  								<td id="survey">No. of Field Officers</td>
  								<td><span class="badge badge-info"><?php echo $survey['num_users']?></span></td>
							  	</tr>
							   
							   
	</tbody>
	<tfoot>
		<tr>
			
		</tr>
	</tfoot>
</table>
</div>
		<div class="row-fluid">
        <div id="actions">
		
            <div id="actionbutton">
            <a href="<?php echo BASE_URL ;?>surveys/fill/<?php echo $survey['survey_id']; ?>" class="btn btn-info"><i class="icon-pencil icon-white"></i> Fill Questionnaire</a>
      		<a href="<?php echo BASE_URL ;?>surveys/viewdata/<?php echo $survey['survey_id']; ?>" class="btn btn-default"><i class="icon-list-alt"></i> View Collected Data</a>
      		<!--a href="<?php echo BASE_URL ;?>surveys/edit/<?php echo $survey['description']; ?>" class="btn btn-default"><i class="icon-edit"></i> Edit Questionnaire</a-->
  				<a href="<?php echo BASE_URL ;?>surveys/summary/<?php echo $survey['survey_id']; ?>" class="btn btn-success"><i class="icon-arrow-up icon-white"></i>View summary</a>
    
            </div>
           </div>
           </div>
      </div>
    </div>
  </div>
  
   <?php } ?>
  
</div>
</div> <!-- End Tab 1-->
    			 <div class="tab-pane" id="tab2">
      				<p>Howdy, I'm in Section 2.</p>
      				
          				<?php echo date();?>

    			 </div> <!-- End Tab 2-->

  				</div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->
	