<?php
$parts = explode('/', $item_id);
$questions = $Model->get_responses_by_response_id($parts['1']);

?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab">
	    		<span><?php echo $page_title?></span></a>
	    	</li>		
  		</ul>
  		<div id="actions">
				<div id="actionbutton">
      			<a href="<?php echo BASE_URL ;?>surveys/viewdata/<?php echo  $parts['0'];?>" class="btn btn-default"><i class="icon-chevron-left"></i>Back</a>
      			</div>
           </div>
  		<?php if (isset($empty_input_exists) && $empty_input_exists){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
<!---------------------------------------------BEGINNING  OF QUESTIONS------------------------------------------------------------ -->			
		<table  cellpadding="0" cellspacing="0" border="0" class="display table table-bordered table-hover" id="example" width="100%">
	<thead>
		<tr>					   
					      		<th>No.</th>
					      		<th>Question</th>
					      		<th>Response</th>
		</tr>
	</thead>
	<tbody>
      	<?php 
      	$group_id="";
      	$count=0;
      	foreach ($questions as $question){
		$count++;
			if($question['group_id']!=$group_id){
				$group_id = $question['group_id'];
		?><tr class="lead"> <td colspan="3"><?php echo $question['group_id'].'. '.$question['group_name']; ?></td></tr>
		
		
		<?php }?>
		<tr>
  								<td><?php echo $count; ?></td>
  								<td><?php echo $question['field_name'] ?></td>
  							
  								
								 <td>
  								 <?php echo $question['value']; ?>
								  </td> 
							   </tr>  	
      	
<!---------------------------------------------END OF QUESTIONS------------------------------------------------------------ -->		  
	<?php }?>
		</tbody>
	<tfoot>
		<tr>
								<th>Date</th>
					      		<th>Field Officer</th>
					      		
					      		<th>Actions</th>
					      		<!--th>Percentage complete</th>
					      		<th>Actions</th-->
		</tr>
	</tfoot>
</table>
		
      </div>