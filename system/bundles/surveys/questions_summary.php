<?php
$parts = explode('/', $item_id);
$questions = $Model->get_responses_summary($item_id);

?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    <?php
 if (isset($item_id)){
 ?>
<li class="<?php if($page_title =='Data Collection Summary')echo 'active';?>">
	<a href="<?php echo BASE_URL.'surveys/summary/'.$item_id?>" data-toggle="tab1">
	<span>Data Collection Summary</span></a>
</li>
<li class="<?php if($page_title =='Survey Summary')echo 'active';?>">
	<a href="<?php echo BASE_URL.'surveys/questions_summary/'.$item_id?>" data-toggle="tab1">
	<span>Baseline Survey Summary</span></a>
</li>
 <li class="<?php if($page_title =='Stove Users Summary')echo 'active';?>">
	<a href="<?php echo BASE_URL.'surveys/users_summary/'.$item_id?>" data-toggle="tab1">
	<span>Stove Users Interviewed</span></a>
</li>
<?php }?>	
  		</ul>
  		<div id="actions">
				<div id="actionbutton">
      			<a href="<?php echo BASE_URL ;?>surveys/survey/" class="btn btn-default"><i class="icon-chevron-left"></i>Back</a>
      			<a href="<?php echo BASE_URL ;?>surveys/download_question_summary/<?php echo $item_id; ?>" class="btn btn-success" target=""><i class="icon-arrow-down icon-white"></i>Download summary</a>
      			</div>
           </div>
  		<?php if (isset($empty_input_exists) && $empty_input_exists){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
<!---------------------------------------------BEGINNING  OF QUESTIONS------------------------------------------------------------ -->			
		<table  cellpadding="0" cellspacing="0" border="0" class="display table table-bordered table-hover" id="example" width="100%">
	<thead>
		<tr>					   
			<th colspan="3" style="text-align: center;">Baseline Questions Summary</th>
		</tr>
	</thead>
	<tbody>
      	<?php 
      	$group_id="";
      	$count=0;
      	foreach ($questions as $question){
		$count++;
			if($question['group_id']!=$group_id){
				$group_id = $question['group_id'];
		?><tr class="lead"> <td colspan="3"><?php echo $question['group_id'].'. '.$question['group_name']; ?></td></tr>
			<tr>
			<th style="text-align: center;">Question</th>
			<th style="text-align: center;">Answers</th>
			<th style="text-align: center;">Number</th>
			</tr>
		
		
		<?php }?>
		<?php switch($question['field_type']){
		 case 'radiogroup':?>
							<tr>				
  								<td><?php echo $question['field_name'] ;?></td>
  								<td></td> 
								<td></td>
							</tr>
							<?php 
							$values = $Model->get_question_summary($question['field_id'],$item_id, true);
  							foreach ($values as $value){ ?>
							 	<tr>				
  								<td></td>
  								<td><?php echo $value['value'];?></td> 
								<td><?php echo $value['number'];?></td>
							</tr>
							 <?php }?> 	
      	<?php break;?>
      	<?php case 'text':?>
					<?php if ($question['field_id']==1 || $question['field_id']==2){
							if($question['field_id']==1){?>
								<?php 
								$values = $Model->get_question_summary($question['field_id'],$item_id, true);
  								?>
							<tr>				
  								<td colspan="2">Number of Stove users interviewed</td>
  								
								<td><?php echo count($values)?></td>
							</tr>
							<tr>
							<td colspan="3"></td>
							</tr>
					<?php }}else{?>
						<tr>				
  								<td><?php echo $question['field_name'] ;?></td>
  								<td></td> 
								<td></td>
							</tr>
							<?php 
							$values = $Model->get_question_summary($question['field_id'],$item_id, true);
  							foreach ($values as $value){ ?>
							 	<tr>				
  								<td></td>
  								<td><?php echo $value['value'];?></td> 
								<td><?php echo $value['number'];?></td>
							</tr>
					<?php }
					}?> 	
      	<?php break;?>
      	<?php case 'choicegroup':?>
							<tr>				
  								<td><?php echo $question['field_name'] ;?></td>
  								<td></td> 
								<td></td>
							</tr>
							<?php 
							$values = $Model->get_question_summary($question['field_id'],$item_id, true);
  							foreach ($values as $value){ ?>
							 	<tr>				
  								<td></td>
  								<td><?php echo $value['value'];?></td> 
								<td><?php echo $value['number'];?></td>
							</tr>
							 <?php }?>
						  
  						
      	<?php break;?>
      	<?php default:?>
      						<tr>				
  								<td><?php echo $question['field_name'] ?></td>
  				
								 <td>
  								 <?php echo $question['value']; ?>
								  </td> 
								   <td> 
								
								  </td>
							   </tr> 
      	
      	<?php break;?>
      	<?php }?>
<!---------------------------------------------END OF QUESTIONS------------------------------------------------------------ -->		  
	<?php }?>
		</tbody>
	<tfoot>
		<tr>
			<th style="text-align: center;">Question</th>
			<th style="text-align: center;">Answers</th>
			<th style="text-align: center;">Number</th>
		</tr>
	</tfoot>
</table>
		
      </div>