<?php 

$all_collected = $Model->get_survey_repsonses_summary($item_id);

require_once DIR_EXTENSIONS.'/phpexcel/Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Admin")
->setLastModifiedBy("Admin")
->setTitle("Responses Summary")
->setSubject("Office 2007 XLSX  Document")
->setDescription("Responses Summary.")
->setKeywords("office 2007 openxml php")
->setCategory("Resposes file");
$objPHPExcel->setActiveSheetIndex(0);

$rowCount = 1;
$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'Field Officer');
$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,'No. of Responses');

	foreach ($all_collected as $user){
	$rowCount++;
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$user['employee']);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$user['filled']);

}

$objPHPExcel->getActiveSheet()->setTitle('Responses Summary');
$objPHPExcel->setActiveSheetIndex(0);
ob_end_clean();
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Basline Survey Work Summary.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>