<?php
$parts = explode('/', $item_id);
$questions = $Model->get_responses_summary($item_id);

?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    <?php
 if (isset($item_id)){
 ?>
<li class="<?php if($page_title =='Data Collection Summary')echo 'active';?>">
	<a href="<?php echo BASE_URL.'surveys/summary/'.$item_id?>" data-toggle="tab1">
	<span>Data Collection Summary</span></a>
</li>
<li class="<?php if($page_title =='Survey Summary')echo 'active';?>">
	<a href="<?php echo BASE_URL.'surveys/questions_summary/'.$item_id?>" data-toggle="tab1">
	<span>Baseline Survey Summary</span></a>
</li>
 <li class="<?php if($page_title =='Stove Users Summary')echo 'active';?>">
	<a href="<?php echo BASE_URL.'surveys/users_summary/'.$item_id?>" data-toggle="tab1">
	<span>Stove Users Interviewed</span></a>
</li>
<?php }?>	
  		</ul>
  		<div id="actions">
				<div id="actionbutton">
      			<a href="<?php echo BASE_URL ;?>surveys/survey/" class="btn btn-default"><i class="icon-chevron-left"></i>Back</a>
      			<a href="<?php echo BASE_URL ;?>surveys/download_stoveuser_summary/<?php echo $item_id; ?>" class="btn btn-success" target=""><i class="icon-arrow-down icon-white"></i>Download summary</a>
      			</div>
           </div>
  		<?php if (isset($empty_input_exists) && $empty_input_exists){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
<!---------------------------------------------BEGINNING  OF QUESTIONS------------------------------------------------------------ -->			
		<table  cellpadding="0" cellspacing="0" border="0" class="display table table-bordered table-hover" id="example" width="100%">
	<thead>
		<tr>					   
			<th colspan="3" style="text-align: center;">Stove Users Interviewed</th>
		</tr>
	</thead>
	<tbody>
	<tr>
			<th style="text-align: center;">No.</th>
			<th style="text-align: center;">User ID</th>
			<th style="text-align: center;">Name</th>
			</tr>
      	<?php 
      	$group_id="";
      	$count=0;
      	
      	foreach ($questions as $question){
		
		?>
			
		
		
	
		<?php switch($question['field_type']){
      	case 'text':?>
      		<?php if ($question['field_id']==1){?>
							<tr>				
  								<td></td> 
								<td></td>
							</tr>
							<?php 
							$values = $Model->get_question_summary($question['field_id'],$item_id, true);
  							foreach ($values as $value){ 
								$count++;?>
  							
							 	<tr>
							 	<td><?php echo $count; ?></td>				
  								<td><?php echo $value['value'];?></td>
  								<td><?php echo $Model->get_resonse_name($value['response_id']);?></td> 
							</tr>
							 <?php }?> 
			<?php }?>	
      	<?php break;?>
   
      	<?php }?>
<!---------------------------------------------END OF QUESTIONS------------------------------------------------------------ -->		  
	<?php }?>
		</tbody>
	<tfoot>
		<tr>
			<th style="text-align: center;">No.</th>
			<th style="text-align: center;">User ID</th>
			<th style="text-align: center;">Name</th>
		</tr>
	</tfoot>
</table>
		
      </div>