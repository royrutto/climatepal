<?php 
$edit_mode = false;
if (isset ($item_id) && !$item_id=='' && $action!='fill'){
	$edit_mode =true;
}

if($edit_mode){
	$user_details  = $Model->get_user_by_id($item_id);
	
	if(count($user_details)>0){
	
		$surname = $user_details['surname'];
		$firstname = $user_details['firstname'];
		$gender = $user_details['gender'];
		$telephone = $user_details['telephone'];
		$postal_address = $user_details['postal_address'];
		$postal_code = $user_details['postal_code'];
		$postal_town = $user_details['town'];
		$email = $user_details['email'];
		$username = $user_details['username'];
		$role_id = $user_details['role_id'];
	}
}



?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab">
	    		<span><?php echo $page_title?></span></a>
	    	</li>		
  		</ul>
  		<?php if (isset($empty_input_exists) && $empty_input_exists){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
      	<form class="form-horizontal"  action="<?php echo BASE_URL ;?>index.php" method="post">
      	<?php if ($edit_mode){?>
      	 	<input name="form_name" type="hidden" value="edit_questionnaire">
      	 	<input name="user_id" type="hidden" value="<?php echo $item_id ;?>">
      	 <?php }else {?>
      	 	 <input name="form_name" type="hidden" value="add_quesionnaire">
      	 <?php }?>
      	
      	 
      	<legend  class="lead">1. User Details</legend>
      	 
      	 <div class="control-group">
      	 <span class="add-on">The fields marked <i class="icon-asterisk"></i> are mandatory.</span> 
      	 </div>
		   <div class="control-group <?php if (isset($surname_error) && $surname_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">User ID</label>
		    <div class="controls">
		       <div class="input-append">
		      <input type="text"style="margin: 0 auto;" data-provide="typeahead" data-items="4" 
		      data-source="[&quot;1234&quot;,&quot;233434&quot;,&quot;454657676&quot;]"
		      value="<?php if(isset($_POST['firstname'])) {echo $_POST['firstname'];}else if(isset($firstname)) {echo $firstname;}?>">
		     
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		       <?php if(isset($surname_error) && $surname_error==1){?><span class="help-inline">Surname is required.</span><?php }?>
		    </div>
		  </div>
		  
		   <div class="control-group <?php if (isset($firstname_error) && $firstname_error==1){echo 'error';}?>">
		    <label class="control-label" for="Location">Location</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="location" id="disabledInput" type="text" placeholder="" class="location"
		       	value="<?php if(isset($_POST['firstname'])) {echo $_POST['firstname'];}else if(isset($firstname)) {echo $firstname;}?>"
		       	disabled>
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($firstname_error) && $firstname_error==1){?><span class="help-inline">Firstname is required.</span><?php }?>
		    </div>
		  </div>
		  <div class="control-group <?php if (isset($firstname_error) && $firstname_error==1){echo 'error';}?>">
		    <label class="control-label" for="Location">Co-ordinates</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input name="location" id="disabledInput" type="text" placeholder="" class="coordinates"
		       	value="<?php if(isset($_POST['firstname'])) {echo $_POST['firstname'];}else if(isset($firstname)) {echo $firstname;}?>"
		       	readonly>
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		        <?php if (isset($firstname_error) && $firstname_error==1){?><span class="help-inline">Firstname is required.</span><?php }?>
		    </div>
		  </div>
		  
<!---------------------------------------------BEGINNING  OF QUESTIONS------------------------------------------------------------ -->	
		   <legend  class="lead">2. Family Situation</legend>
		   <span class="add-on">Number of family members in household</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<select name="noofpeople"  class="span2">
		       	<option value="0" selected>--</option>
		     	<?php  for ($i=1; $i<=20; $i++){?>
		     		<option value="<?php echo $i;?>" > <?php echo $i;?> </option>
				<?php }?>
		       	</select>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">Type of House</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<select name="typeofhouse"  class="span3">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" >Permantent</option>
		     		<option value="2" >Semi-Permantent</option>
		
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <legend  class="lead">3. Cookstove Information</legend>
		  <span class="add-on">Previous Traditional Stove used</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		    
		       <div class="input">
		       	<select name="previousstove"  class="span3">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" >Three Stones</option>
		     		<option value="2" >Charcoal Stove</option>
		     		<option value="3" >Gas</option>
		     		<option value="4" >Biofuel</option>
		     		<option value="5" >Woodstove</option>
		     		<option value="6" >other</option>
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		
		  <span class="add-on">Are you still using the traditional three-stones stove?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<select name="typeofhouse"  class="span2">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" >Yes</option>
		     		<option value="2" >No</option>
		
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">If yes:</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       
		       	<label class="radio">
  					<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
  						Use of low efficient stoves on special occasion only
				</label>
				<label class="radio">
  				<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
  					Increase in household size that justifies the use of a second three-stones stove because the Hifadhi stove is being fully utilized
				</label>
				<label class="radio">
  				<input type="radio" name="optionsRadios" id="optionsRadios4" value="option4">
  					Transfer of the  ownership (e.g. sales or gifts) with sufficient evidence by the original owner that the Hifadhi stove is still in use
				</label>
				<label class="radio">
  				<input type="radio" name="optionsRadios" id="optionsRadios4" value="option4">
  					Other reason
				</label>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">Are you still using the Hifadhi stove?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		        <div class="input">
		       	<select name="typeofhouse"  class="span2">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" >Yes</option>
		     		<option value="2" >No</option>
		
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">For how long have you used the Hifadhi stove?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		      <div class="input">
		       	<select name="noofpeople"  class="span2">
		       	<option value="0" selected>--</option>
		     	<?php  for ($i=1; $i<=10; $i++){?>
		     		<option value="<?php echo $i;?>" > <?php echo $i;?> </option>
				<?php }?>
		       	</select>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		   <span class="add-on">Is there improvement in your kitchen environmentand reduction of fuel consumption since?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		      <div class="input">
		       	<select name="typeofhouse"  class="span2">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" >Yes</option>
		     		<option value="2" >No</option>
		
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">Is there reduction in the number of hours used to cook since?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		      <div class="input">
		       	<select name="typeofhouse"  class="span2">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" >Yes</option>
		     		<option value="2" >No</option>
		
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">Where is your primary cooking location after acquiring the stove?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<select name="typeofhouse"  class="span4">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" > Inside the main house</option>
		     		<option value="2" > Outside</option>
		     		<option value="2" > Separate enclosed kitchen</option>
		     
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">Degree of satisfaction with new Hifadhi stove?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<select name="noofpeople"  class="span2">
		       	<option value="0" selected>--</option>
		     	<?php  for ($i=1; $i<=10; $i++){?>
		     		<option value="<?php echo $i;?>" > <?php echo $i;?> </option>
				<?php }?>
		       	</select>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">Why? what do you like about it? (explain)</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		      <div class="input-append">
		       	<textarea name="postal_address" rows="3">
		       	<?php if(isset($_POST['postal_address'])) {echo $_POST['postal_address'];}else if(isset($postal_address)) {echo $postal_address;}?>
		       	</textarea>
		       </div>
		       
		  </div>
		  
		  <span class="add-on">How often are you using the Hifadhi stove?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<select name="typeofhouse"  class="span3">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" > Every day</option>
		     		<option value="2" > Once a week</option>
		     		<option value="3" > Once a month</option>
		     		<option value="4" > Never</option>
		     
		       	</select>
		     
		       </div>
		       choice: every day; once a week; once a month; never
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		 
		  
		  <legend  class="lead">4. Fuel Wood</legend>
		  <span class="add-on">Where do you acquire fuelwood for cooking?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<select name="typeofhouse"  class="span3">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" > Forest</option>
		     		<option value="2" > Own land</option>
		     		<option value="3" > I buy it</option>
		     
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">What distance you travel to collect fuelwood?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input-append">
		       	<input name="telephone" id="appendedInput" type="text" placeholder=""
		       	value="<?php if(isset($_POST['telephone'])) {echo $_POST['telephone'];}else if(isset($telephone)) {echo $telephone;}?>">
		       	 <span class="add-on">Km</span>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">How often do you collect fuelwood?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input-append">
		       	<input name="telephone" id="appendedInput" type="text" placeholder=""
		       	value="<?php if(isset($_POST['telephone'])) {echo $_POST['telephone'];}else if(isset($telephone)) {echo $telephone;}?>">
		       	 <span class="add-on">times/week</span>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">What quantity of fuelwood you use per day?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input-append">
		       	<input name="telephone" id="appendedInput" type="text" placeholder=""
		       	value="<?php if(isset($_POST['telephone'])) {echo $_POST['telephone'];}else if(isset($telephone)) {echo $telephone;}?>">
		       	 <span class="add-on">Kgs or Ksh</span>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">State any complain or challenge while using the stove</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input-append">
		       <textarea name="postal_address" rows="3">
		       	<?php if(isset($_POST['postal_address'])) {echo $_POST['postal_address'];}else if(isset($postal_address)) {echo $postal_address;}?>
		       	</textarea>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <legend  class="lead">5. Other</legend>
		  <span class="add-on">Do you  experience much smoke in the kitchen using the Hifadhi stove?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<select name="typeofhouse"  class="span2">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" >Yes</option>
		     		<option value="2" >No</option>
		
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  
		 <span class="add-on">Do you feel this has improved since you use the Hifadhi stove?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<select name="typeofhouse"  class="span2">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" >Yes</option>
		     		<option value="2" >No</option>
		
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">What energy source do you use for lighting?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		      <div class="input">
		       	<select name="typeofhouse"  class="span3">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" >Kerosene Lamp</option>
		     		<option value="2" >Pressure Lamp</option>
		     		<option value="3" >Electricity</option>
		     		<option value="4" >Solar</option>
		     		<option value="5" >Other</option>
		
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">What size of land is comprised in the household?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		        <div class="input-append">
		       	<input name="telephone" id="appendedInput" type="text" placeholder=""
		       	value="<?php if(isset($_POST['telephone'])) {echo $_POST['telephone'];}else if(isset($telephone)) {echo $telephone;}?>">
		       	 <span class="add-on"> ha</span>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <legend  class="lead">6. Tree Planting</legend>
		  <span class="add-on">What is your preferred species of fuelwood?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<input name="telephone" id="appendedInput" type="text" placeholder=""
		       	value="<?php if(isset($_POST['And the second?telephone'])) {echo $_POST['telephone'];}else if(isset($telephone)) {echo $telephone;}?>">
		       
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">And the second?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<input name="telephone" id="appendedInput" type="text" placeholder=""
		       	value="<?php if(isset($_POST['telephone'])) {echo $_POST['telephone'];}else if(isset($telephone)) {echo $telephone;}?>">
		       	
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">What tree species you would like to receive?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<input name="telephone" id="appendedInput" type="text" placeholder=""
		       	value="<?php if(isset($_POST['telephone'])) {echo $_POST['telephone'];}else if(isset($telephone)) {echo $telephone;}?>">
		       	 
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">Have you ever been trained on tree planting before Hifadhi-Livelihoods project?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		      <div class="input">
		       	<select name="typeofhouse"  class="span2">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" >Yes</option>
		     		<option value="2" >No</option>
		
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">Did you receive any seedlings since the beginning of the project?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<select name="typeofhouse"  class="span2">
		       		
		       		<option value="0" selected>--</option>
		     		<option value="1" >Yes</option>
		     		<option value="2" >No</option>
		
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">If yes, how many?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		         <div class="input">
		       	<select name="typeofhouse"  class="span2">
		       		
		       		<option value="0" selected>--</option>
		       		<?php for($i=1; $i<=40; $i++){?>
		     		<option value="<?php echo $i;?>" > <?php echo $i;?></option>
		     		<?php }?>
		
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">How many seedlings are still growing?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		        <div class="input">
		       	<select name="typeofhouse"  class="span2">
		       		
		       		<option value="0" selected>--</option>
		       		<?php for($i=1; $i<=40; $i++){?>
		     		<option value="<?php echo $i;?>" > <?php echo $i;?></option>
		     		<?php }?>
		
		       	</select>
		     
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">How satisfied are you with the tree planting project?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		    <div class="input">
		       	<select name="typeofhouse"  class="span2">
		       		
		       		<option value="0" selected>--</option>
		       		<?php for($i=1; $i<=10; $i++){?>
		     		<option value="<?php echo $i;?>" > <?php echo $i;?></option>
		     		<?php }?>
		
		       	</select>
		     
		       </div>
		      
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  <span class="add-on">Why?</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		        <div class="input-append">
		      <textarea rows="3" cols="6"></textarea>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
		  
		  
<!---------------------------------------------END OF QUESTIONS------------------------------------------------------------ -->		  
		  <div class="form-actions">
  		  	<button type="submit" class="btn btn-primary">
  		  		<i class="icon-hdd icon-white"></i> 
  		  		 	<?php if ($edit_mode){?>Save
  		  		 	<?php }else{?>
  		  		 		Save Repsonses
  		  		 	<?php }?>
  		  	</button>
  			<a class="btn" href="<?php echo BASE_URL ;?>surveys">Cancel</a>
		</div>
		</form>
		
      </div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`employees` (`surname`, `firstname`, `gender`, `telephone`, `postal_address`, `postal_code`, `town`, `email`, `added_by`, `date_added`, `date_updated`) VALUES ('Doe', 'John', 1, '0722123456', 'P.O. BOX 123456', '00200', 'Nairobi', 'name@example.com', 1, '2013-02-07', '2013-02-07'); -->
	