<?php 

$questions = $Model->get_survey_summary_report($item_id);

require_once DIR_EXTENSIONS.'/phpexcel/Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Admin")
->setLastModifiedBy("Admin")
->setTitle("Responses Summary")
->setSubject("Office 2007 XLSX  Document")
->setDescription("Responses Summary.")
->setKeywords("office 2007 openxml php")
->setCategory("Resposes file");
$objPHPExcel->setActiveSheetIndex(0);

$rowCount = 1;
$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'Baseline Survey Report');
$rowCount++;
$rowCount++;


$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'Response #');
$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,'FO');
$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,'Date/Time Submitted');
$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,'Location');

$fields = $Model->get_questions_by_survey_id($item_id);
$col = 4;
foreach($fields as $field) {
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $rowCount, $field['field_name']);
    $col++;
}
   
$rowCount++;
$count=0;

foreach ($questions as $question){
	
	if($question['response_id']!=$response_id){
		$response_id= $question['response_id'];
		$count++;
	
		$rowCount++;
		$col = 4;
	}
	$count2++;
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $count);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $question['username']);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $question['date_added']);
	
	if ($question['username']== 'rmwosha'){
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Kyeni East');
	}else if ($question['username']== 'tnjeri'){
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Kyeni North East');
	}else if ($question['username']== 'snjeru'){
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Kagaari North East');
	}else if ($question['username']== 'lwanga'){
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Kagaari South West');
	}else if ($question['username']== 'nwamwea'){
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Kyeni South');
	}else if ($question['username']== 'jkagure'){
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Runyenjes East');
	}else if ($question['username']== 'swambura'){
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Runyenjes West');
	}else if ($question['username']== 'dmunene'){
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Kyeni Central');
	}else if ($question['username']== 'ewaweru'){
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Kagaari South East');
	}else if ($question['username']== 'wnguu'){
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Kyeni North West');
	}
		
	foreach($fields as $field) {
		if ($field['field_id'] == $question['field_id']){
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $rowCount, $question['value']);
		}
	}
	$col++;
}

$objPHPExcel->getActiveSheet()->setTitle('Baseline Responses Summary');
$objPHPExcel->setActiveSheetIndex(0);
ob_end_clean();
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Basline Survey Responses Summary.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>