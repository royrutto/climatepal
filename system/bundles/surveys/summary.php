<?php 
	$all_collected = $Model->get_survey_repsonses_summary($item_id)
?>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php //include DIR_BUNDLES.'/surveys/tab_menu.php'?>
	    	
<?php
 if (isset($item_id)){
 ?>
 <li class="<?php if($page_title =='Data Collection Summary')echo 'active';?>">
	<a href="<?php echo BASE_URL.'surveys/summary/'.$item_id?>" data-toggle="tab1">
	<span>Data Collection Summary</span></a>
</li>
<li class="<?php if($page_title =='Survey Summary')echo 'active';?>">
	<a href="<?php echo BASE_URL.'surveys/questions_summary/'.$item_id?>" data-toggle="tab1">
	<span>Baseline Survey Summary</span></a>
</li>
<li class="<?php if($page_title =='Stove Users Summary')echo 'active';?>">
	<a href="<?php echo BASE_URL.'surveys/users_summary/'.$item_id?>" data-toggle="tab1">
	<span>Stove Users Interviewed</span></a>
</li>
<?php }?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">
    		<div id="actions">
				<div id="actionbutton">
      			<a href="<?php echo BASE_URL ;?>surveys" class="btn btn-default"><i class="icon-chevron-left"></i>Back</a>
      			<a href="<?php echo BASE_URL ;?>surveys/download_summary/<?php echo $item_id; ?>" class="btn btn-success" target=""><i class="icon-arrow-down icon-white"></i>Download summary</a>
      			</div>
           </div>
		<table  cellpadding="0" cellspacing="0" border="0" class="display table table-bordered table-hover" id="example2" width="100%">
	<thead>
		<tr>
								<th><input type="checkbox" name="response_id" value=""></th>
								<th>Field Officer</th>
								<th>No. of questionnares filled</th>  		
					      		<!--th>Actions</th-->
		</tr>
		</tr>
	</thead>
	<tbody>
		<?php 
		$counter=0;
		foreach ($all_collected as $user){
								?>
							   <tr>
							   <td><input type="checkbox" name="response_id" value=" <?php echo $user['id']; ?>"></td>
							   <td><?php echo $user['employee']; ?></td>
							   <td><?php echo $user['filled']; ?></td>
							   <!--td>
  								  <div class="bs-docs-example tooltip-demo">
                      				<a href="<?php echo BASE_URL ;?>surveys/showresponses/<?php echo $item_id."/".$user['response_id']; ?>" title="View"><span class="badge badge-success"><i class="icon-search icon-white"></i></span></a>
                    			  </div>
							   </td--> 
							   </tr>
							   <?php } ?>
	
	</tbody>
	<tfoot>
		<tr>
								<th><input type="checkbox" name="response_id" value=""></th>
								<th>Field Officer</th>
								<th>No. of questionnares filled</th>  		
					      		<!--th>Actions</th-->
		</tr>
	</tfoot>
</table>
    			</div> <!-- End Tab 1-->
    			

    			 <div class="tab-pane" id="tab2">
      				<p>Howdy, I'm in Section 2.</p>
      				
          				<?php echo date();?>

    			 </div> <!-- End Tab 2-->

  				</div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->