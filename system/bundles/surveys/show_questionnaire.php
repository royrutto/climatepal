<?php

$questions = $Model->get_questions_by_survey_id($item_id);


$edit_mode = false;
if (isset ($item_id) && !$item_id=='' && $action!='fill'){
	$edit_mode =true;
}

if($edit_mode){
	$user_details  = $Model->get_user_by_id($item_id);
	
	if(count($user_details)>0){
		$surname = $user_details['surname'];
		$firstname = $user_details['firstname'];
		$gender = $user_details['gender'];
		$telephone = $user_details['telephone'];
		$postal_address = $user_details['postal_address'];
		$postal_code = $user_details['postal_code'];
		$postal_town = $user_details['town'];
		$email = $user_details['email'];
		$username = $user_details['username'];
		$role_id = $user_details['role_id'];
	}
}

?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab">
	    		<span><?php echo $page_title?></span></a>
	    	</li>		
  		</ul>
  		<?php if (isset($empty_input_exists) && $empty_input_exists){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
<!---------------------------------------------BEGINNING  OF QUESTIONS------------------------------------------------------------ -->			
		<span class="add-on">The fields marked <i class="icon-asterisk"></i> are mandatory.</span> 
      	<form class="form-horizontal"  action="<?php echo BASE_URL ;?>index.php" method="post">
      	<?php if ($edit_mode){?>
      	 	<input name="form_name" type="hidden" value="edit_response">
      	 	<input name="survey_id" type="hidden" value="<?php echo $item_id ;?>">
      	 <?php }else {?>
      	 	 <input name="form_name" type="hidden" value="add_response">
      	 	 <input name="survey_id" type="hidden" value="<?php echo $item_id ;?>
      	 <?php }?>
      	<?php 
      	$group_id="";
      	foreach ($questions as $question){
			if($question['group_id']!=$group_id){
				$group_id = $question['group_id'];
		?><legend  class="lead"><?php echo $question['group_id'].'. '.$question['group_name']; ?></legend>
		
		
		<?php }?>
			
		<?php if ($question['field_type']=='text'){?>	
		      	
      	  <span class="add-on"><?php echo $question['field_name'];?></span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input-append">
		       	<input name="<?php echo $question['field_id'];?>" id="appendedInput" type="text" placeholder=""
		       	value="<?php if(isset($_POST[$question['field_id']])) {echo $_POST[$question['field_id']];}else if(isset($telephone)) {echo $telephone;}?>">
		      	<?php if($question['required']==1){?>
		      	<span class="add-on"><i class="icon-asterisk"></i></span>
		      	<?php }?>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
      	<?php }?>
      	
      	
      	<?php if ($question['field_type']=='choicegroup'){?>	
		      	
      	  <span class="add-on"><?php echo $question['field_name'];?></span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       	<select name="<?php echo $question['field_id'];?>"  class="span2">
		       	<option value="" selected>--Select--</option>
		       	<?php $options = explode(",", $question['options']);?>
		     	<?php  for ($i=0; $i<count($options); $i++){?>
		     		<option value="<?php echo $options[$i];?>" > <?php echo $options[$i];?> </option>
				<?php }?>
		       	</select>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
      	<?php }?>
      	
      	<?php if ($question['field_type']=='radiogroup'){?>	
      	<span class="add-on">If yes:</span> 
		   <div class="control-group <?php if (isset($telephone_error) && $telephone_error==1){echo 'error';}?>">
		
		    <div class="controls span10">
		       <div class="input">
		       <?php $options = explode(",", $question['options']);?>
		       <?php  for ($i=0; $i<count($options); $i++){?>
		       	<label class="radio">
  					<input type="radio" name="<?php echo $question['field_id'];?>" id="optionsRadios1" value="<?php echo $options[$i];?>">
  						<?php echo $options[$i];?>
				</label>

		     	<?php }?>
		       </div>
		        <?php if (isset($telephone_error) && $telephone_error==1){?><span class="help-inline">Mobile number required.</span><?php }?>
		    </div>
		  </div>
      		<?php }?>
      	<?php }?>
      	
<!---------------------------------------------END OF QUESTIONS------------------------------------------------------------ -->		  
		  <div class="form-actions">
  		  	<button type="submit" class="btn btn-primary">
  		  		<i class="icon-hdd icon-white"></i> 
  		  		 	<?php if ($edit_mode){?>Save
  		  		 	<?php }else{?>
  		  		 		Save Repsonses
  		  		 	<?php }?>
  		  	</button>
  			<a class="btn" href="<?php echo BASE_URL ;?>surveys">Cancel</a>
		</div>
		</form>
		
      </div><!--/span 9-->
      <!-- INSERT INTO `climatepal`.`employees` (`surname`, `firstname`, `gender`, `telephone`, `postal_address`, `postal_code`, `town`, `email`, `added_by`, `date_added`, `date_updated`) VALUES ('Doe', 'John', 1, '0722123456', 'P.O. BOX 123456', '00200', 'Nairobi', 'name@example.com', 1, '2013-02-07', '2013-02-07'); -->
	