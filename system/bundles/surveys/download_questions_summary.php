<?php 

$questions = $Model->get_responses_summary($item_id);

require_once DIR_EXTENSIONS.'/phpexcel/Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Admin")
->setLastModifiedBy("Admin")
->setTitle("Responses Summary")
->setSubject("Office 2007 XLSX  Document")
->setDescription("Responses Summary.")
->setKeywords("office 2007 openxml php")
->setCategory("Resposes file");
$objPHPExcel->setActiveSheetIndex(0);

$rowCount = 1;
$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'Baseline Survey Summary');

$count=0;
	foreach ($questions as $question){
	$rowCount++;
	$count++;
	if($question['group_id']!=$group_id){
		$group_id = $question['group_id'];
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$question['group_id'].'. '.$question['group_name']);
		$rowCount++;
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'Question');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,'Answers');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,'Number');
		$rowCount++;
	}
			switch($question['field_type']){
			 case 'radiogroup':
			 	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$question['field_name']);
			 	$rowCount++;
				$values = $Model->get_question_summary($question['field_id'],$item_id, true);
	  			foreach ($values as $value){ 		
	  				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$value['value']);
	  				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,$value['number']);
	  				$rowCount++;
				}	
	      	break;
	      	case 'text':
				if ($question['field_id']==1 || $question['field_id']==2){
								if($question['field_id']==1){
									
									$values = $Model->get_question_summary($question['field_id'],$item_id, true);
									
									$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,'Number of Stove users interviewed');
									$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,count($values));
									$rowCount++;
									$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'');
									$rowCount++;
	  					
								}
				}else{
							$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$question['field_name']);
							$rowCount++;
								$values = $Model->get_question_summary($question['field_id'],$item_id, true);
	  							foreach ($values as $value){ 
								 	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$value['value']);
								 	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,$value['number']);
	  								$rowCount++;
							}
						}	
	         break;
	      	 case 'choicegroup':
			 	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$question['field_name']);
			 	$rowCount++;
				$values = $Model->get_question_summary($question['field_id'],$item_id, true);
	  			foreach ($values as $value){ 		
	  				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$value['value']);
	  				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,$value['number']);
	  				$rowCount++;
				}	
	      	break;
	    
	      	 }

}

$objPHPExcel->getActiveSheet()->setTitle('Baseline Responses Summary');
$objPHPExcel->setActiveSheetIndex(0);
ob_end_clean();
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Basline Survey Responses Summary.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>