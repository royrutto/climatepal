<?php 

$questions = $Model->get_responses_summary($item_id);

require_once DIR_EXTENSIONS.'/phpexcel/Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Admin")
->setLastModifiedBy("Admin")
->setTitle("Responses Summary")
->setSubject("Office 2007 XLSX  Document")
->setDescription("Responses Summary.")
->setKeywords("office 2007 openxml php")
->setCategory("Resposes file");
$objPHPExcel->setActiveSheetIndex(0);

$rowCount = 1;
$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'Interviewed Stove User Summary');
$rowCount++;
$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'User ID');
$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,'Stove User name');


$count=0;
	foreach ($questions as $question){
	$rowCount++;
	
	
		
		
	
			switch($question['field_type']){
			
	      	case 'text':
	      		if ($question['field_id']==1){
							$values = $Model->get_question_summary($question['field_id'],$item_id, true);
  							foreach ($values as $value){ 
								
  							
							 	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$value['value']);
							 	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$Model->get_resonse_name($value['response_id']));
							 	$rowCount++;
  							
							 } 
	      		}
				
	         break;
	      
	    
	      	 }

}

$objPHPExcel->getActiveSheet()->setTitle('Interviewed Stove Users Summary');
$objPHPExcel->setActiveSheetIndex(0);
ob_end_clean();
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Interviewed Stove Users Summary.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>