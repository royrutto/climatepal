<?php 
$edit_mode = false;
if (isset ($item_id) && !$item_id==''){
	$edit_mode =true;
}

if($edit_mode){
	$sale_details  = $Model->get_sale_by_id($item_id);
	
	if(count($sale_details)>0){
	
		$cpa = $sale_details['cpa'];
		$serial = $sale_details['firstname'];
		$date = $sale_details['gender'];
		$name = $sale_details['telephone'];
		$gender = $sale_details['postal_address'];
		$natID = $sale_details['postal_code'];
		$mobile = $sale_details['town'];
		$household = $sale_details['email'];
		$lat = $sale_details['salename'];
		$long = $sale_details['role_id'];
		$supplier = $sale_details['role_id'];
		$old = $sale_details['role_id'];
	}
}



?>
      <div class="span9">
      
      	<ul class="nav nav-tabs">
	    	<li class="active"><a href="#" data-toggle="tab">
	    		<span><?php echo $page_title?></span></a>
	    	</li>		
  		</ul>
  		<?php if (isset($empty_input_exists) && $empty_input_exists){?>
  		<div class="alert alert-error">
 		<strong>Error! </strong>Check form details and save again.
		</div>
		<?php }?>
      	<form class="form-horizontal"  action="<?php echo BASE_URL ;?>index.php" method="post">
      	<?php if ($edit_mode){?>
      	 	<input name="form_name" type="hidden" value="edit_survey">
      	 	<input name="survey_id" type="hidden" value="<?php echo $item_id ;?>">
      	 <?php }else {?>
      	 	 <input name="form_name" type="hidden" value="add_survey">
      	 <?php }?>
      	 
      	 <div class="control-group"></div>
      	 
		   <div class="control-group <?php if (isset($cpa_error) && $cpa_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputEmail">Survey Name</label>
		    <div class="controls">
		       <div class="input-append">
		       	<input  name="cpa" id="appendedInput" type="text" placeholder="Survey Name" value="<?php if(isset($_POST['cpa'])) {echo $_POST['cpa'];}else if(isset($cpa)) {echo $cpa;}?>">
		       	 <span class="add-on"><i class="icon-asterisk"></i></span>
		       </div>
		       <?php if(isset($cpa_error) && $cpa_error==1){?><span class="help-inline">Surname is required.</span><?php }?>
		    </div>
		  </div>
		  
		  <div class="control-group <?php if (isset($postal_address_error) && $postal_address_error==1){echo 'error';}?>">
		    <label class="control-label" for="inputError">Survey Description</label>
		    <div class="controls">
		       <div class="input-append">
		       	<textarea name="postal_address" rows="3">
		       	<?php if(isset($_POST['postal_address'])) {echo $_POST['postal_address'];}else if(isset($postal_address)) {echo $postal_address;}?>
		       	</textarea>
		       </div>
		         <?php if (isset($postal_address_error) && $postal_address_error==1){?><span class="help-inline">Postal Address is required.</span><?php }?>
		    </div>
		  </div>
		  <legend>Questionnare Design</legend>
		  <div id="my-form-builder"></div>
		  
		  
		
		  
		 
		  <div class="form-actions">
  		  	<button type="submit" class="btn btn-primary" data-loading-text="Saving...">
  		  		<i class="icon-hdd icon-white"></i> 
  		  		 	<?php if ($edit_mode){?>Save
  		  		 	<?php }else{?>
  		  		 		Save Survey
  		  		 	<?php }?>
  		  	</button>
  			<a class="btn" href="<?php echo BASE_URL ;?>surveys">Cancel</a>
		</div>
		</form>
		
      </div><!--/span 9-->
	