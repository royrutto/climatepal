<?
/*
 * Climate Pal
 * Incharge of interfacing between client side and Application layer (class.app.php)
 */
session_start();

define('BASE_URL','http://' . $_SERVER['HTTP_HOST'].'/climatepal/');
define('DIR_WEB', dirname(__FILE__).'/..');
define('DIR_SYS', DIR_WEB.'/system');
define('DIR_LIB', DIR_SYS.'/lib');
define('DIR_TEMPLATE', DIR_SYS.'/template');
define('DIR_BUNDLES', DIR_SYS.'/bundles');

date_default_timezone_set('Africa/Nairobi');
define('DATE_FORMAT_DEFAULT', "Y-m-d H:i:s");
define('DATE_FORMAT_DATEPICKER', "d-m-Y");

//load required libraries/models
require_once(DIR_LIB.'/load_models.php');

//container for data to be returned to app.js
$app_data = array();

//if form data is being posted
if(!empty($_POST['form'])){
	$posted_form = $_POST['form'];
	switch($posted_form){
		//handles login
		case 'login':
			
			//get login details
			if (!empty($_POST['username'])) $username = $_POST['username'];
			if (!empty($_POST['password'])) $password = $_POST['password'];
		
			$device_type='mobile';
			include(DIR_SYS.'/checklogin.php');
			$app_data = $login_details;
			
			header('Vary: Accept-Encoding');
			header('Content-Type: application/json');
			print json_encode($app_data);
		break;
		
		//handle new distribution details
		case 'newdistribution':
				
			//New Distribution details
			$cpa = $_POST['cpa'];
			$serial = $_POST['serial'];
			$date = $_POST['date'];
			$firstname = $_POST['firstname'];
			$middlename = $_POST['middlename'];
			$householdname = $_POST['householdname'];
			$gender = $_POST['gender'];
			$id = $_POST['userid'];
			$mobile = $_POST['mobile'];
			$location = $_POST['location'];
			$latitude = $_POST['latitude'];
			$longitude = $_POST['longitude'];
			$old = $_POST['oldstove'];
			$added_by = $_POST['added_by'];
			

	
			$device_type='mobile';
			include(DIR_BUNDLES.'/sales/validate_add_sale.php');

			if($data_saved){
				$app_data['dist_status'] =  'success';
				
			}else{
			
				$app_data['dist_status'] =  'fail';
				
			}
			header('Vary: Accept-Encoding');
			header('Content-Type: application/json');
			print json_encode($app_data);
						
		break;
		
		//save survey resuts
		case 'savesurvey':
		
			//New Distribution details
			$survey_id = $_POST['surveyid'];
			$results = $_POST['results'];
			$added_by = $_POST['added_by'];
			$latitude = $_POST['latitude'];
			$longitude = $_POST['longitude'];
			
			$field_values = explode("&", $results);
			$fields = array();
			foreach($field_values as $field_value){
				$getvalues = explode("=", $field_value);
				$fields[''.$getvalues[0].''] = $getvalues[1];
			}
			
			
			$device_type='mobile';
			include(DIR_BUNDLES.'/surveys/validate_add_response.php');
		
			if($data_saved){
				$app_data['dist_status'] =  'success';
		
			}else{
					
				$app_data['dist_status'] =  'fail';
		
			}
			header('Vary: Accept-Encoding');
			header('Content-Type: application/json');
			print json_encode($app_data);
		
			break;
		
		//logout
		case 'logout':
			$device_type='mobile';
			include(DIR_SYS.'/logout.php');
			$app_data['logout_status'] =  'logged_out';
			header('Vary: Accept-Encoding');
			header('Content-Type: application/json');
			print json_encode($app_data);
		break;
		
		
		//getaccountinfo
		case 'get_account_data':
			$device_type='mobile';
			$user_id=$_POST['user_id'];
			
			$app_data = $Users->get_upload_info($user_id);
		
			
			header('Vary: Accept-Encoding');
			header('Content-Type: application/json');
			print json_encode($app_data);
		break;
					
	}//end posted_form switch
	
}else if(!empty($_GET['cpa'])){
	
	//New Distribution details
	$cpa = $_GET['cpa'];
	$serial = $_GET['serial'];
	$date = $_GET['date'];
	$firstname = $_GET['firstname'];
	$middlename = $_GET['middlename'];
	$householdname = $_GET['householdname'];
	$gender = $_GET['gender'];
	$id = $_GET['userid'];
	$mobile = $_GET['mobile'];
	$location = $_GET['location'];
	$latitude = $_GET['latitude'];
	$longitude = $_GET['longitude'];
	$old = $_GET['oldstove'];	
	/* lhttp://localhost/climatepal/mobile/app.php?cpa=123&serial=1223&date=2012-12-12&user=test&gender=1&userid=23232&mobile=123&householdname=household&latitude=0.0&longitude=0.0&oldstove=1%20%3E%3E%20localhoslocalhost/climatepal/mobile/app.pp?cpa=123&serial=1223&date=2012-12-12&name=test&gender=1&userid=23232&mobile=123&householdname=household&latitude=0.0&longitude=0.0&oldstove=1 */
	$device_type='mobile';
	include(DIR_BUNDLES.'/sales/validate_add_sale.php');
	
	if($data_saved){
		$app_data['dist_status'] =  'success';
		
	}else{
	
		$app_data['dist_status'] =  'fail';
		
	}
	header('Vary: Accept-Encoding');
	header('Content-Type: application/json');
	print json_encode($app_data);
}
else{
	
	//get widget content based on getJSON request and parameters
	switch($_GET['a']){	

		//populate subtype and make dropdowns
		case 'initNewDistForm':
			$user_id= $_GET['userid'];
			$app_data['locations'] = $Sales->getNewDistUIData('locations', $user_id);
			$app_data['stovetypes'] = $Sales->getNewDistUIData('stovetypes');
		break;
		
		case 'initSurveyForms':
			$app_data['surveys']= $Surveys->get_all_surveys();
			$app_data['questions']= $Surveys->get_questions_by_survey_id(0, true);
		break;
	
	}
	
	//set necessary headers to allow JSONP and storage of compressed and uncompressed formats
	header('Access-Control-Allow-Origin:*');
	header('Vary: Accept-Encoding');
	header('Content-Type: application/json');
	//json-encode and return JSONP
	print $_GET['callback'].'('.json_encode($app_data).')';
}
?>