/**Climate Pal Mobile Web App System || Client Side Scripting**/
jQuery(document).ready(function($) {
/**********************************************INTITIALIZE GLOBAL VARIABLES**********************************************/
	var db = openDatabase('climatepal', '1.0', 'Climate Pal local DB', 2 * 1024 * 1024); //open local database
	var timer;
	var count=0;
	var user_id=0;
	var surveys;
/****************************************************************************************************************************/

/*********************************************DETECT ANY CHANGES IN HTML TREE*************************************************/
	//check if any access or page change has a logged in user
	$(document).bind('DOMSubtreeModified', function () {
		//check if database has any user
		if (timer) clearTimeout(timer);
		    timer = setTimeout(function() { checksession();}, 100);
		function checksession() {
			db.transaction(function (tx) {
				  tx.executeSql('SELECT * FROM USERS', [], function (tx, results) {
				   var len = results.rows.length;
				   if (len<1){
					   //$.mobile.changePage("#login", "slideright");
				   } else{
					   var user_id=null;
					   var user_name =null;
					   for (var i = 0; i < len; i++){
						   user_id=results.rows.item(i).id;
						   user_name = results.rows.item(i).username;	  
					   }
					   $('#logged_in_user').html(user_name);
					   $('#logged_in_user2').html(user_name);
					   initialize();
					   upload_dist_data();
					   upload_survey_data();
				   }	
				 }, null);
			});
		}
	});
/*********************************************************************************************************************************/	

/*********************************************CREATE WEB SQL TABLES FOR OFFLINE STORAGE*****************************************************/	
	db.transaction(function (tx) {
		   tx.executeSql('CREATE TABLE IF NOT EXISTS USERS (id unique, username, sessionid)');
		   tx.executeSql('CREATE TABLE IF NOT EXISTS DISTRIBUTIONS (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,time,cpa,serial,date,firstname,middlename,surname,gender,userid,mobile,location,lat,long,oldstove,added_by)');
		   tx.executeSql('CREATE TABLE IF NOT EXISTS SURVEYS (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, time, surveyid,results,added_by,latitude,longitude)');
		   tx.executeSql('CREATE TABLE IF NOT EXISTS QUESTIONS (loaded_questions)');
		   tx.executeSql('CREATE TABLE IF NOT EXISTS LOADED_SURVEYS (surveys blob)');
	});
/*********************************************************************************************************************************************/

/*******************************************CHECK IF DATABASE HAS ANY USER*******************************************************************/
	db.transaction(function (tx) {
		  tx.executeSql('SELECT * FROM USERS', [], function (tx, results) {
		   var len = results.rows.length, i;
		   
		   if (len<1){
			   $.mobile.changePage("#login", "slideright");
		   } else{
			   $.mobile.changePage("#home", "slideright");
		   }
		 }, null);
	});
/***********************************************************************************************************************************************/

/**********************************************************GET USER'S LOCATION*****************************************************/
	lat = null;
	lng = null;
	
	var x=document.getElementById("location");
	var y=document.getElementById("coordinates");
	var v=document.getElementById("location2");
	var w=document.getElementById("coordinates2");
	var geocoder;

	if (navigator.geolocation) {
	    navigator.geolocation.watchPosition(successFunction, errorFunction);
	}   else{x.innerHTML="Geolocation is not supported by this browser.";}
	//Get the latitude and the longitude;
	function successFunction(position) {
	    lat = position.coords.latitude;
	    lng = position.coords.longitude;
	    codeLatLng(lat, lng);
	}
	function errorFunction(){
	    alert("Geocoder failed");
	}
	function initialize() {
	    geocoder = new google.maps.Geocoder();
	}

	function codeLatLng(lat, lng) {
	   var latlng = new google.maps.LatLng(lat, lng);
	   geocoder.geocode({'latLng': latlng}, function(results, status) {
	     if (status == google.maps.GeocoderStatus.OK) {
	     console.log(results)
	       if (results[1]) {
	    	   //formatted address
	    	   //alert(results[0].formatted_address)
	    	   //find country name
	           for (var i=0; i<results[0].address_components.length; i++) {
	        	   for (var b=0;b<results[0].address_components[i].types.length;b++) {
	        		   //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
		               if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
		                   //this is the object you are looking for
		                   city= results[0].address_components[i];
		                   break;
		               }
	        	   }
	           }
	       //city data
	       //alert(city.short_name + " " + city.long_name)
	      	x.innerHTML=results[0].formatted_address;
	      	v.innerHTML=results[0].formatted_address;
	      	
	    	//y.innerHTML='Lat: '+lat+', Long: '+lng;
	    	//w.innerHTML='Lat: '+lat+', Long: '+lng;

	       } else {
	       		x.innerHTML="No results found";
	       		v.innerHTML="No results found";
	       }
	     } else {
	   	 	 x.innerHTML="Geocoder failed due to: " + status;
	   	 	 v.innerHTML="Geocoder failed due to: " + status;
	     }
	   });
	 }
/****************************************************************************************************************************/
		
/**********************************************CALL ALL INITIALIZATION METHODS***********************************************/
	initialize();
	if (navigator.onLine) {
		getSurveys();
	}else{
		initSurveyForms(surveys);
	}
	initDistributionForm(user_id);
/*****************************************************************************************************************************/
	
/*********************************************ADD LISTENERS TO BUTTONS********************************************************/
	
	/*----------------------------------------LOGIN BUTTON----------------------------------------------------------*/
	$("#login_button").live("click", {action:"login"}, checklogin);

	/*----------------------------------------LOGOUT BUTTON----------------------------------------------------------*/
	$("#logout_button").live("click", {action:"logout"}, logout);
	
	/*----------------------------------------ACCOUNT INFO BUTTON---------------------------------------------------------------*/
	$('#account_info').live("click", {action:""}, get_account_data);
	
	/*----------------------------------------SAVE DISTRIBUTION BUTTON----------------------------------------------------------*/
	$("#savedist_button").live("click", {action:"save_dist"}, savedistribution);
	
	/*----------------------------------------NEW DISTRIBUTION BUTTON-----------------------------------------------------------*/
	$('#new_dist').live("click", {action:""},cleartextfields);
	
/*******************************************************************************************************************************************/

/********************************************* ALL APP METHODS/FUNCTIONS********************************************************************/
/*------------------------------------------------CHECK LOGIN-----------------------------------------------------------------*/	
	
	function checklogin(){
		$.mobile.loadingMessageTextVisible = true;
		$.mobile.showPageLoadingMsg("a", "Checking credentials, please wait.");
		
		db.transaction(function (tx) {
			  tx.executeSql('SELECT * FROM USERS', [], function (tx, results) {
			   var len = results.rows.length, i;
			   
			   if (len<1){
				   $.post(
							'app.php',
							{
								form:'login',
								username: $('.var_username').val(),
								password:$('.var_password').val()
							},
							function(data){
								if(data.login_status==true){
									//save logged in user details in local browser db
									db.transaction(function (tx) {
										tx.executeSql('INSERT INTO USERS (id, username, sessionid) VALUES ('+data.login_userid+', "'+data.login_username+'", "'+data.login_sessionid+'")');
										initDistributionForm(data.login_userid);
									});
									user_logged_in = true;
									$.mobile.changePage("#home", "slideright");
									$('#logged_in_user').html(data.login_username);
									$.mobile.hidePageLoadingMsg ();
								}
								else{
									$('#login_status').html('<p>Invalid details. Try again</p>');
									$('#login_status').slideDown("slow").delay(5000).slideUp("slow");
									$.mobile.hidePageLoadingMsg ();
								}
							}
						);
			   } else{
				   $.mobile.changePage("#home", "slideright");
			   }
			 }, null);
		});
		
		return false;	
	}

/*----------------------------------------------------------------------------------------------------------------------------------*/	
	
/*------------------------------------------------LOGOUT-----------------------------------------------------------------*/	
	
	function logout(){
		
		$.mobile.loadingMessageTextVisible = true;
		$.mobile.showPageLoadingMsg("a", "Logging out, please wait.");
		
		if (confirm('Are you sure you want to log out?')) {
			$.post(
					'app.php',
					{
						form:'logout',
					},
					function(data){
						//save logged in user details in local browser db
						db.transaction(function (tx) {
							tx.executeSql('DELETE FROM USERS', []);
						});
						$.mobile.changePage("#login", "slideright");
						$.mobile.hidePageLoadingMsg ();
						user_logged_in = false;
					}
				);
		}else{
			$.mobile.hidePageLoadingMsg ();
		}
		return false;	
	}	
/*---------------------------------------------------------------------------------------------------------------------------------*/	

/*------------------------------------------------CLEAT TEXT FIELDS-----------------------------------------------------------------*/
	
	function cleartextfields(){
		$('input:text').val('');
		$('.var_cpa').val('');
		$('.var_userid').val('');
		$('.var_mobile').val('');
		$('.var_serial').val('CP');
	}
/*----------------------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------SAVE DISTRIBUTION-----------------------------------------------------------------*/
	
	function savedistribution(){
		$.mobile.loadingMessageTextVisible = true;
		$.mobile.showPageLoadingMsg("a", "Saving distribution, please wait.");
		
		var gender = null;
		if($('.var_male').is(':checked')) {
			gender = '1';
		} else{
			gender = '2';
		}
		
		db.transaction(function (tx) {
			  tx.executeSql('SELECT * FROM USERS', [], function (tx, results) {
			   var len = results.rows.length, i;
			   
			   if (len<1){
				   $.mobile.changePage("#login", "slideright");
			   } else{
				   for (i = 0; i < len; i++){
				     //alert(results.rows.item(i).username);
					  /** TODO **/
					   user_id = results.rows.item(i).id;  
				   }
			   }
				 }, null);
		}); 
				   
		var distribution_details= $('.distributionform').serialize();
		var dist_values = distribution_details.split('&');
		var values = null;
		var null_value_exists = false;
		$.each(dist_values, function( i, item ) {
			values = item.split('=');
			if (values[1]==null ||values[1]=="")
				null_value_exists =true;
			});
		if (null_value_exists){
			$.mobile.hidePageLoadingMsg ();
			  alert("Please fill all the fields before saving!");
		}else{
			if (confirm('Are you sure you want to save distribution?')) {
				//save logged in user details in local browser db
				db.transaction(function (tx) {
					var currentdate = new Date();
					var datetime = currentdate.getDate() + "/"+(currentdate.getMonth()+1) 
				    + "/" + currentdate.getFullYear() + " @ " 
				    + currentdate.getHours() + ":" 
				    + currentdate.getMinutes() + ":" + currentdate.getSeconds();
					
					tx.executeSql('INSERT INTO DISTRIBUTIONS (time, cpa, serial, date, firstname, middlename, surname, gender, userid, mobile, location, lat, long, oldstove, added_by)'
									+'VALUES ("'+datetime+'","'
									+$('.var_cpa').val()+'", "'
									+$('.var_serial').val()+'", "'
									+$('.var_date').val()+'", "'
									+$('.var_firstname').val()+'", "'
									+$('.var_middlename').val()+'", "'
									+$('.var_householdname').val()+'", "'
									+gender+'", "'
									+$('.var_userid').val()+'", "'
									+$('.var_mobile').val()+'", "'
									+$('.var_location').val()+'", "'
									+lat+'", "'
									+lng+'", "'
									+$('.var_oldstovetype').val()+'", "'
									+user_id+'")');
					});
					
					$.mobile.changePage("#home", "slideright");
					$('#dist_status').html('<p>Distribution saved</p>');
					$('#dist_status').slideDown("slow").delay(4000).slideUp("slow");
					$.mobile.hidePageLoadingMsg ();
					
					} else {
					    // Do nothing!
						$.mobile.hidePageLoadingMsg();
					}
				}

		return false;	
	}
/*---------------------------------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------INITIALIZE AND LOAD DISTRIBUTION FORM----------------------------------------------------------*/
	//loads the subtypes and makes from the db to the search form UI
	
	function initDistributionForm($userid){
		$('.var_serial').val('CP');
		
		$('.var_serial').bind('input propertychange', function() {
		    if($(this).val().length<=2){
		    	$('.var_serial').val('CP');
		    }
		});

		$('.var_serial').bind("keypress", function(event) { 
		    var charCode = event.which;
		    var current_string = $(this).val();
		    var keyChar = String.fromCharCode(charCode); 
		   
		    if (charCode <= 13) return true; 

		    var keyChar = String.fromCharCode(charCode); 
		   
		    if(!(current_string.length>=8)){
		        if(current_string.length>=2 && current_string.length<7){
		            return /[0-9]/.test(keyChar);
		    	}else if (current_string.length>=7){
		    		return /[a-zA-Z]/.test(keyChar);
		        }else{
		    	    return false;
		    	}
		    }else return false;
		    	   
		});

		/* Form Validations */
		$('.var_cpa').bind("keypress", function(event) { 
		    var charCode = event.which;
		    var keyChar = String.fromCharCode(charCode); 
		    if (charCode <= 13) return true;
		    return /[0-9]/.test(keyChar);
		});

		$('.var_user').bind("keypress", function(event) { 
		    var charCode = event.which;
		    var keyChar = String.fromCharCode(charCode); 
		    if (charCode <= 13) return true;
		    return /[a-zA-Z ]/.test(keyChar);
		});

		$('.var_household_name').bind("keypress", function(event) { 
		    var charCode = event.which;
		    var keyChar = String.fromCharCode(charCode); 
		    if (charCode <= 13) return true;
		    return /[a-zA-Z]/.test(keyChar);
		});

		$('.var_userid').bind("keypress", function(event) { 
		    var charCode = event.which;
		    var keyChar = String.fromCharCode(charCode); 
		    if (charCode <= 13) return true;
		    return /[0-9]/.test(keyChar);
		 
		});

		$('.var_mobile').bind("keypress", function(event) { 
		    var charCode = event.which;
		    var keyChar = String.fromCharCode(charCode); 
		    if (charCode <= 13) return true;
		    return /[0-9]/.test(keyChar);
		});

		$(".var_date").click(function() {
			$('.var_date').datebox('open');
		});
		$.getJSON(
			"app.php?callback=?", 
			{
				a: 'initNewDistForm',
				userid:$userid
			}, 
			function(result){
				//load content asynchronously into options
				if(typeof(result) !== 'undefined'){
					$('.var_location').append(result.locations);
					$('.var_oldstovetype').append(result.stovetypes);
				}
			}
		);
		return false;
	}
/*---------------------------------------------------------------------------------------------------------------------------------------*/	
	
/*------------------------------------------INITIALIZE AND LOAD SURVEY FORMS-------------------------------------------------------------*/
	
	function initSurveyForms(result){
				//load content asynchronously into options
				if(typeof(result) !== 'undefined'){
				
					//localStorage.setItem('testObject', JSON.stringify(result.surveys));

					// Retrieve the object from storage
					//var retrievedObject = localStorage.getItem('testObject');

					//console.log('retrievedObject: ', JSON.parse(retrievedObject));
							
					$.each(result.surveys, function( i, item ) {
						
						$(survey_list).append('<li><a href="#quest'+item.survey_id+'"><h3>'+item.description+'</h3></a><p></p></li>');
						
						$('body').append('<div id="quest'+item.survey_id+'" data-url="quest'+item.survey_id+'" data-role="page">'+
								'<div data-role="header" data-theme="e">'+
								'<h1>Climate Pal</h1>'+
								'<div data-role="navbar">'+
								'<ul>'+
								'<li><a href="" data-theme="b" data-icon="plus">'+item.description+'</a></li>'+
								'</ul>'+
								'</div>'+
								
								'</div>'+
								
								'<div data-role="content" id="questions'+item.survey_id+'">'+
								'<form class="quest'+item.survey_id+'"  id="form_tw43" method="POST">');
						var group_id=null;	
						$.each(result.questions, function( x, q_item ) {
							if(q_item.group_id!=group_id){
								group_id = q_item.group_id;
								$('.quest'+item.survey_id+'').append('<br><legend  class="lead"><strong>'+q_item.group_id+'. '+q_item.group_name+'</strong></legend>');
							}
							
							if(item.survey_id== q_item.survey_id){
								if(q_item.field_type=='choicegroup'){
									
									var options = '';
									if(q_item.options!=null)options =q_item.options.split(',');
									//console.log(options);
									var select_options = '';
									$.each(options, function(key, option) {
										select_options+='<option value="'+option+'">'+option+'</option>';
									});
									
									$('.quest'+item.survey_id+'').append(
										'<label for="this">'+q_item.field_name+'</label>'+
										'<div data-role="fieldcontain">'+
										'<select name="'+q_item.field_id+'" id="sel_tw57" data-native-menu="false">'+
										select_options+
										'</select>'+
										'</div>');
									}
								
									if(q_item.field_type=='text'){
										$('.quest'+item.survey_id+'').append(
											'<label for="this">'+q_item.field_name+'</label>'+
											'<div data-role="fieldcontain">'+
											'<input type="text" name="'+q_item.field_id+'" id="inp_tw44" />'+
											'</div>');
									}
									
									if(q_item.field_type=='radiogroup'){
										
										var options = '';
										if(q_item.options!=null)options =q_item.options.split(',');
										
										var select_options = '';
										var counter=0;
										$.each(options, function(key, option) {
											counter++;
											select_options+='<input id="rdo_tw9_'+counter+'" type="radio" name="'+q_item.field_id+'" value="'+option+'"/><label for="rdo_tw9_'+counter+'">'+option+'</label>';

										});
										
										$('.quest'+item.survey_id+'').append(
											'<div data-role="fieldcontain">'+
											'<fieldset data-role="controlgroup">'+
											'<legend>'+q_item.field_name+'</legend>'+
											select_options+
											'</fieldset>'+
											'</div>');
									}		
							}
						});
						$('.quest'+item.survey_id+'').append(
								'<input type="submit" id="savesurvey_button'+item.survey_id+'" data-icon="plus" data-theme="e" value="Save Response"/>'+
								'<a href="#home" data-role="button" data-icon="delete" data-theme="b">Cancel</a></form>');
						
						$('body').append('</div>'+'</div>');
						
						$('#savesurvey_button'+item.survey_id+'').live("click", {action:"submitsurvey"}, save_survey);
						
					});
				}
	}
/*---------------------------------------------------------------------------------------------------------------------------------------*/	

/*------------------------------------------SAVE SURVEY RESPONSE-------------------------------------------------------------------------*/
	
	function save_survey(){
		$.mobile.loadingMessageTextVisible = true;
		$.mobile.showPageLoadingMsg("a", "Saving response, please wait.");
		var n=document.URL.split('#');
		var x=n[1].substring(5);
		var survey_results= $('.quest'+x+'').serialize();
		
		//$('input:text').val('teesting');
		db.transaction(function (tx) {
			tx.executeSql('SELECT * FROM USERS', [], function (tx, results) {
				var len = results.rows.length, i;
				if (len<1){
					$.mobile.changePage("#login", "slideright");
				} else{
					for (i = 0; i < len; i++){
					 /** TODO **/
					   user_id = results.rows.item(i).id;
				   }
			   }
			}, null);
		});
				   
		var survey_values = survey_results.split('&');
		var values = null;
		var null_value_exists = false;
		//check for null value in questionnare
		$.each(survey_values, function( i, item ) {
			values = item.split('=');
			if (values[1]==null ||values[1]=="") 
				null_value_exists =true;
		});
		if (null_value_exists){
			$.mobile.hidePageLoadingMsg();
			alert("Please fill all the fields before saving!");
		}else{
			if (confirm('Are you sure you want to save response?')) {
				//save logged in user details in local browser db
				db.transaction(function (tx) {
					var currentdate = new Date();
					var datetime = currentdate.getDate() + "/"+(currentdate.getMonth()+1) 
				    + "/" + currentdate.getFullYear() + " @ " 
				    + currentdate.getHours() + ":" 
				    + currentdate.getMinutes() + ":" + currentdate.getSeconds();
					
					tx.executeSql('INSERT INTO SURVEYS (time, surveyid,results,added_by,latitude,longitude)'+
									'VALUES ("'+datetime+'","'+x+'", "'+survey_results+'", "'+user_id+'", "'+lat+'", "'+lng+'")');				
				});
				
				$('input:text').val('');
				$.mobile.changePage("#home", "slideright");
				$('#dist_status').html('<p>Survey results saved</p>');
				$('#dist_status').slideDown("slow").delay(4000).slideUp("slow");
				$.mobile.hidePageLoadingMsg ();
			} else {
			    // Do nothing!
				$.mobile.hidePageLoadingMsg();
			}
		}
	}
/*---------------------------------------------------------------------------------------------------------------------------------------*/	

/*-------------------------------------------UPLOAD SURVEY DATA TO SERVER----------------------------------------------------------*/
	//uploads all local data to server and deletes after uploaded
	
	function upload_survey_data(){
		//save data if all the fields are filled
		if (navigator.onLine) {
			//check for data that was saved when navigator was offline
			db.transaction(function (tx) {
				tx.executeSql('SELECT * FROM SURVEYS', [], function (tx, results) {
				var len = results.rows.length;
				//upload data that was saved when the phone/browser was offline
				if (len>0){
					for(var i = 0; i < len; i++){
						var id = results.rows.item(i).id;
						$.post('app.php',
								{
									form:'savesurvey',
									surveyid:results.rows.item(i).surveyid,
									results:results.rows.item(i).results,
									added_by:results.rows.item(i).added_by,
									latitude:results.rows.item(i).latitude,
									longitude:results.rows.item(i).longitude
								},
								function(data){
									if(data.dist_status=='success'){
										db.transaction(function (tx) {
											tx.executeSql('DELETE FROM SURVEYS WHERE id ='+id);
										});	
									}else{
										//alert('Server error occured while uploading, please try again.')
										$('#dist_status').html(data.dist_status);
										$('#dist_status').slideDown("slow").delay(4000).slideUp("slow");
									}
								}
						);
					}
	 						   
				} 	
						 }, null);
			});
			
		
		} else {
			
		}
		
	}
/*---------------------------------------------------------------------------------------------------------------------------------------*/	
	
/*---------------------------------------------UPLOAD DISTRIBUTION DATA TO SERVER--------------------------------------------------------*/
	
	function upload_dist_data(){
		//save data if all the fields are filled
		if (navigator.onLine) {
			//check for data that was saved when navigator was offline
			db.transaction(function (tx) {
				tx.executeSql('SELECT * FROM DISTRIBUTIONS', [], function (tx, results) {
				var len = results.rows.length;
				//upload data that was saved when the phone/browser was offline
				if (len>0){
					for(var i = 0; i < len; i++){
						var id = results.rows.item(i).id;
						$.post(
								'app.php',
								{
									form:'newdistribution',
									cpa:results.rows.item(i).cpa,
									serial:results.rows.item(i).serial,
									date:results.rows.item(i).date,
									firstname:results.rows.item(i).firstname,
									middlename:results.rows.item(i).middlename,
									householdname:results.rows.item(i).surname,
									gender:results.rows.item(i).gender,
									userid:results.rows.item(i).userid,
									mobile:results.rows.item(i).mobile,
									location:results.rows.item(i).location,
									latitude:results.rows.item(i).lat,
									longitude:results.rows.item(i).long,
									oldstove:results.rows.item(i).oldstove,
									added_by:results.rows.item(i).added_by
									
								},
								function(data){
									if(data.dist_status=='success'){
										db.transaction(function (tx) {
											tx.executeSql('DELETE FROM DISTRIBUTIONS WHERE id ='+id);
										});	
									}else{
										//alert('Server error occured while uploading dist, please try again.')
										$('#dist_status').html(data.dist_status);
										$('#dist_status').slideDown("slow").delay(4000).slideUp("slow");
									}
								}
						);
					}
							   
						   
				} 	
						 }, null);
			});
			
		
		} else {
			
		}
		
	}
/*---------------------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------GET USER'S DATA ----------------------------------------------------------------------------*/
	
	function get_account_data(){
	
		db.transaction(function (tx) {
			  tx.executeSql('SELECT * FROM USERS', [], function (tx, results) {
			   var len = results.rows.length, i;
			   
			   if (len<1){
				   $.mobile.changePage("#login", "slideright");
			   } else{
				   for (i = 0; i < len; i++){
				     //alert(results.rows.item(i).username);
					  /** TODO **/
					   user_id = results.rows.item(i).id;  
				   }
			   }
				 }, null);
		}); 
		db.transaction(function (tx) {
			tx.executeSql('SELECT * FROM DISTRIBUTIONS', [], function (tx, results) {
			var len = results.rows.length;
				$('#Dist_Not_Uploaded').html(len);
			
			}, null);
		});
		
		db.transaction(function (tx) {
			tx.executeSql('SELECT * FROM SURVEYS WHERE surveyid =?', ['1'], function (tx, results) {
			var len = results.rows.length;
				$('#Baseline_Not_Uploaded').html(len);
			
			}, null);
		});
		
		db.transaction(function (tx) {
			tx.executeSql('SELECT * FROM SURVEYS WHERE surveyid=?', ['2'], function (tx, results) {
			var len = results.rows.length;
				$('#Monitoring_Not_Uploaded').html(len);
			
			}, null);
		});
		
		$.post(
				'app.php',
				{
					form:'get_account_data',
					user_id: user_id
				},
				function(data){
					$('#Dist_Uploaded').html(data.distributions_uploaded);
					$('#Baseline_Uploaded').html(data.baseline_uploaded);
					$('#Monitoring_Uploaded').html(data.monitoring_uploaded);
					
				}
			);
	}
/*---------------------------------------------------------------------------------------------------------------------------------------*/

function getSurveys(){
	
	$.getJSON(
			"app.php?callback=?", 
			{a: 'initSurveyForms'},function(result){
				//load content asynchronously into options
				if(typeof(result) !== 'undefined'){
					setSurveys(result);
				}
			}
	);
	
}	

function setSurveys(result){
	//localStorage.setItem('testObject', JSON.stringify(result));
	//var results = localStorage.getItem('testObject');
	surveys = result;
	initSurveyForms(surveys);
}
	

/*----------------------------------------CHANGE PAGE ----------------------------------------------------------*/
	//navigate to screen specified
	//Deprecated :-) Not in use anymore
	
	function changePage(page){
		window.location.href = "http://localhost/climatepal/mobile/#"+page;
	}

});
/********************************************THE END********************************************************************/


/**************************************** Sample code to use later******************************************************/
/* Split URL*/
//var n=document.URL.split('#');
//alert(n[1]);
/* Check of browser is online*/
//if (navigator.onLine) {
//	alert('browser is online');
//} else {
	//alert('browser is offline');
//}
/* Get browser and OS*/
//$('#user_agent').html('<p>'+navigator.userAgent+'</p>');