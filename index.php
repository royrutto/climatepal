<?php
/** 
 * Incharge of interfacing between Client side and Application layer (class.app.php)
 */
//Commented (Kitsao)
ini_set('memory_limit', '256M');
session_start();

ob_start();
//defining constants
define('BASE_URL','http://' . $_SERVER['HTTP_HOST'].'/climatepal/');
define('DIR_WEB', dirname(__FILE__));
define('DIR_SYS', DIR_WEB.'/system');
define('DIR_LIB', DIR_SYS.'/lib');
define('DIR_TEMPLATE', DIR_SYS.'/template');
define('DIR_BUNDLES', DIR_SYS.'/bundles');
define('DIR_EXTENSIONS', DIR_SYS.'/extensions');

date_default_timezone_set('Africa/Nairobi');
define('DATE_FORMAT_DEFAULT', "Y-m-d H:i:s");
define('DATE_DEFAULT', "Y-m-d");
define('DATE_FORMAT_DATEPICKER', "d-m-Y");
define('DATE_FORMAT_DATEPICKER_MONTH', "m-Y");
define('DATE_FORMAT_DATEPICKER_YEAR', "Y");

//load required libraries/models
require_once(DIR_LIB.'/load_models.php');
//redirect to page in url
if (isset($_SESSION['session_id'])){
	
	if(isset($_GET['page'])){
		
		$path = $_GET['page'];
		
		$parts = explode('/', $path);
		$page_id = $parts['0'];
		if (count($parts)>1){
			$action = $parts['1'];
		} else $action ='';
		if (count($parts)>2){
			$item_id = $parts['2'];
		} else $item_id ='';
		if (count($parts)>3){
			$sub_item_id = $parts['3'];
		} else $sub_item_id ='';
		
		switch($page_id){
			
			//HOME PAGE
			case 'home':
			
				$page_title = 'Home';
				$page = DIR_SYS.'/home.php';
				include(DIR_TEMPLATE.'/template.php');
			
			break;
			
			//USERS
			case 'users':
				
				if($action==''){
					_redirect('0','Users', '/users/users.php', $Users, $Users);
					break;
				}else 
				if($action=='add'){
					_redirect('0','Add Users', '/users/add_users.php', $Users,$Users);
					break;
				}else
				if($action=='edit'){
					if (!$item_id==''){
					_redirect_item('0','Edit User', '/users/add_users.php',$item_id, $Users, $Users);
					}else{
						_redirect('0','Users', '/users/users.php', $Users, $Users);
					}
					break;
				}else{
					_redirect('0', 'Users', '/users/users.php', $Users, $Users);
					break;
				}
			break;
			
			//ROLES
			case 'roles':
				
				if($action==''){
					_redirect('0','Roles', '/settings/roles.php', $Role, $Users);
					break;
				}else 
				if($action=='add'){
					_redirect('0','Add Roles', '/settings/add_roles.php', $Role, $Users);
					break;
				}else
				if($action=='edit'){
					if (!$item_id==''){
					_redirect_item('0','Edit Roles', '/settings/add_roles.php',$item_id, $Role, $Users);
					}else{
						_redirect('0','Roles', '/settings/roles.php', $Role, $Users);
					}
					break;
				}else{
					_redirect('0', 'Roles', '/settings/roles.php', $Role, $Users);
					break;
				}
			break;
			//PERMISSIONS
			case 'permissions':
				
				if($action==''){
					_redirect('0','Permissions', '/settings/permissions.php', $Permission, $Users);
					break;
				}else 
				if($action=='add'){
					_redirect('0','Add Permissions', '/settings/add_permissions.php', $Permission, $Users);
					break;
				}else
				if($action=='edit'){
					if (!$item_id==''){
					_redirect_item('0','Edit Permissions', '/settings/add_permissions.php',$item_id, $Permission,$Users);
					}else{
						_redirect('0','Permissions', '/settings/permissions.php', $Permission, $Users);
					}
					break;
				}else{
					_redirect('0', 'Permissions', '/settings/permissions.php', $Permission, $Users);
					break;
				}
			break;
			
			//ROLE PERMISSIONS
			case 'role_permissions':
				
				if($action==''){
					_redirect('0','Role Permissions', '/settings/role_permissions.php', $RolePermission, $Users);
					break;
				}else 
				if($action=='add'){
					_redirect('0','Add Role Permissions', '/settings/add_role_permissions.php', $RolePermission, $Users);
					break;
				}else
				if($action=='edit'){
					if (!$item_id==''){
					_redirect_item('0','Edit Role Permissions', '/settings/add_role_permissions.php',$item_id, $RolePermission, $Users);
					}else{
						_redirect('0','Role Permissions', '/settings/role_permissions.php', $RolePermission, $Users);
					}
					break;
				}else{
					_redirect('0', 'Role Permissions', '/settings/role_permissions.php', $RolePermission,$Users);
					break;
				}
			break;
			
			//GENDERS
			case 'genders':
				
				if($action==''){
					_redirect('0','Genders', '/users/genders.php', $Gender, $Users);
					break;
				}else 
				if($action=='add'){
					_redirect('0','Add Gender', '/users/add_gender.php', $Gender, $Users);
					break;
				}else
				if($action=='edit'){
					_redirect_item('0','Edit Gender', '/users/add_gender.php',$item_id, $Gender, $Users);
					break;
				}else{
					_redirect('0', 'Genders', '/users/genders.php', $Gender, $Users);
					break;
				}
			break;
			//STOVE TYPES
			case 'stovetypes':
				
				if($action==''){
					_redirect('0','Stove Types', '/sales/stoves/stove_types.php', $StoveType, $Users);
					break;
				}else 
				if($action=='add'){
					_redirect('0','Add Stove Types', '/sales/stoves/add_stove_types.php', $StoveType, $Users);
					break;
				}else
				if($action=='edit'){
					_redirect_item('0','Edit Stove Types', '/sales/stoves/add_stove_types.php',$item_id, $StoveType, $Users);
					break;
				}else{
					_redirect('0', 'Stove Types', '/sales/stoves/stove_types.php', $StoveType, $Users);
					break;
				}
			break;
			
			//SALES/DISTRIBUTIONS
			case 'distributions':
			
				if($action==''){
					_redirect('1', 'Distributions', '/sales/sales.php', $Sales, $Users);
					break;
				}else
					if($action=='add'){
					_redirect('1', 'Add Distribution', '/sales/add_sale.php', $Sales, $Users);
					break;
						
				}else
					if($action=='edit'){
					_redirect('1', 'Edit Distribution', '/sales/add_sale.php', $item_id, $Sales, $Users);
					break;
						
				}else
				if($action=='upload-excel'){
					_redirect_item('1','Upload Excel Sheet', '/sales/upload-excel.php',$item_id, $Sales, $Users);
					break;
				}else
				if($action=='upload-excel-list'){
					_redirect_item('1','Uploaded Excel Files', '/sales/list-uploaded-files.php',$item_id, $Sales, $Users);
					break;
				}else{
					_redirect('1', 'Distributions', '/sales/sales.php', $Sales, $Users);
					break;
				}
			break;
			
			//DISTRICTS
			case 'districts':
					
				if($action==''){
					_redirect('1', 'Districts', '/locations/districts.php', $District, $Users);
					break;
				}else
					if($action=='add'){
					_redirect('1', 'Add Districts', '/locations/add_district.php', $District, $Users);
					break;
						
				}else
				if($action=='edit'){
					_redirect_item('0','Edit Districts', '/locations/add_district.php',$item_id, $District, $Users);
					break;
				}else{
					_redirect('1', 'Districts', '/locations/districts.php', $District, $Users);
					break;
				}
			break;
			
			//HOUSEHOLDS
			case 'households':
					
				if($action==''){
					_redirect('1', 'Households', '/sales/households/households.php', $Household, $Users);
					break;
				}else
				if($action=='edit'){
					_redirect_item('0','Edit Households', '/sales/households/add_household.php',$item_id, $Household, $Users);
					break;
				}else{
					_redirect('1', 'Households', '/sales/households/households.php', $Household, $Users);
					break;
				}
			break;
			
			//STOVES
			case 'stoves':
					
				if($action==''){
					_redirect('1', 'Stoves', '/sales/stoves/stoves.php', $Stove, $Users);
					break;
				}else
					if($action=='edit'){
					_redirect('1', 'Edit Stove', '/sales/stoves/add_stove.php',$item_id, $Stove, $Users);
					break;
			
				}else{
					_redirect('1', 'Stoves', '/sales/stoves/stoves.php', $Stove, $Users);
					break;
				}
			break;
			
			//Distribution MAP
			case 'map':
			
					_redirect_survey('1', 'Distribution Map', '/reports/map.php',$action,$item_id, $Users, $Users);
				
						
			break;
			
			//SURVEYS
			case 'surveys':
				if($action==''){
					_redirect('2','Surveys', '/surveys/surveys.php', $Surveys, $Users);
					break;
				}else
					if($action=='add'){
					_redirect('2', 'New Survey', '/surveys/add_survey.php', $Surveys, $Users);
				
					break;
						
				}else
					if($action=='fill'){
					_redirect_survey('2', 'Fill Questionnare', '/surveys/show_questionnaire.php',$action,$item_id, $Surveys, $Users);
				
					break;
						
				}else
					if($action=='viewdata'){
					_redirect_item('2', 'Collected Data', '/surveys/collected_data.php', $item_id ,$Surveys, $Users);
				
					break;
						
				}else
					if($action=='showresponses'){
					_redirect_item('2', 'Question Responses', '/surveys/responses_summary.php', $item_id."/".$sub_item_id ,$Surveys, $Users);
				
					break;
						
				}else
					if($action=='summary'){
					_redirect_item('2', 'Data Collection Summary', '/surveys/summary.php', $item_id ,$Surveys, $Users);
				
					break;
						
				}
				else
				if($action=='questions_summary'){
					_redirect_item('2', 'Survey Summary', '/surveys/questions_summary.php', $item_id ,$Surveys, $Users);
				
					break;
				
				}
				else
				if($action=='users_summary'){
					_redirect_item('2', 'Stove Users Summary', '/surveys/stove_users_summary.php', $item_id ,$Surveys, $Users);
				
					break;
				
				}
				else
					if($action=='download_summary'){
					_redirect_item('2', 'Data Collection Summary', '/surveys/download_summary.php', $item_id ,$Surveys, $Users);
				
					break;
						
				}else
					if($action=='download_question_summary'){
					_redirect_item('2', 'Data Collection Summary', '/surveys/download_survey_summary.php', $item_id ,$Surveys, $Users);
				
					break;
						
				}else
					if($action=='download_stoveuser_summary'){
					_redirect_item('2', 'Data Collection Summary', '/surveys/download_stoveuser_summary.php', $item_id ,$Surveys, $Users);
				
					break;
						
				}else{
					_redirect('2','Surveys', '/surveys/surveys.php', $Surveys, $Users);
					break;
				}
				
					
			break;
			
			//REPORTS
			case 'reports':

			if($action==''){
					_redirect('3','Reports', '/reports/report.php', $Users, $Users);
					break;
				}else
					if($action=='viewreport'){
					_redirect_item('3', 'Collected Data', '/surveys/collected_data.php', $item_id ,$Users, $Users);
				
					break;
						
				}else{
					_redirect('3','Reports', '/reports/report.php', $Users, $Users);
					break;
				}
			break;
			//WHO DISTRIBUTED WHAT
			case 'who':

			if($action==''){
					_redirect('3','Who Distributed What', '/reports/distributed.php', $Users, $Users);
					break;
				}
			break;
			case 'nostove':
			
				if($action==''){
					_redirect('3','Number of Stoves in Use', '/reports/nostove.php', $Users, $Users);
					break;
				}
				break;
				case 'freqstove':
				
					if($action==''){
						_redirect('3','Freq of Stove Use', '/reports/freq.php', $Users, $Users);
						break;
					}
					break;
					case 'satisfaction':
					
						if($action==''){
							_redirect('3','Satisfaction Level', '/reports/degree.php', $Users, $Users);
							break;
						}
						break;
						
						case 'tradstove':
								
							if($action==''){
								_redirect('3','Tradition Stoves', '/reports/traditional.php', $Users, $Users);
								break;
							}
							break;
							case 'damagedstoves':
								
							if($action==''){
								_redirect('3','Damaged Stoves', '/reports/damaged.php', $Users, $Users);
								break;
							}
							break;
			
			//REPORTS BY OLD STOVES
			case 'oldstoves':

			if($action==''){
					_redirect('3','Old Stoves', '/reports/oldstoves.php', $Users, $Users);
					break;
				}else
					if($action=='locations'){
					_redirect_survey('3', 'Distribution Locations', '/reports/locations/locations.php',$action,$item_id, $Users, $Users);
				
					break;
						
				}else
					if($action=='viewreport'){
					_redirect_item('3', 'Collected Data', '/surveys/collected_data.php', $item_id ,$Users, $Users);
				
					break;
						
				}else{
					_redirect('3','Reports', '/reports/report.php', $Users,$Users);
					break;
				}
				
			break;
			
			//REPORTS BY RATE OF DISTRIBUTION
			case 'dist_rate':

			if($action==''){
					_redirect('3','Rate of Distribution', '/reports/distribution.php', $Users,$Users);
					break;
				}else
					if($action=='locations'){
					_redirect_survey('3', 'Distribution Locations', '/reports/locations/locations.php',$action,$item_id, $Users,$Users);
				
					break;
						
				}else
					if($action=='viewreport'){
					_redirect_item('3', 'Collected Data', '/surveys/collected_data.php', $item_id ,$Users, $Users);
				
					break;
						
				}else{
					_redirect('3','Reports', '/reports/report.php', $Users, $Users);
					break;
				}
			break;			
			
			//LOCATIONS
			case 'locations':
					
				if($action==''){
					_redirect('1', 'Locations', '/locations/locations.php', $Location, $Users);
					break;
				}else
					if($action=='add'){
					_redirect('1', 'Add Locations', '/locations/add_location.php', $Location, $Users);
					break;
						
				}else
				if($action=='edit'){
					_redirect_item('0','Edit Locations', '/locations/add_location.php',$item_id, $Location, $Users);
					break;
				}else{
					_redirect('1', 'Locations', '/locations/locations.php', $Location,$Users);
					break;
				}
			break;
			
			//SUB LOCATIONS
			case 'sublocations':
					
				if($action==''){
					_redirect('1', 'Sub Locations', '/locations/sublocations.php', $SubLocation,$Users);
					break;
				}else
					if($action=='add'){
					_redirect('1', 'Add Sub Locations', '/locations/add_sublocation.php', $SubLocation, $Users);
					break;
						
				}else
				if($action=='edit'){
					_redirect_item('0','Edit Sub Locations', '/locations/add_sublocation.php',$item_id, $SubLocation, $Users);
					break;
				}else{
					_redirect('1', 'Sub Locations', '/locations/sublocations.php', $SubLocatio,$Users);
					break;
				}
			break;
						
			//LOGOUT
			case 'logout':
				include(DIR_SYS.'/logout.php');

			break;
				
		}
	
	}else if(isset($_POST['form_name'])){
		//handles form submissions (except login)
		$form_name = $_POST['form_name'];
		
		if ($form_name!='login'){
			include(DIR_SYS.'/form_submission.php');	
		}
		
	}else {
		$page_id = 'home';
		$page_title = 'Home';	
		$page = DIR_SYS.'/home.php';
		include(DIR_TEMPLATE.'/template.php');
	}
}
else
if(isset($_POST['form_name'])){

	$form_name = $_POST['form_name'];

	if ($form_name=='login'){
		$username = $_POST['username'];
		$password = $_POST['password'];
		include(DIR_SYS.'/checklogin.php');
	}
	
}else
{	
	include(DIR_SYS.'/login.php');
}

function redirect($page_id, $page_title, $path, $Users){
	$page = DIR_SYS.$path;
	include(DIR_TEMPLATE.'/template.php');

}
//reirect URL with with model object
function _redirect($page_id, $page_title, $path, $Model, $Users){
	$page = DIR_BUNDLES.$path;
	include(DIR_TEMPLATE.'/template.php');
	
}
//reirect URL with item id
function _redirect_item($page_id, $page_title, $path, $item_id, $Model, $Users){
	$page = DIR_BUNDLES.$path;
	include(DIR_TEMPLATE.'/template.php');

}


//reirect URL with for surveys (with action parameter)
function _redirect_survey($page_id, $page_title, $path, $action, $item_id, $Model, $Users){
	$page = DIR_BUNDLES.$path;
	include(DIR_TEMPLATE.'/template.php');

}
?> 