<?php
//We've included ../Includes/FusionCharts.php and ../Includes/DBConn.php, which contains
//functions to help us easily embed the charts and connect to a database.
include("../FusionCharts/Code/PHP/Includes/FusionCharts.php");
include("DBConn.php");
//echo date(DATE_FORMAT_DATEPICKER);
//echo date(DATE_FORMAT_DATEPICKER_MONTH);
// Connect to the DB
	$link = connectToDB();
?>
<HEAD>

<style type="text/css">
select {
	width: 250;
} 
.chart_container{
	border-left: 2px solid #DDD;
	border-bottom: 2px solid #DDD;
	border-right: 2px solid #DDD;
	padding: 3px;
	margin-bottom:15px;
	width:950px;
	margin-left:auto;
	margin-right:auto
}
 
.xtop{
min-width:1300px;
}
#Larger_Graph{
width:900px;
}
 

</style>
	<TITLE>
	Climate Pal - Reports
	</TITLE>
	<?php
	//You need to include the following JS file, if you intend to embed the chart using JavaScript.
	//Embedding using JavaScripts avoids the "Click to Activate..." issue in Internet Explorer
	//When you make your own charts, make sure that the path to this JS file is correct. Else, you would get JavaScript errors.
	?>	
	<SCRIPT LANGUAGE="Javascript" SRC="../FusionCharts/Code/FusionCharts/FusionCharts.js"></SCRIPT>
    <style type="text/css">
	<!--
	body {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	.text{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	.clear{
		width:25px;
	}
	
	-->
	</style>
</HEAD>
<BODY>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/reports/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">		
    			<div id="actions">

            <div id="actionbutton">
            <form class="form-horizontal"  action="" method="post" name="filtered" id="filtered">
      	<input name="form_name" type="hidden" value="filter_dist_rate">
      	 
      	 <legend>Customize Here</legend>
         <table width="100%" border="0" align="left" style="float:left;">
  	<tr>
    <td style="float:left;"><label class="control-label" for="inputFrom">Select Year:</label>&nbsp;&nbsp;<div class="input-append date" id="yearsOnly">
				<input id="distdate" name="distdate" class="span8" size="16" type="text" value="<?php echo " ".date(DATE_FORMAT_DATEPICKER_YEAR); ?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div></td>
    <td><button type="submit" class="btn btn-primary" name="ok" id="ok"> 
  		  		<i class="icon-filter"></i> Filter
  		  	</button></td>
  </tr>
</table>
</form><div class="clear"></div>

  			
            </div>
           </div>


<CENTER>

<?php
	//In this example, we show how to connect FusionCharts to a database.
	//For the sake of ease, we've used an MySQL databases containing two
	//tables.
		
	// Connect to the DB
	$link = connectToDB();
	if(isset($_GET['year'])){
    $result = $_GET['year'];
	}
	else{
		$result = date('Y');
	}
	//echo $result;
	$sql_freq=mysql_query("SELECT options FROM `quest_fields` WHERE field_id=47;") or die(mysql_error());
	$sql_resultset=mysql_fetch_assoc($sql_freq);
	$sql_valueset=$sql_resultset['options'];
	$string_exploded = explode(",",$sql_valueset);
	
	$monthnames=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	$sql_months = "select distinct MONTH(date_added) as month from quest_field_results WHERE YEAR(date_added) = $result order by month asc;";
	$sql_result_months = mysql_query($sql_months) or die(mysql_error());
	$months = array();
	$counter = 0;
	while($month = mysql_fetch_assoc($sql_result_months)){
		$months[$counter] = $month['month'];
		//echo $monthnames[$months[$counter]-1];
		$counter++;
	}

	//$strXML will be used to store the entire XML document generated
	//Generate the chart element
	$strXML = "<chart caption='Frequency of use of Hifadhi Stoves' subCaption='For the year ".$result."' lineThickness='1' showValues='0' formatNumberScale='0' anchorRadius='2'   divLineAlpha='20' divLineColor='1D8BD1' divLineIsDashed='1' showAlternateHGridColor='1' alternateHGridAlpha='5' alternateHGridColor='1D8BD1' shadowAlpha='40' labelStep='2' numvdivlines='5' chartRightMargin='35' bgColor='FFFFFF,1D8BD1' bgAngle='270' bgAlpha='10,10' xAxisName='Month' yAxisName='Number of Stoves' numberSuffix=' '>";
	$strXML .="<categories>";
	foreach($months as $dist_month){
		$strXML .= "<category label='". $monthnames[$dist_month-1] . "'/>";
	}
	$strXML .="</categories>";
	
	foreach($months as $dist_month){
		for($i=0;$i<count($string_exploded);$i++){
		$strXML .="<dataset seriesName='".$string_exploded[$i]."'  >";
		$sql_get_data = "SELECT COUNT(*) AS total_distributed FROM quest_field_results WHERE field_id=47 AND value='".$string_exploded[$i]."'AND MONTH(date_added) = $dist_month;";
		//echo $sql_get_data;
		$sql_result_get_data = mysql_query($sql_get_data) or die(mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		//echo $distributed;
		$strXML .= "<set value='".$distributed."'/>";
		$strXML .="</dataset>";
		}
			//free the resultset
			//mysql_free_result($get_data_resultset);
			//echo $monthnames[1]."  ".$distributed.'<br />';
	}
	mysql_close($link);

	//Finally, close <chart> element
	$strXML .= "</chart>";
	
	//Create the chart - Pie 3D Chart with data from $strXML
	echo renderChart("../FusionCharts/Charts/MSColumn2D.swf", "", $strXML, "ChartId", "800", "400", "0", "1");
?>
</CENTER>
</div> <!-- End Tab 2-->

  				</div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->
</BODY>
</HTML>