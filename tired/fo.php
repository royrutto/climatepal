<?php
//functions to help us easily embed the charts and connect to a database.
include("../FusionCharts/Code/PHP/Includes/FusionCharts.php");
include("DBConn.php");
$officer = $_GET['officer'];
$year = $_GET['year'];
// Connect to the DB
	$link = connectToDB();
?>
<HEAD>

<style type="text/css">
select {
	width: 250;
} 
.chart_container{
	border-left: 2px solid #DDD;
	border-bottom: 2px solid #DDD;
	border-right: 2px solid #DDD;
	padding: 3px;
	margin-bottom:15px;
	width:950px;
	margin-left:auto;
	margin-right:auto
}
 
.xtop{
min-width:1300px;
}
#Larger_Graph{
width:900px;
}
 

</style>
	<TITLE>
	Climate Pal - Reports
	</TITLE>
	<SCRIPT LANGUAGE="Javascript" SRC="../FusionCharts/Code/FusionCharts/FusionCharts.js"></SCRIPT>
    <style type="text/css">
	<!--
	body {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	.text{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	.clear{
		width:25px;
	}
	
	-->
	</style>
</HEAD>
<BODY>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php include DIR_BUNDLES.'/reports/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">		
    			<div id="actions">

            <div id="actionbutton">
            <form class="form-horizontal"  action="" method="post" name="filtered" id="filtered">
      	<input name="form_name" type="hidden" value="filter_dist_rate">
      	 
      	 <legend>Customize Here</legend>
         <table width="100%" border="0" align="left" style="float:left;">
  	<tr>
    <td style="float:left;"><label class="control-label" for="inputFrom">Select Year:</label>&nbsp;&nbsp;<div class="input-append date" id="yearsOnly">
				<input id="distdate" name="distdate" class="span8" size="16" type="text" value="<?php echo " ".date(DATE_FORMAT_DATEPICKER_YEAR); ?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div></td>
    <td><button type="submit" class="btn btn-primary" name="ok" id="ok"> 
  		  		<i class="icon-filter"></i> Filter
  		  	</button></td>
  </tr>
</table>
</form><div class="clear"></div>

  			
            </div>
           </div>


<CENTER>

<?php
	$monthnames=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	$sql_months = "select distinct MONTH(dist_date) as month from distributions WHERE YEAR(dist_date) = $year order by month asc;";
	$sql_result_months = mysql_query($sql_months) or die(mysql_error());
	$months = array();
	$counter = 0;
	while($month = mysql_fetch_assoc($sql_result_months)){
		$months[$counter] = $month['month'];
		//echo $monthnames[$months[$counter]-1];
		$counter++;
	}
	$fo_query = mysql_query("SELECT users.user_id, employees.surname, employees.firstname FROM users, employees WHERE users.employee_id = employees.employee_id AND users.user_id='".$officer."'") or die("Error ".mysql_error());
	$fo_rs = mysql_fetch_assoc($fo_query);
	$fo_fname = $fo_rs['firstname'];
	$fo_sname = $fo_rs['surname'];
	//$strXML will be used to store the entire XML document generated
	//Generate the chart element
	$strXML = "<chart caption='".$fo_fname." ".$fo_sname." Monthly Distributions Summary' subCaption='for the year ".$year."' lineThickness='1' showValues='0' formatNumberScale='0' anchorRadius='2'   divLineAlpha='20' divLineColor='1D8BD1' divLineIsDashed='1' showAlternateHGridColor='1' alternateHGridAlpha='5' alternateHGridColor='1D8BD1' shadowAlpha='40' labelStep='2' numvdivlines='5' chartRightMargin='35' bgColor='FFFFFF,1D8BD1' bgAngle='270' bgAlpha='10,10' xAxisName='Month' yAxisName='Stove Distributions' numberSuffix=' Stoves'>";

	foreach($months as $dist_month){
		$sql_get_data = "SELECT COUNT(*) AS total_distributed FROM distributions WHERE MONTH(dist_date) = '".$dist_month."' AND added_by='".$officer."';";
		//echo $sql_get_data;
		$sql_result_get_data = mysql_query($sql_get_data) or die(mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		//echo $distributed;
		$strXML .= "<set label='" . $monthnames[$dist_month-1] . "' value='" . $distributed . "' link='fo_weekly.php?officer=".$officer."&month=".$dist_month."&year=".$year."' />";
			//free the resultset
			//mysql_free_result($get_data_resultset);
			//echo $monthnames[1]."  ".$distributed.'<br />';
	}
	mysql_close($link);

	//Finally, close <chart> element
	$strXML .= "</chart>";
	
	//Create the chart - Pie 3D Chart with data from $strXML
	echo renderChart("../FusionCharts/Charts/Column2D.swf", "", $strXML, "ChartId", "800", "400", "0", "1");
?>
</CENTER>
</div> <!-- End Tab 2-->

  				</div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->
</BODY>
</HTML>