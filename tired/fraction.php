<?php
//We've included ../Includes/FusionCharts.php and ../Includes/DBConn.php, which contains
//functions to help us easily embed the charts and connect to a database.
include("../FusionCharts/Code/PHP/Includes/FusionCharts.php");
include("DBConn.php");
$link = connectToDB();

?>
<HTML>
<HEAD>
	<TITLE>
	Climate Pal - Reports
	</TITLE>
	<?php
	//You need to include the following JS file, if you intend to embed the chart using JavaScript.
	//Embedding using JavaScripts avoids the "Click to Activate..." issue in Internet Explorer
	//When you make your own charts, make sure that the path to this JS file is correct. Else, you would get JavaScript errors.
	?>	
	<SCRIPT LANGUAGE="Javascript" SRC="../FusionCharts/Code/FusionCharts/FusionCharts.js"></SCRIPT>
    <style type="text/css">
	<!--
	body {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	.text{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	
	-->
	</style>
</HEAD>
<BODY>
<div class="span9">
      <div class="tabbable"> <!-- Only required for left/right tabs -->
      	<ul class="nav nav-tabs">
	    	<?php //include DIR_BUNDLES.'/reports/tab_menu.php'?>
  		</ul>
  		<div class="tab-content">
    	<div class="tab-pane active" id="tab1">		
    			

<CENTER>
<?php
	$strXML = "<chart caption='Fraction of Households using Traditional Stoves' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix=' Households'>";

	// Fetch all factory records
	$strQuery = "SELECT COUNT(*) as TotOutput FROM households WHERE old_stove IN (SELECT id FROM stove_types WHERE description !=  'Three Stones');";
	$result = mysql_query($strQuery) or die(mysql_error());
	$ors = mysql_fetch_assoc($result);
	
	$strQuery2 = "SELECT COUNT(*) as TotTraditional FROM households WHERE old_stove IN (SELECT id FROM stove_types WHERE description =  'Three Stones');";
	$result2 = mysql_query($strQuery2) or die(mysql_error());
	$ors2 = mysql_fetch_array($result2);
	
	$strXML .= "<set value='" . $ors['TotOutput'] . "' label='Other Stoves' />";
	$strXML .= "<set value='" . $ors2['TotTraditional'] . "' label='Traditional Stoves' />";
    
	mysql_close($link);

	//Finally, close <chart> element
	$strXML .= "</chart>";
	
	//Create the chart - Pie 3D Chart with data from $strXML
	echo renderChart("../FusionCharts/Charts/Doughnut2D.swf", "", $strXML, "ChartId", "800", "400", "0", "0");
?>
</CENTER>
</div> <!-- End Tab 2-->

  				</div> <!-- End Tab Content-->
			 </div> <!--/End, Tabbable-->
</div><!--/span 9-->
</BODY>
</HTML>