<?php 
include("../DBConn.php");
$link = connectToDB();
if(isset($_GET['year'])){
    $result = $_GET['year'];
}
else{
	$result = date('Y');
}

$monthnames=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
$sql_months = "select distinct MONTH(dist_date) as month from distributions WHERE YEAR(dist_date) = $result order by month asc;";
$sql_result_months = mysql_query($sql_months) or die(mysql_error());
$months = array();
$counter = 0;
while($month = mysql_fetch_assoc($sql_result_months)){
	$months[$counter] = $month['month'];
	//echo $monthnames[$months[$counter]-1];
	$counter++;
}
?>

<chart caption='Monthly Distributions Summary' subcaption='for the year <?php echo $result;?>' lineThickness='1' showValues='0' formatNumberScale='0'  divLineAlpha='20' divLineColor='CC3300' divLineIsDashed='1' showAlternateHGridColor='1' alternateHGridAlpha='5' alternateHGridColor='CC3300' shadowAlpha='40'   numvdivlines='5'   bgColor='#ffffff' showBorder='0' bgAngle='270' bgAlpha='10,10' xAxisName='Month' yAxisName='Distributions' numberPrefix='$'>
<?php 
	foreach($months as $dist_month){
		$sql_get_data = "SELECT COUNT(*) AS total_distributed FROM distributions WHERE MONTH(dist_date) = $dist_month;";
		echo $sql_get_data;
		$sql_result_get_data = mysql_query($sql_get_data) or die(mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		//echo $distributed;
		?>
		<set value='<?php echo $distributed;?>'  label='<?php echo $monthnames[$dist_month-1];?>'/>
	<?php }?>
	<styles>                
		<definition>
                         
			<style name='CaptionFont' type='font' size='12'></style>
		</definition>
		<application>
			<apply toObject='CAPTION' styles='CaptionFont' />
			<apply toObject='SUBCAPTION' styles='CaptionFont' />
		</application>
	</styles>

</chart>


