<?php 
include("DBConn.php");
$link = connectToDB();
$monthnames=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
if(!isset($_GET['parameters'])){
	$result = date('Y');
	$sql_months = "SELECT DISTINCT MONTH(dist_date) as month, YEAR(dist_date) as year FROM distributions ORDER BY month ASC;";
	$sql_result_months = mysql_query($sql_months) or die(mysql_error());
	$months = array();
	$years = array();
	$counter = 0;
	while($month = mysql_fetch_assoc($sql_result_months)){
		$months[$counter] = $month['month'];
		$years[$counter] =  $month['year'];
		//echo $monthnames[$months[$counter]-1];
		$counter++;
	}

	//$strXML will be used to store the entire XML document generated
	//Generate the chart element
		?>
	<chart caption='Monthly Distributions Summary' subCaption='For the year <?php echo $result; ?>' lineThickness='1' showValues='0' decimals='0' formatNumberScale='0' xAxisName='Months' yAxisName='Stove Distributions' numberSuffix=' ' useRoundEdges='0' numvdivlines='5'>
    <?php
	$x=0;
	foreach($months as $dist_month){
		$sql_get_data = "SELECT COUNT(*) AS total_distributed FROM distributions WHERE MONTH(dist_date) = $dist_month AND YEAR(dist_date)=$result;";
		//echo $sql_get_data;
		$sql_result_get_data = mysql_query($sql_get_data) or die(mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		//echo $distributed;
		?>
		<set label='<?php echo $monthnames[$dist_month-1] ." ".$years[$x]; ?>' value='<?php echo $distributed; ?>' link = 'dist_rate_monthly?month=<?php echo $dist_month; ?>'/>;
        <?php
			//free the resultset
			//mysql_free_result($get_data_resultset);
			//echo $monthnames[1]."  ".$distributed.'<br />';
			$x++;
	}
	?>
    </chart>
    <?php
}
else{
	$parameters = explode('_',$_GET['parameters']);
	$from = $parameters[0];
	$to = $parameters[1];
	
	$sql_months = "SELECT DISTINCT MONTH(dist_date) as month, YEAR(dist_date) as year FROM distributions WHERE dist_date BETWEEN '".$from."' AND '".$to."';";
	$sql_result_months = mysql_query($sql_months) or die(mysql_error());
	$months = array();
	$years = array();
	$counter = 0;
	while($month = mysql_fetch_assoc($sql_result_months)){
		$months[$counter] = $month['month'];
		$years[$counter] =  $month['year'];
		//echo $monthnames[$months[$counter]-1];
		$counter++;
	}

	//$strXML will be used to store the entire XML document generated
	//Generate the chart element
	?>
	<chart caption='Monthly Distributions Summary' subCaption='From <?php echo $from; ?> To <?php echo $to; ?>' lineThickness='1' showValues='0' formatNumberScale='0' xAxisName='Months' yAxisName='Stove Distributions' numberSuffix=' Stoves' numvdivlines='5' divLineIsDashed='1' divLineAlpha='20'>;
    <?php
	$x=0;
	foreach($months as $dist_month){
		$sql_get_data = "SELECT COUNT(*) AS total_distributed FROM distributions WHERE MONTH(dist_date) = $dist_month AND YEAR(dist_date) = '".$years[$x]."';";
		//echo $sql_get_data;
		$sql_result_get_data = mysql_query($sql_get_data) or die(mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		//echo $distributed;
		?>
		<set label='<?php echo $monthnames[$dist_month-1] ." ".$years[$x]; ?>' value='<?php echo $distributed; ?>' link = 'dist_rate_monthly?month=<?php echo $dist_month; ?>'/>;
        <?php
			//free the resultset
			//mysql_free_result($get_data_resultset);
			//echo $monthnames[1]."  ".$distributed.'<br />';
			$x++;
	}
	?>
    </chart>
    <?php
}
?>