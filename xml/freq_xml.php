<?php 
include("DBConn.php");
$link = connectToDB();
$monthnames=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
$sql_freq=mysql_query("SELECT options FROM `quest_fields` WHERE field_id IN (SELECT field_id FROM quest_fields WHERE field_name='How often are you using the Hifadhi stove?');") or die(mysql_error());
$sql_resultset=mysql_fetch_assoc($sql_freq);
$sql_valueset=$sql_resultset['options'];
$string_exploded = explode(",",$sql_valueset);
	
if(!isset($_GET['parameters'])){
	$result = date('Y');
//echo $year;
	$sql_months = "select distinct MONTH(date_added) as month from quest_field_results WHERE YEAR(date_added) = $result order by month asc;";
	$sql_result_months = mysql_query($sql_months) or die(mysql_error());
	$months = array();
	$counter = 0;
	while($month = mysql_fetch_assoc($sql_result_months)){
		$months[$counter] = $month['month'];
		//echo $monthnames[$months[$counter]-1];
		$counter++;
	}
?>

<chart palette='2' caption='Frequency of use of Hifadhi Stoves' subCaption='For the year <?php echo $result; ?>' lineThickness='1' showValues='0' decimals='0' formatNumberScale='0' xAxisName='Month' yAxisName='Number of Stoves' numberSuffix=' ' useRoundEdges='0' numvdivlines='5'>
<categories >

<?php 
foreach($months as $dist_month){?>
		<category label='<?php echo $monthnames[$dist_month-1]." ".$result; ?>'/>";
        <?php
	}
?>
</categories> <?php
foreach($months as $dist_month){
		for($i=0;$i<count($string_exploded);$i++){?>
		<dataset seriesName='<?php echo $string_exploded[$i]; ?>'>;
        <?php
		$sql_get_data = "SELECT COUNT(*) AS total_distributed FROM quest_field_results WHERE field_id IN (SELECT field_id FROM quest_fields WHERE field_name='How often are you using the Hifadhi stove?') AND value='".$string_exploded[$i]."' AND MONTH(date_added) = $dist_month AND YEAR(date_added) = $result;";
		//echo $sql_get_data;
		$sql_result_get_data = mysql_query($sql_get_data) or die(mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		//echo $distributed; ?>
		<set value='<?php echo $distributed; ?>'/>;
		</dataset>;
        <?php
		}
			//free the resultset
			//mysql_free_result($get_data_resultset);
			//echo $monthnames[1]."  ".$distributed.'<br />';
	}
	$counter++;
}
else{
	$parameters = explode('_',$_GET['parameters']);
	$from = $parameters[0];
	$to = $parameters[1];
	$sql_months = "select distinct MONTH(date_added) as month, YEAR(date_added) as year from quest_field_results WHERE date_added BETWEEN '".$from."' AND '".$to."';";
$sql_result_months = mysql_query($sql_months) or die(mysql_error());
$months = array();
$years = array();
$counter = 0;
while($row = mysql_fetch_assoc($sql_result_months)){
	$months[$counter] = $row['month'];
	$years[$counter] = $row['year'];
	$counter++;
}
?>
<chart palette='2' caption='Frequency of use of Hifadhi Stoves' subCaption='From: <?php echo $from;?> To: <?php echo $to;?>' lineThickness='1' showValues='0' decimals='0' formatNumberScale='0' xAxisName='Month' yAxisName='Number of Stoves' numberSuffix=' ' useRoundEdges='0' numvdivlines='5'>
<categories >

<?php
$x=0; 
foreach($months as $dist_month){?>
		<category label='<?php echo $monthnames[$dist_month-1]." ".$years[$x]; ?>'/>";
        <?php
		$x++;
	}
?>
</categories> <?php
$y=0;
foreach($months as $dist_month){
		for($i=0;$i<count($string_exploded);$i++){?>
		<dataset seriesName='<?php echo $string_exploded[$i]; ?>'>;
        <?php
		$sql_get_data = "SELECT COUNT(*) AS total_distributed FROM quest_field_results WHERE field_id IN (SELECT field_id FROM quest_fields WHERE field_name='How often are you using the Hifadhi stove?') AND value='".$string_exploded[$i]."' AND MONTH(date_added) = $dist_month AND YEAR(date_added) = '".$years[$y]."';";
		//echo $sql_get_data;
		$sql_result_get_data = mysql_query($sql_get_data) or die(mysql_error());
		$get_data_resultset = mysql_fetch_assoc($sql_result_get_data);
		$distributed = $get_data_resultset['total_distributed']; 
		//echo $distributed; ?>
		<set value='<?php echo $distributed; ?>'/>;
		</dataset>;
        <?php
		}
		$y++;
			//free the resultset
			//mysql_free_result($get_data_resultset);
			//echo $monthnames[1]."  ".$distributed.'<br />';
	}
	$counter++;
}

?>
	<styles>                
		<definition>
                         
			<style name='CaptionFont' type='font' size='12' />
		</definition>
		<application>

			<apply toObject='CAPTION' styles='CaptionFont' />
			<apply toObject='SUBCAPTION' styles='CaptionFont' />
		</application>
	</styles>

</chart>
