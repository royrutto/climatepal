<?php 
include("DBConn.php");
$link = connectToDB();
if(!isset($_GET['parameters'])){
	$year = date('Y');
	?>
	<chart caption='Fraction of Households using Traditional Stoves' subCaption='For the Year <?php echo $year; ?>' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix=' Households'>;
    <?php

	// Fetch all factory records
	$strQuery = "SELECT COUNT(*) as TotOutput FROM households WHERE old_stove IN (SELECT id FROM stove_types WHERE description !=  'Three Stones') AND YEAR(date_added)='".$year."';";
	$result = mysql_query($strQuery) or die(mysql_error());
	$ors = mysql_fetch_assoc($result);
	
	$strQuery2 = "SELECT COUNT(*) as TotTraditional FROM households WHERE old_stove IN (SELECT id FROM stove_types WHERE description =  'Three Stones') AND YEAR(date_added)='".$year."';";
	$result2 = mysql_query($strQuery2) or die(mysql_error());
	$ors2 = mysql_fetch_array($result2);
	?>	
	<set value='<?php echo $ors['TotOutput']; ?>' label='Other Stoves' />";
	<set value='<?php echo $ors2['TotTraditional']; ?>' label='Traditional Stoves' />";
    <?php
	//mysql_close($link);

	//Finally, close <chart> element
	?>
	</chart>;
    <?php
}
else{
	$parameters = explode('_',$_GET['parameters']);
	$from = $parameters[0];
	$to = $parameters[1];
	?>
    <chart caption='Fraction of Households using Traditional Stoves' subCaption='From: <?php echo $from; ?> To: <?php echo $to; ?>' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix=' Households'>;
    <?php

	// Fetch all factory records
	$strQuery = "SELECT COUNT(*) as TotOutput FROM households,distributions WHERE households.old_stove IN (SELECT id FROM stove_types WHERE description !=  'Three Stones') AND households.id=distributions.household AND distributions.dist_date BETWEEN '".$from."' AND '".$to."';";
	$result = mysql_query($strQuery) or die(mysql_error());
	$ors = mysql_fetch_assoc($result);
	
	$strQuery2 = "SELECT COUNT(*) as TotTraditional FROM households,distributions WHERE households.old_stove IN (SELECT id FROM stove_types WHERE description =  'Three Stones') AND households.id=distributions.household AND distributions.dist_date BETWEEN '".$from."' AND '".$to."';";
	$result2 = mysql_query($strQuery2) or die(mysql_error());
	$ors2 = mysql_fetch_array($result2);
	?>	
	<set value='<?php echo $ors['TotOutput']; ?>' label='Other Stoves' />";
	<set value='<?php echo $ors2['TotTraditional']; ?>' label='Traditional Stoves' />";
    <?php
	//mysql_close($link);

	//Finally, close <chart> element
	?>
	</chart>;
    <?php
}
?>