
//validation of Stove Serial number
$('.serial').val('CP');
$('.serial').bind('input propertychange', function() {
    if($(this).val().length<=2){
    	$('.serial').val('CP');
    }
});

$('.serial').bind("keypress", function(event) { 
    var charCode = event.which;
    var current_string = $(this).val();
    var keyChar = String.fromCharCode(charCode); 
   
    if (charCode <= 13) return true; 

    var keyChar = String.fromCharCode(charCode); 
   
    if(!(current_string.length>=8)){
        if(current_string.length>=2 && current_string.length<7){
            return /[0-9]/.test(keyChar);
    	}else if (current_string.length>=7){
    		return /[A-Z]/.test(keyChar);
        }else{
    	    return false;
    	}
    }else return false;
    	   
});

$('.mobile_number').bind("keypress", function(event) { 
	//$('.mobile_number').popover('hide');
    var charCode = event.which;
    var keyChar = String.fromCharCode(charCode); 
   
    if (charCode <= 13) return true;
    if(/[a-zA-Z]/.test(keyChar)) $('.mobile_number').popover('show');
    if(/[0-9]/.test(keyChar)) $('.mobile_number').popover('hide');
 
    return /[0-9]/.test(keyChar);
 
});

$('.user_id').bind("keypress", function(event) { 
    var charCode = event.which;
    var keyChar = String.fromCharCode(charCode); 
   
    if (charCode <= 13) return true;  
    
    
    if(/[a-zA-Z]/.test(keyChar)) $('.user_id').popover('show');
    if(/[0-9]/.test(keyChar)) $('.user_id').popover('hide');
 
    return /[0-9]/.test(keyChar);

});

$('.firstname').bind("keypress", function(event) { 
    var charCode = event.which;
    var keyChar = String.fromCharCode(charCode); 
   
    if (charCode <= 13) return true;  
    
    
    if(/[0-9]/.test(keyChar)) $('.firstname').popover('show');
    if(/[a-zA-Z]/.test(keyChar)) $('.firstname').popover('hide');
 
    return /[a-zA-Z]/.test(keyChar);

});

$('.surname').bind("keypress", function(event) { 
    var charCode = event.which;
    var keyChar = String.fromCharCode(charCode); 
   
    if (charCode <= 13) return true;  
    
    
    if(/[0-9]/.test(keyChar)) $('.surname').popover('show');
    if(/[a-zA-Z]/.test(keyChar)) $('.surname').popover('hide');
 
    return /[a-zA-Z]/.test(keyChar);

});


