-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: climatepal
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_log`
--

DROP TABLE IF EXISTS `access_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_log` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(50) NOT NULL,
  `account` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_log`
--

LOCK TABLES `access_log` WRITE;
/*!40000 ALTER TABLE `access_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distributions`
--

DROP TABLE IF EXISTS `distributions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distributions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpa_number` varchar(50) NOT NULL,
  `household` int(11) NOT NULL,
  `stove_serial` varchar(10) NOT NULL,
  `dist_date` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_distributions_1_idx` (`added_by`),
  KEY `fk_distributions_2_idx` (`household`),
  CONSTRAINT `fk_distributions_1` FOREIGN KEY (`added_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_distributions_2` FOREIGN KEY (`household`) REFERENCES `households` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distributions`
--

LOCK TABLES `distributions` WRITE;
/*!40000 ALTER TABLE `distributions` DISABLE KEYS */;
INSERT INTO `distributions` VALUES (1,'1',1,'CP34334F','2013-04-26',1,1,'2013-04-26 12:17:49','2013-04-26 12:17:49');
/*!40000 ALTER TABLE `distributions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `districts`
--

DROP TABLE IF EXISTS `districts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `district` varchar(50) NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `districts`
--

LOCK TABLES `districts` WRITE;
/*!40000 ALTER TABLE `districts` DISABLE KEYS */;
INSERT INTO `districts` VALUES (1,'Kilaguni','-1.292066','36.821946','2013-02-21 13:41:25','2013-02-21 13:46:33',1,1),(2,'Tharaka','-1.292066','36.82194','2013-02-21 13:51:09','2013-02-21 14:08:33',1,1);
/*!40000 ALTER TABLE `districts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `gender` int(11) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `postal_address` varchar(50) NOT NULL,
  `postal_code` varchar(50) NOT NULL,
  `town` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` varchar(50) NOT NULL,
  `date_updated` varchar(50) NOT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `fk_employees_1_idx` (`added_by`),
  CONSTRAINT `fk_employees_1` FOREIGN KEY (`added_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (1,'Admin','Admin',1,'0721574146','P.O BOX 123456','00200','0722123456','rrutto@strathmore.edu',1,'2013-02-07','2013-02-20 11:10:57');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(10) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gender`
--

LOCK TABLES `gender` WRITE;
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` VALUES (1,'Male','2013-02-18 13:33:10','2013-02-18 13:33:10',1,0),(2,'Female','2013-02-18 14:00:27','2013-02-18 14:00:27',1,0);
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `households`
--

DROP TABLE IF EXISTS `households`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `households` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `gender` int(11) NOT NULL,
  `id_number` varchar(25) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `location` int(11) NOT NULL,
  `household` varchar(100) NOT NULL,
  `old_stove` int(11) NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_households_1_idx` (`location`),
  KEY `fk_households_2_idx` (`old_stove`),
  CONSTRAINT `fk_households_1` FOREIGN KEY (`location`) REFERENCES `sublocations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_households_2` FOREIGN KEY (`old_stove`) REFERENCES `stove_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `households`
--

LOCK TABLES `households` WRITE;
/*!40000 ALTER TABLE `households` DISABLE KEYS */;
INSERT INTO `households` VALUES (1,'Mary',2,'232323','0721123456',1,'Wanjiku',1,'-1.292066','36.821946','2013-04-26 12:17:49','2013-04-26 12:17:49',1,1);
/*!40000 ALTER TABLE `households` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_permissions`
--

DROP TABLE IF EXISTS `location_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_permissions` (
  `employee_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  KEY `fk_location_permissions_1_idx` (`employee_id`),
  KEY `fk_location_permissions_2_idx` (`location_id`),
  CONSTRAINT `fk_location_permissions_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_location_permissions_2` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_permissions`
--

LOCK TABLES `location_permissions` WRITE;
/*!40000 ALTER TABLE `location_permissions` DISABLE KEYS */;
INSERT INTO `location_permissions` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11);
/*!40000 ALTER TABLE `location_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(50) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'Kyeni East','2013-02-25 10:02:36','2013-02-25 10:02:36',1,1),(2,'Kyeni South','2013-02-25 10:02:48','2013-02-25 10:02:48',1,1),(3,'Kyeni N. West','2013-02-25 10:03:01','2013-02-25 10:03:01',1,1),(4,'Kyeni N. East','2013-02-25 10:03:15','2013-02-25 10:03:15',1,1),(5,'Kyeni Central','2013-02-25 10:03:34','2013-02-25 10:03:34',1,1),(6,'Runyenjes West','2013-02-25 10:03:49','2013-02-25 10:03:49',1,1),(7,'Kagaari East','2013-02-25 10:04:00','2013-02-25 10:04:00',1,1),(8,'Runyenjes East','2013-02-25 10:04:11','2013-02-25 10:04:11',1,1),(9,'Kagaari N.East','2013-02-25 10:05:29','2013-02-25 10:05:29',1,1),(10,'Kagaari S.West','2013-02-25 10:05:40','2013-02-25 10:05:40',1,1),(11,'Kagaari N.West','2013-02-25 10:05:53','2013-02-25 10:05:53',1,1);
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(25) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'Create Surveys','2013-02-20 14:32:14','2013-02-20 14:36:43',1,1),(2,'Add Distribution','2013-02-20 14:32:31','2013-02-20 14:32:31',1,0),(3,'test','2013-03-25 18:08:49','2013-03-25 18:08:49',0,0);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_field_groups`
--

DROP TABLE IF EXISTS `quest_field_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_field_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(25) NOT NULL,
  `description` varchar(25) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_field_groups`
--

LOCK TABLES `quest_field_groups` WRITE;
/*!40000 ALTER TABLE `quest_field_groups` DISABLE KEYS */;
INSERT INTO `quest_field_groups` VALUES (1,'User Details',''),(2,'Family Situation',''),(3,'Cook Stove Information',''),(4,'Fuel Wood',''),(5,'Other',''),(6,'Tree Planting','');
/*!40000 ALTER TABLE `quest_field_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_field_responses`
--

DROP TABLE IF EXISTS `quest_field_responses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_field_responses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_quest_field_responses_1_idx` (`user_id`),
  KEY `fk_quest_field_responses_2_idx` (`survey_id`),
  CONSTRAINT `fk_quest_field_responses_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_quest_field_responses_2` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`survey_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_field_responses`
--

LOCK TABLES `quest_field_responses` WRITE;
/*!40000 ALTER TABLE `quest_field_responses` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_field_responses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_field_results`
--

DROP TABLE IF EXISTS `quest_field_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_field_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `data_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_quest_field_results_1_idx` (`response_id`),
  KEY `fk_quest_field_results_2_idx` (`field_id`),
  CONSTRAINT `fk_quest_field_results_1` FOREIGN KEY (`response_id`) REFERENCES `quest_field_responses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_quest_field_results_2` FOREIGN KEY (`field_id`) REFERENCES `quest_fields` (`field_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_field_results`
--

LOCK TABLES `quest_field_results` WRITE;
/*!40000 ALTER TABLE `quest_field_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_field_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_fields`
--

DROP TABLE IF EXISTS `quest_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_fields` (
  `field_id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `field_name` varchar(1000) NOT NULL,
  `field_type` varchar(25) NOT NULL,
  `field_size` varchar(25) NOT NULL,
  `required` bit(1) NOT NULL,
  `options` varchar(1000) DEFAULT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `fk_quest_fields_1_idx` (`questionnaire_id`),
  KEY `fk_quest_fields_2_idx` (`group_id`),
  CONSTRAINT `fk_quest_fields_1` FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaires` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_quest_fields_2` FOREIGN KEY (`group_id`) REFERENCES `quest_field_groups` (`group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_fields`
--

LOCK TABLES `quest_fields` WRITE;
/*!40000 ALTER TABLE `quest_fields` DISABLE KEYS */;
INSERT INTO `quest_fields` VALUES (1,1,1,'User ID','text','20','',NULL,'',''),(2,1,1,'User name','text','20','\0',NULL,'',''),(3,1,1,'Position in Family','choicegroup','20','\0','Father,Mother,Child,Other','',''),(4,1,1,'Sub-Location','text','20','',NULL,'',''),(5,1,1,'Coordinates','text','50','',NULL,'',''),(6,1,2,'Number of Family Members ','choicegroup','20','\0','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20','',''),(7,1,2,'Type of House','choicegroup','20','\0','Permanent,Semi-Permanent','',''),(8,1,3,'Type of Stove in use?','choicegroup','20','\0','Three-stones,Charcoal stove,Gas,Biofuel,Woodstove,Other','',''),(9,1,3,'Do you  experience much smoke in the kitchen while using the stove?','choicegroup','20','\0','Yes,No','',''),(10,1,3,'For how long have you used the current stove?','text','20','\0',NULL,'',''),(11,1,3,'How many times do you use to cook per day?','text','20','\0',NULL,'',''),(12,1,3,'How many hours do you use to cook per day?','text','20','\0',NULL,'',''),(13,1,3,'Where is your primary cooking location?','text','20','\0','','',''),(14,1,3,'Would you be interested in getting a more efficient stove?','choicegroup','20','\0','Yes,No','',''),(15,1,3,'If so, would you discontinue use of your current stove?','choicegroup','20','\0',NULL,'',''),(16,1,4,'Where do you acquire fuelwood for cooking?','choicegroup','20','\0','Forest,Own land,I buy it','',''),(17,1,4,'What distance you travel to collect fuelwood?','text','20','\0',NULL,'',''),(18,1,4,'If you buy, how much do you spend on firewood per week?','text','20','\0',NULL,'',''),(19,1,4,'Where do you buy the wood from?','text','20','\0',NULL,'',''),(20,1,4,'How often do you collect fuelwood? (times per week)','text','20','\0',NULL,'',''),(21,1,4,'What quantity of fuelwood you use per day? (KG/KSH)','text','20','\0',NULL,'',''),(22,1,4,'State any complain or challenge while using the stove','text','20','\0',NULL,'',''),(23,1,5,'What energy source do you use for lighting?','choicegroup','20','\0','Kerosene lamp,Pressure lamp,Electricity,Solar,Other','',''),(24,1,5,'What size of land is comprised in the household? (ha)','text','20','\0',NULL,'',''),(25,1,6,'Would you be interested to participate in a tree planting project?','choicegroup','20','\0','Yes,No','',''),(26,1,6,'What is your preferred species of fuelwood?','text','20','\0',NULL,'',''),(27,1,6,'And the Second?','text','20','\0',NULL,'',''),(28,1,6,'What tree species you would like to receive?','text','20','\0',NULL,'',''),(29,1,6,'Have you ever been trained on tree planting before Hifadhi-Livelihoods project?','choicegroup','20','\0','Yes,No','',''),(30,2,1,'User ID','text','20','\0',NULL,'',''),(31,2,1,'Hifadhi Serial Number','text','20','\0',NULL,'',''),(32,2,1,'Position in the family','choicegroup','20','\0','Father, Mother,Child,Other','',''),(33,2,1,'Location','text','20','\0','','',''),(34,2,1,'Coordinates','text','20','\0','','',''),(35,2,2,'Number of family members on household','choicegroup','20','\0','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20','',''),(36,2,2,'Type of house','choicegroup','20','\0','Permanent, Semi-Permanent','',''),(37,2,3,'Previous (traditional) stove used','choicegroup','20','\0','Three-stones,Charcoal stove,Gas,Biofuel, woodstove, other','',''),(38,2,3,'Are you still using the traditional three-stones stove?','choicegroup','20','\0','Yes,No','',''),(39,2,3,'If yes','radiogroup','20','\0','Use of low efficient stoves on special occasion only, Increase in household size that justifies the use of a second three-stones stove because the Hifadhi stove is being fully utilized, Transfer of the  ownership (e.g. sales or gifts) with sufficient evidence by the original owner that the Hifadhi stove is still in use,Other reason','',''),(40,2,3,'Are you still using the Hifadhi stove?','choicegroup','20','\0','Yes,No','',''),(41,2,3,'For how long have you used the Hifadhi stove? (Years)','choicegroup','20','\0','1,2,3,4,5,6,7,8,9,10','',''),(42,2,3,'Is there improvement in your kitchen environment and reduction of fuel consumption since?','choicegroup','20','\0','Yes,No','',''),(43,2,3,'Is your stove damaged?','choicegroup','20','\0','Yes,No','',''),(44,2,3,'Is there reduction in the number of hours used to cook since?','choicegroup','20','\0','Yes,No','',''),(45,2,3,'Where is your primary cooking location after acquiring after aquiring the stove?','choicegroup','20','\0','Inside the main house,Outside,Separate enclosed kitchen','',''),(46,2,3,'Degree of satisfaction with new Hifadhi stove?','choicegroup','20','\0','1,2,3,4,5,6,7,8,9,10','',''),(47,2,3,'Why?what do you like about it?','text','20','\0',NULL,'',''),(48,2,3,'How often are you using the Hifadhi stove?','choicegroup','20','\0','Every day,Once a week,Once a month,Never','',''),(49,2,4,'Where do you acquire fuelwood for cooking?','choicegroup','20','\0','Forest,Own land,I buy it','',''),(50,2,4,'What distance you travel to collect fuelwood? (Km)','text','20','\0','','',''),(51,2,4,'How often do you collect fuelwood? (times per week)','text','20','\0','','',''),(52,2,4,'What quantity of fuelwood you use per day? (KG/KSH)','text','20','\0','','',''),(53,2,4,'State any complaint or challenge while using the stove.','text','20','\0','','',''),(54,2,5,'Do you experience much smoke in the kitchen using the Hifadhi stove?','choicegroup','20','\0','Yes,No','',''),(55,2,5,'Do you feel this has improved since you use the Hifadhi Stove?','choicegroup','20','\0','Yes,No','',''),(56,2,5,'What energy source do you use for lighting?','choicegroup','20','\0','Kerosene lamp,Pressure lamp,Electricity,Solar,Other','',''),(57,2,5,'What size of land is comprised in the household? (ha)','text','20','\0',NULL,'',''),(58,2,6,'What is your preferred species of fuelwood?','text','20','\0',NULL,'',''),(59,2,6,'And the second?','text','20','\0','','',''),(60,2,6,'What tree species you would like to receive?','text','20','\0',NULL,'',''),(61,2,6,'Have you ever been trained on tree planting before Hifadhi-Livelihoods project?','choicegroup','20','\0','Yes,No','',''),(62,2,6,'Did you receive any seedlings since the beginning of the project?','choicegroup','20','\0','Yes,No','',''),(63,2,6,'If yes, how many?','choicegroup','20','\0','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40','',''),(64,2,6,'How many seedlings are still growing?','choicegroup','20','\0','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40','',''),(65,2,6,'How satisfied are you with the tree planting project?','choicegroup','20','\0','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20','',''),(66,2,6,'Why?','text','20','\0','','','');
/*!40000 ALTER TABLE `quest_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaires`
--

DROP TABLE IF EXISTS `questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `account` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_questionnaires_1_idx` (`survey_id`),
  CONSTRAINT `fk_questionnaires_1` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`survey_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaires`
--

LOCK TABLES `questionnaires` WRITE;
/*!40000 ALTER TABLE `questionnaires` DISABLE KEYS */;
INSERT INTO `questionnaires` VALUES (1,1,'Questionnaire for baseline survey',0,'',''),(2,2,'Questionnaire for monitoring survey',0,'','');
/*!40000 ALTER TABLE `questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permissions`
--

DROP TABLE IF EXISTS `role_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` int(11) NOT NULL,
  `permission` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_role_permissions_1_idx` (`role`),
  KEY `fk_role_permissions_2_idx` (`permission`),
  CONSTRAINT `fk_role_permissions_1` FOREIGN KEY (`role`) REFERENCES `roles` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_permissions_2` FOREIGN KEY (`permission`) REFERENCES `permissions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permissions`
--

LOCK TABLES `role_permissions` WRITE;
/*!40000 ALTER TABLE `role_permissions` DISABLE KEYS */;
INSERT INTO `role_permissions` VALUES (1,1,1,'2013-02-21 11:13:55','2013-02-21 11:14:03',1,1);
/*!40000 ALTER TABLE `role_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(25) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrator','Controls the whole system',1,'2013-02-07','',0,0),(2,'Donor','Views Reports',1,'2013-02-20 14:07:49','2013-02-20 14:15:11',1,1),(3,'Field Officer','Ground Work',1,'2013-03-18 15:11:50','2013-03-18 15:11:50',1,1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_active_date` datetime NOT NULL,
  `is_expired` bit(1) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `computer_name` varchar(50) DEFAULT NULL,
  `Device` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sessions_1_idx` (`user_id`),
  CONSTRAINT `fk_sessions_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,'fMi5onXjkdU2dtMCL2Ku',1,'2013-04-23 22:42:48','2013-04-23 22:42:48','\0','127.0.0.1',NULL,NULL),(2,'9aPfoSGtNMfqhaxxRl3i',1,'2013-04-26 12:16:15','2013-04-26 12:16:15','\0','127.0.0.1',NULL,NULL);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stove_types`
--

DROP TABLE IF EXISTS `stove_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stove_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stove_types`
--

LOCK TABLES `stove_types` WRITE;
/*!40000 ALTER TABLE `stove_types` DISABLE KEYS */;
INSERT INTO `stove_types` VALUES (1,'Three Stones','2013-02-14 16:19:55','2013-02-20 12:40:58',1,1),(2,'Liquified Petroleum Gas','2013-02-18 15:13:02','2013-02-18 15:13:02',1,0),(3,'Charcoal Stove','2013-02-25 14:54:49','2013-02-25 14:54:49',1,0),(4,'Biofuel','2013-02-25 14:55:02','2013-02-25 14:55:02',1,0),(5,'Wood Stove','2013-02-25 14:55:09','2013-02-25 14:55:09',1,0),(6,'Other','2013-02-25 14:55:15','2013-02-25 14:55:15',1,0);
/*!40000 ALTER TABLE `stove_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stoves`
--

DROP TABLE IF EXISTS `stoves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stoves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_number` varchar(50) NOT NULL,
  `stove_type` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stoves`
--

LOCK TABLES `stoves` WRITE;
/*!40000 ALTER TABLE `stoves` DISABLE KEYS */;
/*!40000 ALTER TABLE `stoves` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sublocations`
--

DROP TABLE IF EXISTS `sublocations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sublocations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sublocation` varchar(50) NOT NULL,
  `location` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sublocations_1_idx` (`location`),
  CONSTRAINT `fk_sublocations_1` FOREIGN KEY (`location`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sublocations`
--

LOCK TABLES `sublocations` WRITE;
/*!40000 ALTER TABLE `sublocations` DISABLE KEYS */;
INSERT INTO `sublocations` VALUES (1,'Kigumo',1,'2013-02-25 10:08:26','2013-02-25 10:08:26',1,1),(2,'Mukuria',1,'2013-02-25 10:08:44','2013-02-25 10:08:44',1,1),(3,'Kasafari',2,'2013-02-25 10:09:05','2013-02-25 10:09:05',1,1),(4,'Kirurumo',2,'2013-02-25 10:09:19','2013-02-25 10:09:19',1,1),(5,'Kathunguri',2,'2013-02-25 10:09:33','2013-02-25 10:09:33',1,1),(6,'Kariru',2,'2013-02-25 10:09:44','2013-02-25 10:09:44',1,1),(7,'Mufu',3,'2013-02-25 10:09:58','2013-02-25 10:09:58',1,1),(8,'Rukuriri',3,'2013-02-25 10:10:11','2013-02-25 10:10:11',1,1),(9,'Njeruri',3,'2013-02-25 10:10:25','2013-02-25 10:10:25',1,1),(10,'Kathari',4,'2013-02-25 10:10:39','2013-02-25 10:10:39',1,1),(11,'Kianglingi',4,'2013-02-25 10:10:52','2013-02-25 10:10:52',1,1),(12,'Iriari',4,'2013-02-25 10:11:03','2013-02-25 10:11:03',1,1),(13,'Kathanjuri',5,'2013-02-25 10:11:24','2013-02-25 10:11:24',1,1),(14,'Nyagari',5,'2013-02-25 10:11:38','2013-02-25 10:11:38',1,1),(15,'Gikuuri',6,'2013-02-25 10:13:21','2013-02-25 10:13:21',1,1),(16,'Mbiruri',6,'2013-02-25 10:13:34','2013-02-25 10:13:34',1,1),(17,'Gitare',6,'2013-02-25 10:13:47','2013-02-25 10:13:47',1,1),(18,'Kiringa',7,'2013-02-25 10:14:03','2013-02-25 10:14:03',1,1),(19,'Gichera',7,'2013-02-25 10:14:17','2013-02-25 10:14:17',1,1),(20,'Gichiche',8,'2013-02-25 10:14:37','2013-02-25 10:14:37',1,1),(21,'Kigaa',8,'2013-02-25 10:14:55','2013-02-25 10:14:55',1,1),(22,'Kanja North',9,'2013-02-25 10:15:15','2013-02-25 10:15:15',1,1),(23,'Kanja South',9,'2013-02-25 10:15:28','2013-02-25 10:15:28',1,1),(24,'Mukuuri',9,'2013-02-25 10:15:43','2013-02-25 10:15:43',1,1),(25,'Nduuri',9,'2013-02-25 10:15:57','2013-02-25 10:15:57',1,1),(26,'Kawanjara',10,'2013-02-25 10:16:15','2013-02-25 10:16:15',1,1),(27,'Nthagayia',10,'2013-02-25 10:16:30','2013-02-25 10:16:30',1,1),(28,'Maranga',10,'2013-02-25 10:16:44','2013-02-25 10:16:44',1,1),(29,'Mugui',11,'2013-02-25 10:17:01','2013-02-25 10:17:01',1,1),(30,'Kianjokoma',11,'2013-02-25 10:17:13','2013-02-25 10:17:52',1,1),(31,'Mbuinjeru',11,'2013-02-25 10:17:27','2013-02-25 10:17:27',1,1),(32,'Kamugere',11,'2013-02-25 10:17:40','2013-02-25 10:17:40',1,1);
/*!40000 ALTER TABLE `sublocations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `survey_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `account` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`survey_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveys`
--

LOCK TABLES `surveys` WRITE;
/*!40000 ALTER TABLE `surveys` DISABLE KEYS */;
INSERT INTO `surveys` VALUES (1,'Baseline Survey',1,'',''),(2,'Monitoring Survey',1,'','');
/*!40000 ALTER TABLE `surveys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_users_1_idx` (`role_id`),
  KEY `fk_users_2_idx` (`employee_id`),
  CONSTRAINT `fk_users_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_2` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,1,'admin','$2a$08$PdQNC.mPWBaQRCxO0Pf98OT3WkiO/GCQuPZ1YVVXidPb9YuvTw6vm',1,'','2013-02-20 11:10:57');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-04-29 14:31:56
