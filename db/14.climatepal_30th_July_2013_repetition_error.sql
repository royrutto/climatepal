ALTER TABLE `quest_field_responses` ADD COLUMN `deleted` VARCHAR(45) NULL DEFAULT 1  AFTER `longitude` ;
UPDATE quest_field_responses 
SET
deleted = 0 
WHERE id
IN (SELECT response_id 
	FROM quest_field_results 
	WHERE field_id =1 
	GROUP BY (value)
	);