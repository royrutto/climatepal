SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
--
-- Table structure for table `quest_fields`
--

DROP TABLE `quest_fields`;
CREATE TABLE IF NOT EXISTS `quest_fields` (
  `field_id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `field_name` varchar(1000) NOT NULL,
  `field_type` varchar(25) NOT NULL,
  `field_size` varchar(25) NOT NULL,
  `required` bit(1) NOT NULL,
  `options` varchar(1000) DEFAULT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`field_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `quest_fields`
--

INSERT INTO `quest_fields` (`field_id`, `questionnaire_id`, `group_id`, `field_name`, `field_type`, `field_size`, `required`, `options`, `date_added`, `date_updated`) VALUES
(1, 1, 1, 'User ID', 'text', '20', b'1', NULL, '', ''),
(2, 1, 1, 'User name', 'text', '20', b'0', NULL, '', ''),
(3, 1, 1, 'Position in Family', 'choicegroup', '20', b'0', 'Father,Mother,Child,Other', '', ''),
(4, 1, 1, 'Sub-Location', 'text', '20', b'1', NULL, '', ''),
(5, 1, 2, 'Number of Family Members ', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '', ''),
(6, 1, 2, 'Person in charge of cooking in the use', 'choicegroup', '20', b'0', 'Mother,Father,Kid,Grandmother,Grandfather', '', ''),
(7, 1, 2, 'Type of House', 'choicegroup', '20', b'0', 'Permanent,Semi-Permanent', '', ''),
(8, 1, 2, 'Is the stove used for private or commercial use?', 'choicegroup', '20', b'0','private,commercial,both', '', ''),
(9, 1, 2, 'For how many people do you cook in the family?', 'text', '20', b'0', NULL, '', ''),
(10, 1, 3, 'Type of Stove in use?', 'choicegroup', '20', b'0', 'Three-stones,Charcoal stove,Gas,Biofuel,Woodstove,Other', '', ''),
(11, 1, 3, 'Do you  experience much smoke in the kitchen while using the stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(12, 1, 3, 'For how long have you used the most used current stove?', 'text', '20', b'0', NULL, '', ''),
(13, 1, 3, 'How many times do you use to cook per day on the most used stove?', 'text', '20', b'0', NULL, '', ''),
(14, 1, 3, 'How many hours do you use to cook per day?', 'text', '20', b'0', NULL, '', ''),
(15, 1, 3, 'Where is your primary cooking location?', 'choicegroup', '20', b'0', 'Inside the main house,Outside,Separate enclosed kitchen', '', ''),
(16, 1, 3, 'Would you be interested in getting a more efficient stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(17, 1, 3, 'If so, would you discontinue use of your current stove?', 'choicegroup', '20', b'0','Yes,No', '', ''),
(18, 1, 5, 'Where do you acquire fuelwood for cooking?', 'choicegroup', '20', b'0', 'Forest,Own land,I buy it', '', ''),
(19, 1, 5, 'Who is in charge of getting fuelwood?', 'choicegroup', '20', b'0', 'Mother,Father,Grandmother,Grandfather,Kid', '', ''),
(20, 1, 5, 'What distance you travel to collect fuelwood?', 'text', '20', b'0', NULL, '', ''),
(21, 1, 5, 'If you buy, how much do you spend on firewood per week?', 'text', '20', b'0', NULL, '', ''),
(22, 1, 5, 'Where do you buy the wood from?', 'text', '20', b'0', NULL, '', ''),
(23, 1, 5, 'How often do you collect fuelwood? (times per week)', 'text', '20', b'0', NULL, '', ''),
(24, 1, 5, 'What quantity of fuelwood you use per day? (KG/KSH)', 'text', '20', b'0', NULL, '', ''),
(25, 1, 5, 'State any complain or challenge while using the stove', 'text', '20', b'0', NULL, '', ''),
(26, 1, 5, 'What do you like most about your current stove?', 'text', '20', b'0', NULL, '', ''),
(27, 1, 5, 'Would you want to participate in a period of detailed check with measures of wood consumption?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(28, 1, 6, 'What energy source do you use for lighting?', 'choicegroup', '20', b'0', 'Kerosene lamp,Pressure lamp,Electricity,Solar,Other', '', ''),
(29, 1, 6, 'What size of land is comprised in the household? (ha)', 'text', '20', b'0', NULL, '', ''),
(30, 1, 7, 'Would you be interested to participate in a tree planting project?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(31, 1, 7, 'What is your preferred species of fuelwood?', 'text', '20', b'0', NULL, '', ''),
(32, 1, 7, 'And the Second?', 'text', '20', b'0', NULL, '', ''),
(33, 1, 7, 'What tree species you would like to receive?', 'text', '20', b'0', NULL, '', ''),
(34, 1, 7, 'Have you ever been trained on tree planting before Hifadhi-Livelihoods project?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(35, 2, 1, 'User ID', 'text', '20', b'0', NULL, '', ''),
(36, 2, 1, 'Hifadhi Serial Number', 'text', '20', b'0', NULL, '', ''),
(37, 2, 1, 'Position in the family', 'choicegroup', '20', b'0', 'Father, Mother,Child,Other', '', ''),
(38, 2, 1, 'Sub-Location', 'text', '20', b'0', '', '', ''),
(39, 2, 2, 'Number of family members on household', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '', ''),
(40, 2, 2, 'Type of house', 'choicegroup', '20', b'0', 'Permanent, Semi-Permanent', '', ''),
(41, 2, 3, 'Previous (traditional) stove used', 'choicegroup', '20', b'0', 'Three-stones,Charcoal stove,Gas,Biofuel, woodstove, other', '', ''),
(42, 2, 3, 'Are you still using the traditional three-stones stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(43, 2, 3, 'If yes', 'radiogroup', '20', b'0', 'Use of low efficient stoves on special occasion only, Increase in household size that justifies the use of a second three-stones stove because the Hifadhi stove is being fully utilized, Transfer of the  ownership (e.g. sales or gifts) with sufficient evidence by the original owner that the Hifadhi stove is still in use,Other reason', '', ''),
(44, 2, 3, 'With what frequency?', 'text', '20', b'0', '', '', ''),
(45, 2, 3, 'Are you still using the Hifadhi stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(46, 2, 3, 'For how long have you used the Hifadhi stove? (Years)', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10', '', ''),
(47, 2, 3, 'Is there improvement in your kitchen environment and reduction of fuel consumption since?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(48, 2, 3, 'Is your stove damaged?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(49, 2, 3, 'Is there reduction in the number of hours used to cook since?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(50, 2, 3, 'Where is your primary cooking location after acquiring after aquiring the stove?', 'choicegroup', '20', b'0', 'Inside the main house,Outside,Separate enclosed kitchen', '', ''),
(51, 2, 3, 'Degree of satisfaction with new Hifadhi stove?', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10', '', ''),
(52, 2, 3, 'Why?what do you like about it?', 'text', '20', b'0', NULL, '', ''),
(53, 2, 3, 'How often are you using the Hifadhi stove?', 'choicegroup', '20', b'0', 'Every day,Once a week,Once a month,Never', '', ''),
(54, 2, 4, 'What type of stoves are present in the kitchen?', 'text', '20', b'0', '', '', ''),
(55, 2, 4, 'The stove appears recently used (is it warm at touch, does it have ashes...)', 'text', '20', b'0', '', '', ''),
(56, 2, 4, 'Other stoves appear to be used?', 'text', '20', b'0', '', '', ''),
(57, 2, 4, 'What kind of fuel appears to be used?', 'text', '20', b'0', '', '', ''),
(58, 2, 4, 'The stove appears damaged? How?', 'text', '20', b'0', '', '', ''),
(59, 2, 4, 'Is there any evidence of repairs?', 'text', '20', b'0', '', '', ''),
(60, 2, 5, 'Where do you acquire fuelwood for cooking?', 'choicegroup', '20', b'0', 'Forest,Own land,I buy it', '', ''),
(61, 2, 5, 'What distance you travel to collect fuelwood? (Km)', 'text', '20', b'0', '', '', ''),
(62, 2, 5, 'How often do you collect fuelwood? (times per week)', 'text', '20', b'0', '', '', ''),
(63, 2, 5, 'What quantity of fuelwood you use per day? (KG/KSH)', 'text', '20', b'0', '', '', ''),
(64, 2, 5, 'State any complaint or challenge while using the stove.', 'text', '20', b'0', '', '', ''),
(65, 2, 6, 'Do you experience much smoke in the kitchen using the Hifadhi stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(66, 2, 6, 'Do you feel this has improved since you use the Hifadhi Stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(67, 2, 6, 'What energy source do you use for lighting?', 'choicegroup', '20', b'0', 'Kerosene lamp,Pressure lamp,Electricity,Solar,Other', '', ''),
(68, 2, 6, 'What size of land is comprised in the household? (ha)', 'text', '20', b'0', NULL, '', ''),
(69, 2, 7, 'What is your preferred species of fuelwood?', 'text', '20', b'0', NULL, '', ''),
(70, 2, 7, 'And the second?', 'text', '20', b'0', '', '', ''),
(71, 2, 7, 'What tree species you would like to receive?', 'text', '20', b'0', NULL, '', ''),
(72, 2, 7, 'Have you ever been trained on tree planting before Hifadhi-Livelihoods project?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(73, 2, 7, 'Did you receive any seedlings since the beginning of the project?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(74, 2, 7, 'If yes, how many?', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40', '', ''),
(75, 2, 7, 'How many seedlings are still growing?', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40', '', ''),
(76, 2, 7, 'How satisfied are you with the tree planting project?', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '', ''),
(77, 2, 7, 'Why?', 'text', '20', b'0', '', '', '');

-- --------------------------------------------------------

DROP TABLE `quest_field_groups`;
CREATE TABLE IF NOT EXISTS `quest_field_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `quest_field_groups`
--

INSERT INTO `quest_field_groups` (`group_id`, `group_name`, `description`) VALUES
(1, 'User Details', ''),
(2, 'Family Situation', ''),
(3, 'Cook Stove Information', ''),
(4, 'Observation from field officer', ''),
(5, 'Fuel Wood', ''),
(6, 'Other', ''),
(7, 'Tree Planting', '');



