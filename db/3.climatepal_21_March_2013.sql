-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 21, 2013 at 03:35 PM
-- Server version: 5.5.29
-- PHP Version: 5.4.6-1ubuntu1.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `climatepal_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_log`
--

CREATE TABLE IF NOT EXISTS `access_log` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(50) NOT NULL,
  `account` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `distributions`
--

CREATE TABLE IF NOT EXISTS `distributions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpa_number` varchar(50) NOT NULL,
  `household` int(11) NOT NULL,
  `stove_serial` int(11) NOT NULL,
  `dist_date` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE IF NOT EXISTS `districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `district` varchar(50) NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `district`, `latitude`, `longitude`, `date_added`, `date_updated`, `added_by`, `updated_by`) VALUES
(1, 'Kilaguni', '-1.292066', '36.821946', '2013-02-21 13:41:25', '2013-02-21 13:46:33', 1, 1),
(2, 'Tharaka', '-1.292066', '36.82194', '2013-02-21 13:51:09', '2013-02-21 14:08:33', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `gender` int(11) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `postal_address` varchar(50) NOT NULL,
  `postal_code` varchar(50) NOT NULL,
  `town` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` varchar(50) NOT NULL,
  `date_updated` varchar(50) NOT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`employee_id`, `surname`, `firstname`, `gender`, `telephone`, `postal_address`, `postal_code`, `town`, `email`, `added_by`, `date_added`, `date_updated`) VALUES
(1, 'admin', 'admin', 1, '', '', '', '', '', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(10) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `description`, `date_added`, `date_updated`, `added_by`, `updated_by`) VALUES
(1, 'Male', '2013-02-18 13:33:10', '2013-02-18 13:33:10', 1, 0),
(2, 'Female', '2013-02-18 14:00:27', '2013-02-18 14:00:27', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `households`
--

CREATE TABLE IF NOT EXISTS `households` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `gender` int(11) NOT NULL,
  `id_number` varchar(25) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `location` int(11) NOT NULL,
  `household` varchar(100) NOT NULL,
  `old_stove` int(11) NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(50) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `location`, `date_added`, `date_updated`, `added_by`, `updated_by`) VALUES
(1, 'Kyeni East', '2013-02-25 10:02:36', '2013-02-25 10:02:36', 1, 1),
(2, 'Kyeni South', '2013-02-25 10:02:48', '2013-02-25 10:02:48', 1, 1),
(3, 'Kyeni N. West', '2013-02-25 10:03:01', '2013-02-25 10:03:01', 1, 1),
(4, 'Kyeni N. East', '2013-02-25 10:03:15', '2013-02-25 10:03:15', 1, 1),
(5, 'Kyeni Central', '2013-02-25 10:03:34', '2013-02-25 10:03:34', 1, 1),
(6, 'Runyenjes West', '2013-02-25 10:03:49', '2013-02-25 10:03:49', 1, 1),
(7, 'Kagaari East', '2013-02-25 10:04:00', '2013-02-25 10:04:00', 1, 1),
(8, 'Runyenjes East', '2013-02-25 10:04:11', '2013-02-25 10:04:11', 1, 1),
(9, 'Kagaari N.East', '2013-02-25 10:05:29', '2013-02-25 10:05:29', 1, 1),
(10, 'Kagaari S.West', '2013-02-25 10:05:40', '2013-02-25 10:05:40', 1, 1),
(11, 'Kagaari N.West', '2013-02-25 10:05:53', '2013-02-25 10:05:53', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(25) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `description`, `date_added`, `date_updated`, `added_by`, `updated_by`) VALUES
(1, 'Create Surveys', '2013-02-20 14:32:14', '2013-02-20 14:36:43', 1, 1),
(2, 'Add Distribution', '2013-02-20 14:32:31', '2013-02-20 14:32:31', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `questionnaires`
--

CREATE TABLE IF NOT EXISTS `questionnaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `account` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `questionnaires`
--

INSERT INTO `questionnaires` (`id`, `survey_id`, `description`, `account`, `date_added`, `date_updated`) VALUES
(1, 1, 'Questionnaire for baseline survey', 0, '', ''),
(2, 2, 'Questionnaire for monitoring survey', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `quest_fields`
--

CREATE TABLE IF NOT EXISTS `quest_fields` (
  `field_id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `field_name` varchar(1000) NOT NULL,
  `field_type` varchar(25) NOT NULL,
  `field_size` varchar(25) NOT NULL,
  `required` bit(1) NOT NULL,
  `options` varchar(1000) DEFAULT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`field_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `quest_fields`
--

INSERT INTO `quest_fields` (`field_id`, `questionnaire_id`, `group_id`, `field_name`, `field_type`, `field_size`, `required`, `options`, `date_added`, `date_updated`) VALUES
(1, 1, 1, 'User ID', 'text', '20', b'1', NULL, '', ''),
(2, 1, 1, 'Location', 'text', '20', b'1', NULL, '', ''),
(3, 1, 1, 'Coordinates', 'text', '50', b'1', NULL, '', ''),
(4, 1, 2, 'Number of Family Members ', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '', ''),
(5, 1, 2, 'Type of House', 'choicegroup', '20', b'0', 'Permanent,Semi-Permanent', '', ''),
(6, 1, 3, 'Type of Stove in use?', 'choicegroup', '20', b'0', 'Three-stones,Charcoal stove,Gas,Biofuel,Woodstove,Other', '', ''),
(7, 1, 3, 'Do you  experience much smoke in the kitchen while using the stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(8, 1, 3, 'For how long have you used the current stove?', 'text', '20', b'0', NULL, '', ''),
(9, 1, 3, 'How many times do you use to cook per day?', 'text', '20', b'0', NULL, '', ''),
(10, 1, 3, 'How many hours do you use to cook per day?', 'text', '20', b'0', NULL, '', ''),
(11, 1, 3, 'Where is your primary cooking location?', 'text', '20', b'0', '', '', ''),
(12, 1, 3, 'Would you be interested in getting a more efficient stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(13, 1, 3, 'If so, would you discontinue use of your current stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(14, 1, 4, 'Where do you acquire fuelwood for cooking?', 'choicegroup', '20', b'0', 'Forest,Own land,I buy it', '', ''),
(15, 1, 4, 'What distance you travel to collect fuelwood?', 'text', '20', b'0', NULL, '', ''),
(16, 1, 4, 'If you buy, how much do you spend on firewood per week?', 'text', '20', b'0', NULL, '', ''),
(17, 1, 4, 'Where do you buy the wood from?', 'text', '20', b'0', NULL, '', ''),
(18, 1, 4, 'How often do you collect fuelwood? (times per week)', 'text', '20', b'0', NULL, '', ''),
(19, 1, 4, 'What quantity of fuelwood you use per day? (KG/KSH)', 'text', '20', b'0', NULL, '', ''),
(20, 1, 4, 'State any complain or challenge while using the stove', 'text', '20', b'0', NULL, '', ''),
(21, 1, 5, 'What energy source do you use for lighting?', 'choicegroup', '20', b'0', 'Kerosene lamp,Pressure lamp,Electricity,Solar,Other', '', ''),
(22, 1, 5, 'What size of land is comprised in the household? (ha)', 'text', '20', b'0', NULL, '', ''),
(23, 1, 6, 'Would you be interested to participate in a tree planting project?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(24, 1, 6, 'What is your preferred species of fuelwood?', 'text', '20', b'0', NULL, '', ''),
(25, 1, 6, 'And the Second?', 'text', '20', b'0', NULL, '', ''),
(26, 1, 6, 'What tree species you would like to receive?', 'text', '20', b'0', NULL, '', ''),
(27, 1, 6, 'Have you ever been trained on tree planting before Hifadhi-Livelihoods project?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(28, 1, 1, 'Username', 'text', '20', b'0', NULL, '', ''),
(29, 1, 1, 'Position in Family', 'choicegroup', '20', b'0', 'Father,Mother,Child,Other', '', ''),
(30, 2, 1, 'User ID', 'text', '20', b'0', NULL, '', ''),
(31, 2, 1, 'Hifadhi Serial Number', 'text', '20', b'0', NULL, '', ''),
(32, 2, 1, 'Position in the family', 'choicegroup', '20', b'0', 'Father, Mother,Child,Other', '', ''),
(33, 2, 1, 'Location', 'text', '20', b'0', '', '', ''),
(34, 2, 1, 'Coordinates', 'text', '20', b'0', '', '', ''),
(35, 2, 2, 'Number of family members on household', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '', ''),
(36, 2, 2, 'Type of house', 'choicegroup', '20', b'0', 'Permanent, Semi-Permanent', '', ''),
(37, 2, 3, 'Previous (traditional) stove used', 'choicegroup', '20', b'0', 'Three-stones,Charcoal stove,Gas,Biofuel, woodstove, other', '', ''),
(38, 2, 3, 'Are you still using the traditional three-stones stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(39, 2, 3, 'If yes', 'radiogroup', '20', b'0', 'Use of low efficient stoves on special occasion only, Increase in household size that justifies the use of a second three-stones stove because the Hifadhi stove is being fully utilized, Transfer of the  ownership (e.g. sales or gifts) with sufficient evidence by the original owner that the Hifadhi stove is still in use,Other reason', '', ''),
(40, 2, 3, 'Are you still using the Hifadhi stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(41, 2, 3, 'For how long have you used the Hifadhi stove? (Years)', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10', '', ''),
(42, 2, 3, 'Is there improvement in your kitchen environment and reduction of fuel consumption since?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(43, 2, 3, 'Is there reduction in the number of hours used to cook since?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(44, 2, 3, 'Where is your primary cooking location after acquiring after aquiring the stove?', 'choicegroup', '20', b'0', 'Inside the main house,Outside,Separate enclosed kitchen', '', ''),
(45, 2, 3, 'Degree of satisfaction with new Hifadhi stove?', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10', '', ''),
(46, 2, 3, 'Why?what do you like about it?', 'text', '20', b'0', NULL, '', ''),
(47, 2, 3, 'How often are you using the Hifadhi stove?', 'choicegroup', '20', b'0', 'Every day,Once a week,Once a month,Never', '', ''),
(48, 2, 4, 'Where do you acquire fuelwood for cooking?', 'choicegroup', '20', b'0', 'Forest,Own land,I buy it', '', ''),
(49, 2, 4, 'What distance you travel to collect fuelwood? (Km)', 'text', '20', b'0', '', '', ''),
(50, 2, 4, 'How often do you collect fuelwood? (times per week)', 'text', '20', b'0', '', '', ''),
(51, 2, 4, 'What quantity of fuelwood you use per day? (KG/KSH)', 'text', '20', b'0', '', '', ''),
(52, 2, 4, 'State any complaint or challenge while using the stove.', 'text', '20', b'0', '', '', ''),
(53, 2, 5, 'Do you experience much smoke in the kitchen using the Hifadhi stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(54, 2, 5, 'Do you feel this has improved since you use the Hifadhi Stove?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(55, 2, 5, 'What energy source do you use for lighting?', 'choicegroup', '20', b'0', 'Kerosene lamp,Pressure lamp,Electricity,Solar,Other', '', ''),
(56, 2, 5, 'What size of land is comprised in the household? (ha)', 'text', '20', b'0', NULL, '', ''),
(57, 2, 6, 'What is your preferred species of fuelwood?', 'text', '20', b'0', NULL, '', ''),
(58, 2, 6, 'And the second?', 'text', '20', b'0', '', '', ''),
(59, 2, 6, 'What tree species you would like to receive?', 'text', '20', b'0', NULL, '', ''),
(60, 2, 6, 'Have you ever been trained on tree planting before Hifadhi-Livelihoods project?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(61, 2, 6, 'Did you receive any seedlings since the beginning of the project?', 'choicegroup', '20', b'0', 'Yes,No', '', ''),
(62, 2, 6, 'If yes, how many?', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40', '', ''),
(63, 2, 6, 'How many seedlings are still growing?', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40', '', ''),
(64, 2, 6, 'How satisfied are you with the tree planting project?', 'choicegroup', '20', b'0', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '', ''),
(65, 2, 6, 'Why?', 'text', '20', b'0', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `quest_field_groups`
--

CREATE TABLE IF NOT EXISTS `quest_field_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(25) NOT NULL,
  `description` varchar(25) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `quest_field_groups`
--

INSERT INTO `quest_field_groups` (`group_id`, `group_name`, `description`) VALUES
(1, 'User Details', ''),
(2, 'Family Situation', ''),
(3, 'Cook Stove Information', ''),
(4, 'Fuel Wood', ''),
(5, 'Other', ''),
(6, 'Tree Planting', '');

-- --------------------------------------------------------

--
-- Table structure for table `quest_field_responses`
--

CREATE TABLE IF NOT EXISTS `quest_field_responses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quest_field_results`
--

CREATE TABLE IF NOT EXISTS `quest_field_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `value` varchar(500) NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `data_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(25) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`, `description`, `status`, `date_added`, `date_updated`, `added_by`, `updated_by`) VALUES
(1, 'Administrator', 'Controls the whole system', 1, '2013-02-07', '', 0, 0),
(2, 'Donor', 'Views Reports', 1, '2013-02-20 14:07:49', '2013-02-20 14:15:11', 1, 1),
(3, 'Field Officer', 'Ground Work', 1, '2013-03-18 15:11:50', '2013-03-18 15:11:50', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE IF NOT EXISTS `role_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` int(11) NOT NULL,
  `permission` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `role`, `permission`, `date_added`, `date_updated`, `added_by`, `updated_by`) VALUES
(1, 1, 1, '2013-02-21 11:13:55', '2013-02-21 11:14:03', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_active_date` datetime NOT NULL,
  `is_expired` bit(1) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `computer_name` varchar(255) DEFAULT NULL,
  `Device` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stoves`
--

CREATE TABLE IF NOT EXISTS `stoves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_number` varchar(50) NOT NULL,
  `stove_type` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stove_types`
--

CREATE TABLE IF NOT EXISTS `stove_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `stove_types`
--

INSERT INTO `stove_types` (`id`, `description`, `date_added`, `date_updated`, `added_by`, `updated_by`) VALUES
(1, 'Three Stones', '2013-02-14 16:19:55', '2013-02-20 12:40:58', 1, 1),
(2, 'Liquified Petroleum Gas', '2013-02-18 15:13:02', '2013-02-18 15:13:02', 1, 0),
(3, 'Charcoal Stove', '2013-02-25 14:54:49', '2013-02-25 14:54:49', 1, 0),
(4, 'Biofuel', '2013-02-25 14:55:02', '2013-02-25 14:55:02', 1, 0),
(5, 'Wood Stove', '2013-02-25 14:55:09', '2013-02-25 14:55:09', 1, 0),
(6, 'Other', '2013-02-25 14:55:15', '2013-02-25 14:55:15', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sublocations`
--

CREATE TABLE IF NOT EXISTS `sublocations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sublocation` varchar(50) NOT NULL,
  `location` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `sublocations`
--

INSERT INTO `sublocations` (`id`, `sublocation`, `location`, `date_added`, `date_updated`, `added_by`, `updated_by`) VALUES
(1, 'Kigumo', 1, '2013-02-25 10:08:26', '2013-02-25 10:08:26', 1, 1),
(2, 'Mukuria', 1, '2013-02-25 10:08:44', '2013-02-25 10:08:44', 1, 1),
(3, 'Kasafari', 2, '2013-02-25 10:09:05', '2013-02-25 10:09:05', 1, 1),
(4, 'Kirurumo', 2, '2013-02-25 10:09:19', '2013-02-25 10:09:19', 1, 1),
(5, 'Kathunguri', 2, '2013-02-25 10:09:33', '2013-02-25 10:09:33', 1, 1),
(6, 'Kariru', 2, '2013-02-25 10:09:44', '2013-02-25 10:09:44', 1, 1),
(7, 'Mufu', 3, '2013-02-25 10:09:58', '2013-02-25 10:09:58', 1, 1),
(8, 'Rukuriri', 3, '2013-02-25 10:10:11', '2013-02-25 10:10:11', 1, 1),
(9, 'Njeruri', 3, '2013-02-25 10:10:25', '2013-02-25 10:10:25', 1, 1),
(10, 'Kathari', 4, '2013-02-25 10:10:39', '2013-02-25 10:10:39', 1, 1),
(11, 'Kianglingi', 4, '2013-02-25 10:10:52', '2013-02-25 10:10:52', 1, 1),
(12, 'Iriari', 4, '2013-02-25 10:11:03', '2013-02-25 10:11:03', 1, 1),
(13, 'Kathanjuri', 5, '2013-02-25 10:11:24', '2013-02-25 10:11:24', 1, 1),
(14, 'Nyagari', 5, '2013-02-25 10:11:38', '2013-02-25 10:11:38', 1, 1),
(15, 'Gikuuri', 6, '2013-02-25 10:13:21', '2013-02-25 10:13:21', 1, 1),
(16, 'Mbiruri', 6, '2013-02-25 10:13:34', '2013-02-25 10:13:34', 1, 1),
(17, 'Gitare', 6, '2013-02-25 10:13:47', '2013-02-25 10:13:47', 1, 1),
(18, 'Kiringa', 7, '2013-02-25 10:14:03', '2013-02-25 10:14:03', 1, 1),
(19, 'Gichera', 7, '2013-02-25 10:14:17', '2013-02-25 10:14:17', 1, 1),
(20, 'Gichiche', 8, '2013-02-25 10:14:37', '2013-02-25 10:14:37', 1, 1),
(21, 'Kigaa', 8, '2013-02-25 10:14:55', '2013-02-25 10:14:55', 1, 1),
(22, 'Kanja North', 9, '2013-02-25 10:15:15', '2013-02-25 10:15:15', 1, 1),
(23, 'Kanja South', 9, '2013-02-25 10:15:28', '2013-02-25 10:15:28', 1, 1),
(24, 'Mukuuri', 9, '2013-02-25 10:15:43', '2013-02-25 10:15:43', 1, 1),
(25, 'Nduuri', 9, '2013-02-25 10:15:57', '2013-02-25 10:15:57', 1, 1),
(26, 'Kawanjara', 10, '2013-02-25 10:16:15', '2013-02-25 10:16:15', 1, 1),
(27, 'Nthagayia', 10, '2013-02-25 10:16:30', '2013-02-25 10:16:30', 1, 1),
(28, 'Maranga', 10, '2013-02-25 10:16:44', '2013-02-25 10:16:44', 1, 1),
(29, 'Mugui', 11, '2013-02-25 10:17:01', '2013-02-25 10:17:01', 1, 1),
(30, 'Kianjokoma', 11, '2013-02-25 10:17:13', '2013-02-25 10:17:52', 1, 1),
(31, 'Mbuinjeru', 11, '2013-02-25 10:17:27', '2013-02-25 10:17:27', 1, 1),
(32, 'Kamugere', 11, '2013-02-25 10:17:40', '2013-02-25 10:17:40', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

CREATE TABLE IF NOT EXISTS `surveys` (
  `survey_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `account` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`survey_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`survey_id`, `description`, `account`, `date_added`, `date_updated`) VALUES
(1, 'Baseline Survey', 1, '', ''),
(2, 'Monitoring Survey', 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `employee_id`, `role_id`, `username`, `password`, `flag`, `date_added`, `date_updated`) VALUES
(1, 1, 1, 'admin', '$2a$08$bcA025O1jFNsejfjrkzJpOu/VerwGf9RdAfCTOOb2xrbzGcK/AoKS', 1, '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
