-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 11, 2013 at 02:39 PM
-- Server version: 5.5.29
-- PHP Version: 5.4.6-1ubuntu1.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `climatepal`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_log`
--

CREATE TABLE IF NOT EXISTS `access_log` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(50) NOT NULL,
  `account` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `gender` int(11) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `postal_address` varchar(50) NOT NULL,
  `postal_code` varchar(50) NOT NULL,
  `town` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` varchar(50) NOT NULL,
  `date_updated` varchar(50) NOT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`employee_id`, `surname`, `firstname`, `gender`, `telephone`, `postal_address`, `postal_code`, `town`, `email`, `added_by`, `date_added`, `date_updated`) VALUES
(1, 'Doe', 'John', 1, '0722123456', 'P.O. BOX 123456', '00200', 'Nairobi', 'name@example.com', 1, '2013-02-07', '2013-02-07');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(10) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hifadhi`
--

CREATE TABLE IF NOT EXISTS `hifadhi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpa_number` varchar(50) NOT NULL,
  `household` int(11) NOT NULL,
  `stove` int(11) NOT NULL,
  `account` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `households`
--

CREATE TABLE IF NOT EXISTS `households` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `gender` int(11) NOT NULL,
  `id_number` varchar(25) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `address` varchar(25) NOT NULL,
  `code` varchar(25) NOT NULL,
  `town` varchar(25) NOT NULL,
  `location` int(11) NOT NULL,
  `old_stove` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `coordinates` float NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(25) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `questionnaire`
--

CREATE TABLE IF NOT EXISTS `questionnaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  `account` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quest_fields`
--

CREATE TABLE IF NOT EXISTS `quest_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire` int(11) NOT NULL,
  `field_name` varchar(25) NOT NULL,
  `field_type` varchar(25) NOT NULL,
  `field_size` varchar(25) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quest_field_values`
--

CREATE TABLE IF NOT EXISTS `quest_field_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field` int(11) NOT NULL,
  `values` varchar(100) NOT NULL,
  `survey` int(11) NOT NULL,
  `account` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(25) NOT NULL,
  `decription` varchar(25) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`, `decription`, `date_added`, `date_updated`) VALUES
(1, 'Administrator', 'Controls the whole system', '2013-02-07', '');

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE IF NOT EXISTS `role_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` int(11) NOT NULL,
  `permission` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_active_date` datetime NOT NULL,
  `is_expired` bit(1) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `computer_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `user_id`, `created_date`, `last_active_date`, `is_expired`, `ip_address`, `computer_name`) VALUES
('weawraoi', 1, '2013-02-06 00:00:00', '2013-02-06 10:00:00', b'1', '127.0.0.1', NULL),
('HS0nwPkl1ZxYs479kHxW', 1, '2013-02-06 00:00:00', '2013-02-06 00:00:00', b'0', '127.0.0.1', NULL),
('kGTZ6C6zvZIeV2pC2JFR', 1, '2013-02-06 23:24:56', '2013-02-06 23:24:56', b'0', '127.0.0.1', NULL),
('zuTTFf25EhoxMYN2jPwX', 1, '2013-02-06 13:18:23', '2013-02-06 13:18:23', b'0', '127.0.0.1', NULL),
('RNtkoJ7YGtGV1mgGz62T', 1, '2013-02-06 16:21:48', '2013-02-06 16:21:48', b'0', '127.0.0.1', NULL),
('IAfHufp3zymdAmqmXkn5', 1, '2013-02-06 16:34:09', '2013-02-06 16:34:09', b'0', '127.0.0.1', NULL),
('oiMQSn8sNTz84a4EDqtA', 1, '2013-02-06 16:36:15', '2013-02-06 16:36:21', b'1', '127.0.0.1', NULL),
('6xMrUYA9JuW2m8r7crXA', 1, '2013-02-06 16:37:27', '2013-02-06 16:37:32', b'1', '127.0.0.1', NULL),
('XWZIX2HKYbEgAT8eYEpm', 1, '2013-02-06 17:25:42', '2013-02-06 17:25:45', b'1', '127.0.0.1', NULL),
('b4Zzu3rXz4ugLj0lttE5', 1, '2013-02-06 17:26:31', '2013-02-06 17:31:36', b'1', '127.0.0.1', NULL),
('4YrcBni8UvuhvS0GlfUP', 1, '2013-02-06 17:31:45', '2013-02-06 17:31:45', b'0', '127.0.0.1', NULL),
('SZ9ylfMTd0iLnlGoQzoT', 1, '2013-02-06 17:33:11', '2013-02-06 17:33:27', b'1', '192.168.170.25', NULL),
('KdGcUKRQksMqXzfj34p1', 1, '2013-02-07 10:20:37', '2013-02-07 10:20:43', b'1', '127.0.0.1', NULL),
('zxhhcIvjFrFdFNsFgsHq', 1, '2013-02-07 10:25:15', '2013-02-07 11:33:06', b'1', '127.0.0.1', NULL),
('Jb8mfwUn3VyzWs6R65tI', 1, '2013-02-07 12:31:32', '2013-02-07 12:31:38', b'1', '127.0.0.1', NULL),
('L3NFDlgyPy49zcZJCkQm', 1, '2013-02-07 12:33:44', '2013-02-07 12:34:06', b'1', '127.0.0.1', NULL),
('ygw6WSrbM11vXhMVbyyO', 1, '2013-02-07 12:34:24', '2013-02-07 12:34:49', b'1', '127.0.0.1', NULL),
('1iklC8GgCf9I9y5QNd7S', 1, '2013-02-07 12:38:01', '2013-02-07 12:40:27', b'1', '127.0.0.1', NULL),
('LNaFE6uzxD38T2j6OkrK', 1, '2013-02-07 13:06:37', '2013-02-07 13:06:37', b'0', '127.0.0.1', NULL),
('ACd0Y6FRhMbq9c5yCFBi', 1, '2013-02-07 13:32:02', '2013-02-07 13:59:17', b'1', '127.0.0.1', NULL),
('Y1snIMJ7hW8mzfOZnxI9', 1, '2013-02-07 14:00:10', '2013-02-07 15:05:52', b'1', '127.0.0.1', NULL),
('dqJS6wMbbImjpBstevK4', 1, '2013-02-07 15:05:56', '2013-02-07 15:47:37', b'1', '127.0.0.1', NULL),
('JDBHSGkIyMmSyxy4B62J', 1, '2013-02-07 15:47:42', '2013-02-07 15:48:15', b'1', '127.0.0.1', NULL),
('BV7J7vFY33ByiWIblajY', 1, '2013-02-07 15:48:25', '2013-02-07 16:38:53', b'1', '127.0.0.1', NULL),
('YU08JDOeZycvgAVp79Rv', 1, '2013-02-07 16:39:00', '2013-02-07 19:44:01', b'1', '127.0.0.1', NULL),
('qE4v1sGBorfJcQAgqFpC', 1, '2013-02-07 19:44:22', '2013-02-07 19:44:46', b'1', '127.0.0.1', NULL),
('IpkYZ7oSTBADUwCHs2DK', 1, '2013-02-07 19:44:50', '2013-02-07 19:44:50', b'0', '127.0.0.1', NULL),
('dWhTAiP9wTZxwsVK8nnt', 1, '2013-02-07 22:58:00', '2013-02-07 23:22:15', b'1', '127.0.0.1', NULL),
('WogiauA6qnvqAtbBxHUo', 1, '2013-02-07 23:23:15', '2013-02-07 23:31:25', b'1', '127.0.0.1', NULL),
('zYmkJwedoPfZmQ4XKF2R', 1, '2013-02-07 23:31:35', '2013-02-07 23:31:35', b'0', '127.0.0.1', NULL),
('JP1I06eCfLZEZYIc6jdG', 1, '2013-02-08 11:00:19', '2013-02-08 11:19:56', b'1', '127.0.0.1', NULL),
('tm2jOOf1jVgkycyatssp', 1, '2013-02-08 11:20:10', '2013-02-08 11:20:10', b'0', '127.0.0.1', NULL),
('lGicpAAk9l0OFqVEQ5Wt', 1, '2013-02-08 16:49:41', '2013-02-08 16:49:48', b'1', '127.0.0.1', NULL),
('3v4iqE3qxf0lTJPcVbgh', 1, '2013-02-08 19:08:06', '2013-02-08 19:08:06', b'0', '127.0.0.1', NULL),
('o2CGJCnPC48VTdKR6zJg', 1, '2013-02-08 19:23:23', '2013-02-08 19:23:23', b'0', '127.0.0.1', NULL),
('uFdTmHgA9SpPVGSpA162', 1, '2013-02-08 19:33:52', '2013-02-08 19:33:52', b'0', '127.0.0.1', NULL),
('bCgWNqt8FJFjBdOG0QBT', 1, '2013-02-09 11:11:59', '2013-02-09 11:12:26', b'1', '127.0.0.1', NULL),
('gfXAN3AkeVlWryn8PNr1', 1, '2013-02-09 11:12:47', '2013-02-09 11:12:52', b'1', '127.0.0.1', NULL),
('1nYYLLLXkeTLfF1mKd0Z', 1, '2013-02-09 11:31:20', '2013-02-09 11:31:24', b'1', '127.0.0.1', NULL),
('g7A0PiFpmzmpe6gCDjUC', 1, '2013-02-09 11:31:56', '2013-02-09 11:32:00', b'1', '127.0.0.1', NULL),
('3pJDoByyhjYqxGUa6mlg', 1, '2013-02-09 11:32:09', '2013-02-09 11:34:19', b'1', '127.0.0.1', NULL),
('csWVZWY8LmlGPmmZo3lG', 1, '2013-02-09 11:34:28', '2013-02-09 11:34:37', b'1', '127.0.0.1', NULL),
('R57SZic5heBr0GrW7gZd', 1, '2013-02-09 11:51:40', '2013-02-09 11:59:34', b'1', '127.0.0.1', NULL),
('iVeIBefIVVyYFYrK6yJk', 1, '2013-02-09 12:00:26', '2013-02-09 12:00:40', b'1', '127.0.0.1', NULL),
('oibV35nHcpY853jxCxoQ', 1, '2013-02-11 10:33:48', '2013-02-11 10:33:48', b'0', '127.0.0.1', NULL),
('Zkal3TAq2FegNmhbJhtk', 1, '2013-02-11 14:14:26', '2013-02-11 14:46:40', b'1', '127.0.0.1', NULL),
('yXZj4Z7dawp9vCkHHa0u', 1, '2013-02-11 15:00:37', '2013-02-11 15:01:28', b'1', '127.0.0.1', NULL),
('uZYOZvsWjNDqBiQuQxUI', 1, '2013-02-11 15:01:38', '2013-02-11 15:01:38', b'0', '127.0.0.1', NULL),
('zIiROWzJwG9VTCD21Hv0', 1, '2013-02-11 15:58:23', '2013-02-11 16:00:07', b'1', '127.0.0.1', NULL),
('MVGBVZ107fLVoZcB90L0', 1, '2013-02-11 16:00:13', '2013-02-11 16:25:31', b'1', '127.0.0.1', NULL),
('vKyLmLQZLL2dyjB8q7EG', 1, '2013-02-11 16:25:36', '2013-02-11 17:38:48', b'1', '127.0.0.1', NULL),
('IbPf58AL4FUa3bx5NegE', 1, '2013-02-11 17:38:53', '2013-02-11 17:38:53', b'0', '127.0.0.1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stoves`
--

CREATE TABLE IF NOT EXISTS `stoves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_number` varchar(50) NOT NULL,
  `stove_type` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stove_types`
--

CREATE TABLE IF NOT EXISTS `stove_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

CREATE TABLE IF NOT EXISTS `surveys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `account` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL,
  `date_added` varchar(25) NOT NULL,
  `date_updated` varchar(25) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `employee_id`, `role_id`, `username`, `password`, `flag`, `date_added`, `date_updated`) VALUES
(1, 1, 1, 'admin', 'admin', 1, '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
